package pe.entel.android.plantilla.mapper;

import org.modelmapper.ModelMapper;

import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.model.PedidoModel;

public final class PedidoModelMapper {

    public static PedidoModel adapter(Pedido pedido){
        PedidoModel pedidoModel =  new PedidoModel();

        if(pedido != null) {
            ModelMapper modelMapper = new ModelMapper();
            pedidoModel = modelMapper.map(pedido, PedidoModel.class);
        }
        return pedidoModel;
    }

    public static Pedido revert(PedidoModel model){
        Pedido pedido =  new Pedido();

        if(model != null) {
            ModelMapper modelMapper = new ModelMapper();
            pedido = modelMapper.map(model, Pedido.class);
        }
        return pedido;
    }
}
