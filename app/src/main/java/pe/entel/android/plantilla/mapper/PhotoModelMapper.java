package pe.entel.android.plantilla.mapper;

import org.modelmapper.ModelMapper;

import pe.entel.android.plantilla.domain.entity.Photo;
import pe.entel.android.plantilla.domain.entity.Usuario;
import pe.entel.android.plantilla.model.PhotoModel;
import pe.entel.android.plantilla.model.UsuarioModel;

public final class PhotoModelMapper {

    public static PhotoModel adapter(Photo entidad){
        PhotoModel model =  new PhotoModel();

        if(entidad != null) {
            ModelMapper modelMapper = new ModelMapper();
            model = modelMapper.map(entidad, PhotoModel.class);
        }
        return model;
    }
}
