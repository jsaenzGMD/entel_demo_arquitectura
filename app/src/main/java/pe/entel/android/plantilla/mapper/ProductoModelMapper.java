package pe.entel.android.plantilla.mapper;

import com.google.gson.Gson;

import org.modelmapper.ModelMapper;
import org.modelmapper.internal.util.Strings;

import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.model.PedidoModel;
import pe.entel.android.plantilla.model.ProductoModel;

public final class ProductoModelMapper {


    public static ProductoModel StringAdapter(String text){
        ProductoModel model =  new ProductoModel();

        if(text != null) {
            Gson gson = new Gson();
            model = gson.fromJson(text, ProductoModel.class);
        }
        return model;
    }

    public static ProductoModel EntityAdapter(Producto entidad){
        ProductoModel model =  new ProductoModel();

        if(entidad != null) {
            ModelMapper modelMapper = new ModelMapper();
            model = modelMapper.map(entidad, ProductoModel.class);
        }
        return model;
    }

    public static Producto EntityRevert(ProductoModel model){
        Producto entidad =  new Producto();

        if(model != null) {
            ModelMapper modelMapper = new ModelMapper();
            entidad = modelMapper.map(model, Producto.class);
        }
        return entidad;
    }
}
