package pe.entel.android.plantilla.mapper;

import org.modelmapper.ModelMapper;

import pe.entel.android.plantilla.domain.entity.Usuario;
import pe.entel.android.plantilla.model.UsuarioModel;

public final class UsuarioModelMapper {

    public static UsuarioModel adapter(Usuario usuario){
        UsuarioModel usuarioModel =  new UsuarioModel();

        if(usuario != null) {
            ModelMapper modelMapper = new ModelMapper();
            usuarioModel = modelMapper.map(usuario, UsuarioModel.class);
        }
        return usuarioModel;
    }
}
