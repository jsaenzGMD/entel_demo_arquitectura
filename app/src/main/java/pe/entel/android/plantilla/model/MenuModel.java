package pe.entel.android.plantilla.model;

import android.graphics.drawable.Drawable;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.plantilla.R;

import static java.security.AccessController.getContext;

/**
 * Created by rtamayov on 12/04/2017.
 */
public class MenuModel {

    public final static int SINCRONIZAR = 0;
    public final static int SALIR = 1;
    public final static int INICIO = 2;
    public final static int PENDIENTE = 3;
    public final static int FOTO = 4;

    int id;
    String titulo;
    int imagen;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public static List<MenuModel> getMenus(int pendientes) {

        List<MenuModel> lista = new ArrayList<>();
        MenuModel model;

        model = new MenuModel();
        model.setId(INICIO  );
        model.setTitulo("Pedidos");
        model.setImagen(R.drawable.ic_icon_menu_pedidos_esc);
        lista.add(model);

        model = new MenuModel();
        model.setId(FOTO);
        model.setTitulo("Foto");
        model.setImagen(R.drawable.icon_menu_camara_esc);
        lista.add(model);

        model = new MenuModel();
        model.setId(SINCRONIZAR);
        model.setTitulo("Sincronizacion");
        model.setImagen(R.drawable.ic_sincronizar_menu);
        lista.add(model);

        model = new MenuModel();
        model.setId(PENDIENTE);
        model.setTitulo("Pendientes ("+pendientes+")");
        model.setImagen(R.drawable.ic_pendientes);
        lista.add(model);

        model = new MenuModel();
        model.setId(SALIR);
        model.setTitulo("Salir");
        model.setImagen(R.drawable.ic_salir);
        lista.add(model);

        return lista;
    }
}
