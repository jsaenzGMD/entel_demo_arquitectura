package pe.entel.android.plantilla.model;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Producto;


public class PedidoModel {
    private Long id = null;
    private String idUsuario;
    private String latitud;
    private String longitud;
    private String fecha;
    private List<Producto> detalles;
    private String flgEnviado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFlgEnviado() {
        return flgEnviado;
    }

    public void setFlgEnviado(String flgEnviado) {
        this.flgEnviado = flgEnviado;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public List<Producto> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<Producto> detalles) {
        this.detalles = detalles;
    }

    public void eliminarProducto(String codigoProducto) {


        Producto bean;

        for (int i = detalles.size() - 1; i >= 0; i--) {
            bean = detalles.get(i);

            if (bean.getCodigoProducto().equals(String.valueOf(codigoProducto))) {
                detalles.remove(i);
                break;
            }
        }


    }
}
