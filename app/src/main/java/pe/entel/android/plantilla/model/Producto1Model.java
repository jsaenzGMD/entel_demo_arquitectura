package pe.entel.android.plantilla.model;

/**
 * Created by malzamoraj on 19/09/2017.
 */

public class Producto1Model {
    private boolean flagIcono;
    private boolean flagTipoIcono;
    private String urlIcono;
    private int icono;
    private String desProducto;
    private String desSubTotal;
    private String resSubTotal;
    private String desPrecioUnitario;
    private String resPrecioUnitario;
    private String desStock;
    private String resStock;
    private String desCantidad;
    private String resCantidad;
    private String desDescuento;
    private String resDescuento;
    private String desBonificacion;
    private String resBonificacion;

    public Producto1Model(boolean flagIcono, boolean flagTipoIcono, String urlIcono, int icono,
                          String desProducto, String desSubTotal, String resSubTotal,
                          String desPrecioUnitario, String resPrecioUnitario, String desStock,
                          String resStock, String desCantidad, String resCantidad,
                          String desDescuento, String resDescuento, String desBonificacion,
                          String resBonificacion) {
        this.flagIcono = flagIcono;
        this.flagTipoIcono = flagTipoIcono;
        this.urlIcono = urlIcono;
        this.icono = icono;
        this.desProducto = desProducto;
        this.desSubTotal = desSubTotal;
        this.resSubTotal = resSubTotal;
        this.desPrecioUnitario = desPrecioUnitario;
        this.resPrecioUnitario = resPrecioUnitario;
        this.desStock = desStock;
        this.resStock = resStock;
        this.desCantidad = desCantidad;
        this.resCantidad = resCantidad;
        this.desDescuento = desDescuento;
        this.resDescuento = resDescuento;
        this.desBonificacion = desBonificacion;
        this.resBonificacion = resBonificacion;
    }

    public boolean isFlagIcono() {
        return flagIcono;
    }

    public void setFlagIcono(boolean flagIcono) {
        this.flagIcono = flagIcono;
    }

    public boolean isFlagTipoIcono() {
        return flagTipoIcono;
    }

    public void setFlagTipoIcono(boolean flagTipoIcono) {
        this.flagTipoIcono = flagTipoIcono;
    }

    public String getUrlIcono() {
        return urlIcono;
    }

    public void setUrlIcono(String urlIcono) {
        this.urlIcono = urlIcono;
    }

    public int getIcono() {
        return icono;
    }

    public void setIcono(int icono) {
        this.icono = icono;
    }

    public String getDesProducto() {
        return desProducto;
    }

    public void setDesProducto(String desProducto) {
        this.desProducto = desProducto;
    }

    public String getDesSubTotal() {
        return desSubTotal;
    }

    public void setDesSubTotal(String desSubTotal) {
        this.desSubTotal = desSubTotal;
    }

    public String getResSubTotal() {
        return resSubTotal;
    }

    public void setResSubTotal(String resSubTotal) {
        this.resSubTotal = resSubTotal;
    }

    public String getDesPrecioUnitario() {
        return desPrecioUnitario;
    }

    public void setDesPrecioUnitario(String desPrecioUnitario) {
        this.desPrecioUnitario = desPrecioUnitario;
    }

    public String getResPrecioUnitario() {
        return resPrecioUnitario;
    }

    public void setResPrecioUnitario(String resPrecioUnitario) {
        this.resPrecioUnitario = resPrecioUnitario;
    }

    public String getDesStock() {
        return desStock;
    }

    public void setDesStock(String desStock) {
        this.desStock = desStock;
    }

    public String getResStock() {
        return resStock;
    }

    public void setResStock(String resStock) {
        this.resStock = resStock;
    }

    public String getDesCantidad() {
        return desCantidad;
    }

    public void setDesCantidad(String desCantidad) {
        this.desCantidad = desCantidad;
    }

    public String getResCantidad() {
        return resCantidad;
    }

    public void setResCantidad(String resCantidad) {
        this.resCantidad = resCantidad;
    }

    public String getDesDescuento() {
        return desDescuento;
    }

    public void setDesDescuento(String desDescuento) {
        this.desDescuento = desDescuento;
    }

    public String getResDescuento() {
        return resDescuento;
    }

    public void setResDescuento(String resDescuento) {
        this.resDescuento = resDescuento;
    }

    public String getDesBonificacion() {
        return desBonificacion;
    }

    public void setDesBonificacion(String desBonificacion) {
        this.desBonificacion = desBonificacion;
    }

    public String getResBonificacion() {
        return resBonificacion;
    }

    public void setResBonificacion(String resBonificacion) {
        this.resBonificacion = resBonificacion;
    }
}
