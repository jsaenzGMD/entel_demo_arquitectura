package pe.entel.android.plantilla.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

import pe.entel.android.controlesentel.busqueda.model.ListaBusqueda;
import pe.entel.android.controlesentel.busqueda.view.activity.BusquedaActivity;
import pe.entel.android.controlesentel.generic.view.activity.MensajeConfirmacionActivity;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.util.EntelConfig;
import pe.entel.android.plantilla.view.activity.FotoActivity;
import pe.entel.android.plantilla.view.activity.LoginActivity;
import pe.entel.android.plantilla.view.activity.MensajeActivity;
import pe.entel.android.plantilla.view.activity.MenuActivity;
import pe.entel.android.plantilla.view.activity.OperadorInvalidoActivity;
import pe.entel.android.plantilla.view.activity.PedidoActivity;
import pe.entel.android.plantilla.view.activity.PreferenciasActivity;
import pe.entel.android.plantilla.view.activity.ProductoDetalleActivity;
import pe.entel.android.plantilla.view.activity.ProductoEditarActivity;
import pe.entel.android.plantilla.view.activity.ProductoListaActivity;
import pe.entel.android.plantilla.view.activity.UpdateAppActivity;


public final class Navigator {

    public static void navigateToLogin(Context context){
        if(context != null){
            Intent intent = LoginActivity.getCallingIntent(context);
            context.startActivity(intent);
        }
    }

    public static void navigateToLoginSalir(Context context){
        if(context != null){
            Intent intent = LoginActivity.getCallingIntent(context);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
    }

    public static void navigateToFoto(Context context){
        if(context != null){
            Intent intent = FotoActivity.getCallingIntent(context);
            ((Activity)context).startActivityForResult(intent, EntelConfig.CONSIDCAMARA);
        }
    }

    public static void navigateToMensaje(Context context){
        if(context != null){
            Intent intent = MensajeActivity.getCallingIntent(context);
            context.startActivity(intent);
        }
    }

    public static void navigateToMenu(Context context){
        if(context != null){
            Intent intent = MenuActivity.getCallingIntent(context);
            ((Activity)context).finish();
            context.startActivity(intent);
        }
    }

    public static void navigateToMenuWithFlag(Context context, int flag){
        if(context != null){
            Intent loIntent = MenuActivity.getCallingIntent(context);
            loIntent.addFlags(flag);
            ((Activity)context).finish();
            context.startActivity(loIntent);
        }
    }

    public static void navigateToOperadorInvalido(Context context){
        if(context != null){
            Intent intent = OperadorInvalidoActivity.getCallingIntent(context);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
    }

    public static void navigateToPedido(Context context){
        if(context != null){
            Intent intent = PedidoActivity.getCallingIntent(context);
            context.startActivity(intent);
        }
    }

    public static void navigateToPedidoWhithFlag(Context context, int flag){
        if(context != null){
            Intent intent = PedidoActivity.getCallingIntent(context);
            intent.addFlags(flag);
            ((Activity)context).finish();
            context.startActivity(intent);
        }
    }

    public static void navigateToPreferencias(Context context, int perfil){
        if(context != null){
            Intent intent = PreferenciasActivity.getCallingIntent(context);
            intent.putExtra(context.getString(R.string.sp_permiso), perfil);
            context.startActivity(intent);
        }
    }

    public static void navigateToProductoDetalle(Context context){
        if(context != null){
            Intent intent = ProductoDetalleActivity.getCallingIntent(context);
            context.startActivity(intent);
        }
    }

    public static void navigateToProductoDetalleWithProduct(Context context,Producto producto){
        if(context != null){
            Gson gson = new Gson();
            Intent intent = ProductoDetalleActivity.getCallingIntent(context);
            intent.putExtra(Producto.TAG,gson.toJson(producto));
            context.startActivity(intent);
        }
    }

    public static void navigateToProductoEditar(Context context){
        if(context != null){
            Intent intent = ProductoEditarActivity.getCallingIntent(context);
            context.startActivity(intent);
        }
    }

    public static void navigateToProductoEditarWithProduct(Context context, Producto producto){
        if(context != null){
            Gson gson = new Gson();
            Intent intent = ProductoEditarActivity.getCallingIntent(context);
            intent.putExtra(Producto.TAG,gson.toJson(producto));
            context.startActivity(intent);
        }
    }

    public static void navigateToProductoLista(Context context, List<ListaBusqueda> list, int tipoList){
        //if(context != null){
        //    Intent intent = ProductoListaActivity.getCallingIntent(context);
        //    context.startActivity(intent);
        //}
        if(context != null){
            Intent intent = BusquedaActivity.getCallingIntent(context);
            intent.putExtra(BusquedaActivity.paramList, (Serializable) list);
            intent.putExtra(BusquedaActivity.paramTipoList, tipoList);
            ((Activity)context).startActivityForResult(intent, 101);

        }
    }

    public static void navigateToUpdateApp(Context context,String isUrlActualizar){
        if(context != null){
            Intent intent = UpdateAppActivity.getCallingIntent(context);
            intent.putExtra("URL_APK",isUrlActualizar);
            context.startActivity(intent);
        }
    }

    public static void navigateToMensajeConfirmacionPedido(Context context,Boolean respuesta,String mensaje){
        if(context != null) {
            if (respuesta == true) {
                Intent validarMensajeConfirmacion = new Intent(context, MensajeConfirmacionActivity.class);
                Bundle args = new Bundle();
                args.putBoolean("respuesta", respuesta);
                args.putString("descripcion", mensaje);
                validarMensajeConfirmacion.putExtras(args);

                ((Activity)context).startActivityForResult(validarMensajeConfirmacion, 100);
            } else {
                Intent validarMensajeConfirmacion = new Intent(context, MensajeConfirmacionActivity.class);
                Bundle args = new Bundle();
                args.putString("titulo1","Error");
                args.putString("titulo2","");
                args.putBoolean("respuesta", false);
                args.putString("descripcion", mensaje);
                validarMensajeConfirmacion.putExtras(args);
                ((Activity)context).startActivityForResult(validarMensajeConfirmacion, 100);
            }
        }
    }

}
