package pe.entel.android.plantilla.presenter;

import pe.entel.android.plantilla.data.repository.OperadorDataRepository;
import pe.entel.android.plantilla.data.repository.UtilDataRepository;
import pe.entel.android.plantilla.domain.repository.OperadorRepository;
import pe.entel.android.plantilla.domain.repository.UtilRepository;
import pe.entel.android.plantilla.domain.usecase.ValidarOperadorUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.ValidarOperadorUseCaseImpl;
import pe.entel.android.plantilla.view.BaseView;

/**
 * Created by rtamayov on 07/04/2017.
 */
public class BasePresenter {

    final BaseView view;
    public BasePresenter(BaseView view) {
        this.view = view;
    }

    public void iniciar() {
        validarOperador();
        validarSubscriptor();
    }

    private void validarOperador() {
        UtilRepository utilRepository = new UtilDataRepository(view.getContext());
        OperadorRepository operadorRepository = new OperadorDataRepository(view.getContext());
        ValidarOperadorUseCase validarOperadorUseCase = new ValidarOperadorUseCaseImpl(operadorRepository,utilRepository);

        try {
            validarOperadorUseCase.ejecutar();
        }catch (Exception e){
            view.irOperadorInvalido();
        }

    }

    private void validarSubscriptor() {
        view.validarSubscriptor();
    }


}
