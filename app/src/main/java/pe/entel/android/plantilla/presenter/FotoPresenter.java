package pe.entel.android.plantilla.presenter;

import android.graphics.Bitmap;
import android.hardware.Camera;

import java.util.Date;

import pe.entel.android.plantilla.data.repository.FotoDataRepository;
import pe.entel.android.plantilla.data.repository.UsuarioDataRepository;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.FotoRepository;
import pe.entel.android.plantilla.domain.repository.UsuarioRepository;
import pe.entel.android.plantilla.domain.usecase.EliminarFotoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.EliminarFotoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.GuardarFotoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.GuardarFotoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.ObtenerFotoRutaUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.ObtenerFotoRutaUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.GetTipoFotoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.GetTipoFotoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.SetTipoFotoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.SetTipoUseCaseImpl;
import pe.entel.android.plantilla.util.UtilitarioApp;
import pe.entel.android.plantilla.view.FotoView;

/**
 * Created by rtamayov on 17/04/2017.
 */
public class FotoPresenter {

    final FotoView view;

    FotoRepository fotoRepository;

    String identificador;


    public FotoPresenter(FotoView view) {
        this.view = view;
        identificador = String.valueOf(new Date().getTime());
        fotoRepository = new FotoDataRepository(view.getContext());
    }


    public void grabarFoto(byte[] data) {
        UsuarioRepository usuarioRepository = new UsuarioDataRepository(view.getContext());

        GuardarFotoUseCase guardarFotoUseCase = new GuardarFotoUseCaseImpl(fotoRepository, usuarioRepository);

        view.showloading("Grabando foto..");
        guardarFotoUseCase.ejecutar(identificador, data, new GuardarFotoUseCase.GuardarFotoCallback() {
            @Override
            public void onGuardado() {

                finGrabarPhotoOk();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                finGrabarPhotoError(errorBundle);
            }
        });

    }

    private void finGrabarPhotoError(ErrorBundle errorBundle) {
        view.hideloading();
        String mensaje = errorBundle.getErrorMessage();
        view.showError(mensaje);
        view.saveFotoError();
    }

    private void finGrabarPhotoOk() {
        view.saveFotoOk();
        view.mostrarEnvio();
        view.hideloading();
        view.hideCamera();

        prepararPreview();
    }

    public void prepararPreview() {
        try {
            ObtenerFotoRutaUseCase obtenerFotoRutaUseCase = new ObtenerFotoRutaUseCaseImpl(fotoRepository);
            String ruta = obtenerFotoRutaUseCase.ejecutar(identificador);
            Bitmap bMapRotate = UtilitarioApp.getPreview(ruta);

            if (getTipoCamara() == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                bMapRotate = UtilitarioApp.rotarMapaBits(bMapRotate, 180);
                UtilitarioApp.grabarFoto(ruta, bMapRotate);
            }

            view.preview(bMapRotate);

        } catch (Exception e) {
            view.showError(e.getMessage());
        }
    }

    public int getTipoCamara() {
        GetTipoFotoUseCase getTipoFotoUseCase = new GetTipoFotoUseCaseImpl(fotoRepository);
        return getTipoFotoUseCase.ejecutar();
    }

    public void setTipoCamara(int tipo) {
        SetTipoFotoUseCase setTipoFotoUseCase = new SetTipoUseCaseImpl(fotoRepository);
        setTipoFotoUseCase.ejecutar(tipo);
    }

    public void eliminarFoto() {
        EliminarFotoUseCase eliminarFotoUseCase = new EliminarFotoUseCaseImpl(fotoRepository);
        eliminarFotoUseCase.ejecutar(identificador);
    }

    public String getIdentificador() {
        return identificador;
    }
}
