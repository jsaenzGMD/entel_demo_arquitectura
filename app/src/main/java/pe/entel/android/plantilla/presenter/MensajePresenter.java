package pe.entel.android.plantilla.presenter;

import android.content.Intent;
import android.os.Bundle;


import pe.entel.android.plantilla.util.EntelConfig;
import pe.entel.android.plantilla.view.MensajeView;

/**
 * Created by rtamayov on 20/04/2017.
 */
public class MensajePresenter {

    final MensajeView view;

    public MensajePresenter(MensajeView view) {
        this.view = view;
    }

    public void iniciar() {
        Bundle loExtras = view.getIntent().getExtras();
        String mensaje = loExtras.getString(EntelConfig.PUSH_BUNDLE_MENSAJE);
        view.actualizarMensaje(mensaje);
    }
}
