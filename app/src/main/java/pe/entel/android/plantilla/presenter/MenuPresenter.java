package pe.entel.android.plantilla.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.controlesentel.dashboard.model.EntidadDashboard;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.data.exception.NetworkConnectionException;
import pe.entel.android.plantilla.data.repository.FotoDataRepository;
import pe.entel.android.plantilla.data.repository.MaestroDataRepository;
import pe.entel.android.plantilla.data.repository.PedidoDataRepository;
import pe.entel.android.plantilla.data.repository.ProductoDataRepository;
import pe.entel.android.plantilla.data.repository.UsuarioDataRepository;
import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.entity.Photo;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.domain.entity.Usuario;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.FotoRepository;
import pe.entel.android.plantilla.domain.repository.MaestroRepository;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;
import pe.entel.android.plantilla.domain.repository.ProductoRepository;
import pe.entel.android.plantilla.domain.repository.UsuarioRepository;
import pe.entel.android.plantilla.domain.usecase.EnviarFotoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.EnviarFotoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.EnviarPendientesUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.EnviarPendientesUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.GetUsuarioUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.GetUsuarioUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.ListaFotoPendientesUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.ListaFotoPendientesUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.ListaPedidoPendientesUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.ListaPedidoPendientesUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.ListarProductoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.ListarProductoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.ObtenerMaestrosUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.ObtenerMaestrosUseCaseImpl;
import pe.entel.android.plantilla.mapper.UsuarioModelMapper;
import pe.entel.android.plantilla.model.MenuModel;
import pe.entel.android.plantilla.model.UsuarioModel;
import pe.entel.android.plantilla.view.MenuView;

import static java.security.AccessController.getContext;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class MenuPresenter {
    final MenuView view;
    final PedidoRepository pedidoRepository;
    final FotoRepository fotoRepository;

    public MenuPresenter(MenuView view) {
        this.view = view;
        pedidoRepository = new PedidoDataRepository(view.getContext());
        fotoRepository = new FotoDataRepository(view.getContext());
    }

    public void iniciar(Context context) {
        actualizarMenu();
        generarDashboard(context);
        generarSkeeBar();
    }

    private void actualizarMenu() {
        ListaPedidoPendientesUseCase listaPedidoPendientesUseCase = new ListaPedidoPendientesUseCaseImpl(pedidoRepository);
        ListaFotoPendientesUseCase listaFotoPendientesUseCase = new ListaFotoPendientesUseCaseImpl(fotoRepository);

        List<Pedido> pedidosPendientes = listaPedidoPendientesUseCase.ejecutar();
        List<Photo> fotosPendientes = listaFotoPendientesUseCase.ejecutar();

        view.actualizarMenu(MenuModel.getMenus(pedidosPendientes.size()+fotosPendientes.size()));
    }

    private void generarDashboard(Context context) {
        view.generarDashboard("N°", "CLIENTES", llenarListDashboard(context), 0, 0);
    }

    private void generarSkeeBar() {
        view.generarSkeeBar(1200, 800);
    }

    public List<EntidadDashboard> llenarListDashboard(Context context){
        List<EntidadDashboard> listEntidadDashboard = new ArrayList<>();
        EntidadDashboard entidadDashboard1 = new EntidadDashboard(context.getResources().getColor(R.color.color_black), "Con pedido:", 32);
        listEntidadDashboard.add(entidadDashboard1);
        EntidadDashboard entidadDashboard2 = new EntidadDashboard(context.getResources().getColor(R.color.orange_dashboard), "No Pedido:", 32);
        listEntidadDashboard.add(entidadDashboard2);
        EntidadDashboard entidadDashboard3 = new EntidadDashboard(context.getResources().getColor(R.color.skyBlue_dashboard), "Por Visitar:", 64);
        listEntidadDashboard.add(entidadDashboard3);
        return  listEntidadDashboard;
    }

    public void obtenerMaestros(){
        UsuarioRepository usuarioRepository = new UsuarioDataRepository(view.getContext());
        MaestroRepository maestroRepository = new MaestroDataRepository(view.getContext());
        GetUsuarioUseCase getUsuarioUseCase = new GetUsuarioUseCaseImpl(usuarioRepository);
        ObtenerMaestrosUseCase obtenerMaestrosUseCase = new ObtenerMaestrosUseCaseImpl(maestroRepository);

        UsuarioModel usuario = UsuarioModelMapper.adapter(getUsuarioUseCase.ejecutar());

        view.showloading("Obteniendo Maestros...");
        obtenerMaestrosUseCase.ejecutar(usuario.getId(), new ObtenerMaestrosUseCase.Callback() {
            @Override
            public void onSincronizado(String strResult) {
                view.hideloading();
                view.showCorrect(strResult);
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                view.hideloading();
                String mensaje = errorBundle.getErrorMessage();

                if (mensaje == null || mensaje.equals("")) {
                    mensaje = errorBundle.getException().getClass().getName();

                    if (errorBundle.getException().getClass().isInstance(new NetworkConnectionException())) {
                        mensaje = "Fuera de cobertura";
                    }
                }
                view.showError(mensaje);
            }
        });
    }


    public void realizarAccion(int id) {

        switch (id) {
            case MenuModel.INICIO:
                iniciarPedido();
                break;

            case MenuModel.FOTO:
                view.irFoto();
                break;

            case MenuModel.PENDIENTE:
                enviarPendientes();
                break;

            case MenuModel.SINCRONIZAR:
                obtenerMaestros();
                break;

            case MenuModel.SALIR:
                view.abrirDialogoSalir();
                break;

            default:
                break;

        }

    }

    public void enviarPendientes() {
        view.showloading("Enviando pendientes..");
        EnviarPendientesUseCase enviarPendientesUseCase = new EnviarPendientesUseCaseImpl(pedidoRepository, fotoRepository);
        enviarPendientesUseCase.ejecutar(new EnviarPendientesUseCase.EnviarPendientesCalback() {
            @Override
            public void onEnviar(String mensaje) {
                view.hideloading();
                actualizarMenu();
                view.showCorrect(mensaje);
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                view.hideloading();
                String mensaje = errorBundle.getErrorMessage();

                if (mensaje == null || mensaje.equals("")) {
                    mensaje = errorBundle.getException().getClass().getName();

                    if (errorBundle.getException().getClass().isInstance(new NetworkConnectionException())) {
                        mensaje = "Fuera de cobertura";
                    }
                }
                actualizarMenu();
                view.showError(mensaje);
            }
        });



    }

    public void iniciarPedido() {
        ProductoRepository productoRepository = new ProductoDataRepository();
        ListarProductoUseCase listarProductoUseCase = new ListarProductoUseCaseImpl(productoRepository);
        List<Producto> productos = listarProductoUseCase.ejecutar(true,"");

        if (productos.size()==0){
            view.showInfo("Sincronizar los datos");
        }else{
            view.irPedido();
        }

    }

    public void enviarFoto(String identificador) {

        EnviarFotoUseCase enviarFotoUseCase = new EnviarFotoUseCaseImpl(fotoRepository);
        view.showloading("Enviando foto..");
        enviarFotoUseCase.ejecutar(identificador, new EnviarFotoUseCase.EnviarFotoCallback() {
            @Override
            public void onEnviado(String mensaje) {
                view.hideloading();
                actualizarMenu();
                view.showCorrect(mensaje);
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                view.hideloading();
                String mensaje = errorBundle.getErrorMessage();

                if (mensaje == null || mensaje.equals("")) {
                    mensaje = errorBundle.getException().getClass().getName();

                    if (errorBundle.getException().getClass().isInstance(new NetworkConnectionException())) {
                        mensaje = "Fuera de cobertura";
                    }
                }
                actualizarMenu();
                view.showError(mensaje);
            }
        });

    }
}
