package pe.entel.android.plantilla.presenter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.controlesentel.busqueda.model.EntidadBusqueda;
import pe.entel.android.controlesentel.busqueda.model.ListaBusqueda;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.data.exception.NetworkConnectionException;
import pe.entel.android.plantilla.data.repository.LocationDataRepository;
import pe.entel.android.plantilla.data.repository.PedidoDataRepository;
import pe.entel.android.plantilla.data.repository.ProductoDataRepository;
import pe.entel.android.plantilla.data.repository.UsuarioDataRepository;
import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.LocationRepository;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;
import pe.entel.android.plantilla.domain.repository.ProductoRepository;
import pe.entel.android.plantilla.domain.repository.UsuarioRepository;
import pe.entel.android.plantilla.domain.usecase.EnviarPedidoUseCase;
import pe.entel.android.plantilla.domain.usecase.GetUsuarioUseCase;
import pe.entel.android.plantilla.domain.usecase.ListarProductoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.EnviarPedidoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.GetPedidoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.GetPedidoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.SetPedidoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.GetUsuarioUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.implementation.ListarProductoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.implementation.SetPedidoUseCaseImpl;
import pe.entel.android.plantilla.mapper.PedidoModelMapper;
import pe.entel.android.plantilla.mapper.UsuarioModelMapper;
import pe.entel.android.plantilla.model.PedidoModel;
import pe.entel.android.plantilla.model.UsuarioModel;
import pe.entel.android.plantilla.navigation.Navigator;
import pe.entel.android.plantilla.view.PedidoView;

/**
 * Created by rtamayov on 13/04/2017.
 */

public class PedidoPresenter {
    final PedidoView view;
    ProductoRepository productoRepository;
    PedidoRepository pedidoRepository;
    UsuarioRepository usuarioRepository;
    PedidoModel pedido;

    public PedidoPresenter(PedidoView view) {
        this.view = view;
        productoRepository = new ProductoDataRepository();
        pedidoRepository = new PedidoDataRepository(view.getContext());
        usuarioRepository = new UsuarioDataRepository(view.getContext());
    }

    public void abandonarPedido() {
        SetPedidoUseCase setPedidoUseCase = new SetPedidoUseCaseImpl(pedidoRepository);
        setPedidoUseCase.ejecutar(null);
        view.irMenu();
    }

    public void actualizarLista() {
        GetPedidoUseCase getPedidoUseCase = new GetPedidoUseCaseImpl(pedidoRepository);
        pedido = PedidoModelMapper.adapter(getPedidoUseCase.ejecutar());

        int cantProductos = 0;
        int cantItems = 0;
        double cantMonto = 0.0;

        if(pedido !=null){
            if (pedido.getDetalles()!=null){
                for (int i = 0; i< pedido.getDetalles().size(); i++){
                    Producto item = pedido.getDetalles().get(i);
                    cantProductos ++;
                    cantItems = cantItems + Integer.valueOf(item.getCantidad());
                    cantMonto = cantMonto + Double.valueOf(item.getMonto());
                }
                view.actualizarLista(pedido.getDetalles());
            }
        }
        view.actualizarToolbar(String.valueOf(cantProductos), String.valueOf(cantItems), String.valueOf(cantMonto));
    }

    public void irProductoLista() {
        ListarProductoUseCase listarProductoUseCase = new ListarProductoUseCaseImpl(productoRepository);
        List<Producto> productos = listarProductoUseCase.ejecutar();
        List<ListaBusqueda> list = new ArrayList<>();
        EntidadBusqueda itemEntidadBusqueda;
        List<EntidadBusqueda> listEntidadBusqueda = new ArrayList<>();

        if (productos!=null){
            for(Producto producto:productos){
                itemEntidadBusqueda = new EntidadBusqueda();
                itemEntidadBusqueda.setFlagIndicador_0(true);
                itemEntidadBusqueda.setFlagIndicadorTipoDato_0(1);
                itemEntidadBusqueda.setFlagIndicadorTipoImage_0(false);
                itemEntidadBusqueda.setImageIndicador_0(R.drawable.ic_icon_producto_stock);
                itemEntidadBusqueda.setFlagIndicadorTitulo(true);
                itemEntidadBusqueda.setTitulo(producto.getNombre());
                itemEntidadBusqueda.setFlagIndicadorDetalle(true);
                itemEntidadBusqueda.setDetalle(producto.getCodigoProducto());
                itemEntidadBusqueda.setFlagLayoutIndicador(false);
                listEntidadBusqueda.add(itemEntidadBusqueda);
            }
        }else {
            itemEntidadBusqueda = new EntidadBusqueda();
            listEntidadBusqueda.add(itemEntidadBusqueda);
        }

        ListaBusqueda itemList1 = new ListaBusqueda();
        itemList1.setTitle("Búsqueda por Nombre");
        itemList1.setOrden("0");
        itemList1.setList(listEntidadBusqueda);
        itemList1.setAttibute("titulo");

        ListaBusqueda itemList2 = new ListaBusqueda();
        itemList2.setTitle("Búsqueda por Código");
        itemList2.setOrden("1");
        itemList2.setList(listEntidadBusqueda);
        itemList2.setAttibute("detalle");

        list.add(itemList1);
        list.add(itemList2);

        view.irProductoLista(list,2);
    }

    public void validarRepetido(EntidadBusqueda entidadBusqueda) {
        GetPedidoUseCase getPedidoUseCase = new GetPedidoUseCaseImpl(pedidoRepository);
        Pedido pedido = getPedidoUseCase.ejecutar();

        ListarProductoUseCase listarProductoUseCase = new ListarProductoUseCaseImpl(productoRepository);
        List<Producto> productos = listarProductoUseCase.ejecutar();

        Producto item = new Producto();
        for (Producto producto:productos){
            if(producto.getCodigoProducto().equalsIgnoreCase(entidadBusqueda.getDetalle())){
                item = producto;
                item.setEstado(1);
                item.setDescuento("12.3");
                item.setBonificacion("6.0");
                item.setStock("13");
            }
        }

        if(pedido == null){
            view.irProductoDetalle(item);
        }else{
            boolean isRepetido = pedido.existeProducto(item.getCodigoProducto());
            if(!isRepetido) {
                view.irProductoDetalle(item);
            } else {
                view.showInfo("Producto Repetido");
                irProductoLista();
            }
        }
    }

    public void verDetalle() {
        GetUsuarioUseCase getUsuarioUseCase = new GetUsuarioUseCaseImpl(usuarioRepository);
        GetPedidoUseCase getPedidoUseCase = new GetPedidoUseCaseImpl(pedidoRepository);
        if(pedido.getDetalles()!=null && pedido.getDetalles().size()>0) {
            pedido = PedidoModelMapper.adapter(getPedidoUseCase.ejecutar());
        }else{
            view.showInfo("Ingresar al menos 1 producto");
        }
        UsuarioModel usuario = UsuarioModelMapper.adapter(getUsuarioUseCase.ejecutar());
        String mensaje = "¿Está seguro que desea finalizar el registro?";

        if(pedido !=null){
            if (pedido.getDetalles()!=null){
                view.verDetalle(usuario, mensaje, pedido.getDetalles());
            }else{
                view.showInfo("Ingresar al menos 1 producto");
            }
        }else{
            view.showInfo("Ingresar al menos 1 producto");
        }
    }

    public void eliminarProducto(Producto producto) {
        GetPedidoUseCase getPedidoUseCase = new GetPedidoUseCaseImpl(pedidoRepository);
        pedido = PedidoModelMapper.adapter(getPedidoUseCase.ejecutar());

        pedido.eliminarProducto(producto.getCodigoProducto());

        SetPedidoUseCase setPedidoUseCase = new SetPedidoUseCaseImpl(pedidoRepository);
        setPedidoUseCase.ejecutar(PedidoModelMapper.revert(pedido));

        actualizarLista();
    }

    public void enviarPedido(final Context context) {
        GetPedidoUseCase getPedidoUseCase = new GetPedidoUseCaseImpl(pedidoRepository);
        pedido = PedidoModelMapper.adapter(getPedidoUseCase.ejecutar());

        if(pedido != null && pedido.getDetalles()!=null && pedido.getDetalles().size()>0) {

            LocationRepository locationRepository = new LocationDataRepository(view.getContext());

            EnviarPedidoUseCase enviarPedidoUseCase = new EnviarPedidoUseCaseImpl(pedidoRepository, locationRepository, usuarioRepository);
            view.showloading("Enviando Pedido");
            enviarPedidoUseCase.ejecutar(PedidoModelMapper.revert(pedido), new EnviarPedidoUseCase.Callback() {
                @Override
                public void onEnviar(String mensaje) {
                    view.hideloading();
                    Navigator.navigateToMensajeConfirmacionPedido(context,true,mensaje);
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    view.hideloading();
                    String mensaje = errorBundle.getErrorMessage();

                    if (mensaje == null || mensaje.equals("")) {
                        mensaje = errorBundle.getException().getClass().getName();

                        if (errorBundle.getException().getClass().isInstance(new NetworkConnectionException())) {
                            mensaje = "Fuera de cobertura";
                        }
                    }
                    Navigator.navigateToMensajeConfirmacionPedido(context,false,mensaje);
                }
            });

        }else{
            view.showInfo("Ingresar al menos 1 producto");
        }
    }
}
