package pe.entel.android.plantilla.presenter;

import android.os.Bundle;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.plantilla.data.repository.PedidoDataRepository;
import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;
import pe.entel.android.plantilla.domain.usecase.GetPedidoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.GetPedidoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.SetPedidoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.SetPedidoUseCaseImpl;
import pe.entel.android.plantilla.mapper.ProductoModelMapper;
import pe.entel.android.plantilla.model.ProductoModel;
import pe.entel.android.plantilla.view.ProductoDetalleView;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class ProductoDetallePresenter {
    final ProductoDetalleView view;

    PedidoRepository pedidoRepository;
    SetPedidoUseCase setPedidoUseCase;
    ProductoModel producto;

    public ProductoDetallePresenter(ProductoDetalleView view) {
        this.view = view;
        pedidoRepository = new PedidoDataRepository(view.getContext());
        setPedidoUseCase = new SetPedidoUseCaseImpl(pedidoRepository);
    }

    public void abandonarPedido() {
        setPedidoUseCase.ejecutar(null);
        view.irMenu();
    }


    public void iniciar() {
        Bundle loExtras = view.getIntent().getExtras();
        String strProducto = loExtras.getString(Producto.TAG);
        producto = ProductoModelMapper.StringAdapter(strProducto);
        view.iniciarPantalla(producto);

    }

    public void actualizarMonto(String trim) {
        String valor = "0";

        if(!trim.equals("")){

            Float precio = Float.valueOf(producto.getPrecio());
            Float cantidad = Float.valueOf(trim);
            valor = String.valueOf(precio * cantidad);

        }

        view.actualizarMonto(valor);
    }

    public void guardarProducto(String cantidad, String monto) {
        if(cantidad.equals("")||cantidad.equals("0")){
            view.showInfo("Ingresar cantidad mayor de 0");
        }else{
            producto.setCantidad(cantidad);
            producto.setMonto(monto);

            GetPedidoUseCase getPedidoUseCase = new GetPedidoUseCaseImpl(pedidoRepository);
            Pedido pedido = getPedidoUseCase.ejecutar();
            if (pedido == null){
                pedido = new Pedido();
            }

            List<Producto> productos = pedido.getDetalles();
            productos.add(ProductoModelMapper.EntityRevert(producto));

            pedido.setDetalles(productos);
            pedido.ordenar();

            setPedidoUseCase.ejecutar(pedido);

            view.irPedido();


        }

    }
}
