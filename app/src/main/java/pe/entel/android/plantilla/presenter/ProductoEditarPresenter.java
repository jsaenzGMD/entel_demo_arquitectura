package pe.entel.android.plantilla.presenter;

import android.os.Bundle;

import com.google.gson.Gson;

import pe.entel.android.plantilla.data.repository.PedidoDataRepository;
import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;
import pe.entel.android.plantilla.domain.usecase.GetPedidoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.GetPedidoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.SetPedidoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.SetPedidoUseCaseImpl;
import pe.entel.android.plantilla.view.ProductoEditarView;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class ProductoEditarPresenter {
    final ProductoEditarView view;

    PedidoRepository pedidoRepository;
    SetPedidoUseCase setPedidoUseCase;
    Producto producto;

    public ProductoEditarPresenter(ProductoEditarView view) {
        this.view = view;
        pedidoRepository = new PedidoDataRepository(view.getContext());
        setPedidoUseCase = new SetPedidoUseCaseImpl(pedidoRepository);
    }

    public void iniciar() {
        Gson gson = new Gson();
        Bundle loExtras = view.getIntent().getExtras();
        String strProducto = loExtras.getString(Producto.TAG);
        producto = gson.fromJson(strProducto, Producto.class);
        view.iniciarPantalla(producto);

    }

    public void abandonarPedido() {
        setPedidoUseCase.ejecutar(null);
        view.irMenu();
    }

    public void actualizarMonto(String trim) {
        String valor = "0";

        if(!trim.equals("")){

            Float precio = Float.valueOf(producto.getPrecio());
            Float cantidad = Float.valueOf(trim);
            valor = String.valueOf(precio * cantidad);

        }

        view.actualizarMonto(valor);
    }

    public void guardarProducto(String cantidad, String monto) {
        if(cantidad.equals("")||cantidad.equals("0")){
            view.showInfo("Ingresar cantidad mayor de 0");
        }else{
            producto.setCantidad(cantidad);
            producto.setMonto(monto);

            GetPedidoUseCase getPedidoUseCase = new GetPedidoUseCaseImpl(pedidoRepository);
            Pedido pedido = getPedidoUseCase.ejecutar();

            pedido.editarProducto(producto);
            pedido.ordenar();
            setPedidoUseCase.ejecutar(pedido);

            view.irPedido();

        }

    }

}
