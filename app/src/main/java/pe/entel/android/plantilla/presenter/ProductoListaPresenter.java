package pe.entel.android.plantilla.presenter;

import java.util.List;

import pe.entel.android.plantilla.data.repository.PedidoDataRepository;
import pe.entel.android.plantilla.data.repository.ProductoDataRepository;
import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;
import pe.entel.android.plantilla.domain.repository.ProductoRepository;
import pe.entel.android.plantilla.domain.usecase.GetPedidoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.GetPedidoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.ListarProductoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.ListarProductoUseCaseImpl;
import pe.entel.android.plantilla.domain.usecase.SetPedidoUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.SetPedidoUseCaseImpl;
import pe.entel.android.plantilla.view.ProductoListaView;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class ProductoListaPresenter {
    final ProductoListaView view;
    PedidoRepository pedidoRepository;
    ProductoRepository productoRepository;



    public ProductoListaPresenter(ProductoListaView view) {
        this.view = view;
        pedidoRepository = new PedidoDataRepository(view.getContext());
        productoRepository = new ProductoDataRepository();
    }

    public void abandonarPedido() {
        SetPedidoUseCase setPedidoUseCase = new SetPedidoUseCaseImpl(pedidoRepository);
        setPedidoUseCase.ejecutar(null);
        view.irMenu();
    }

    public void buscarProducto(boolean isCodigo, String busqueda) {
        ListarProductoUseCase listarProductoUseCase = new ListarProductoUseCaseImpl(productoRepository);
        List<Producto> productos = listarProductoUseCase.ejecutar(isCodigo,busqueda);
        view.actualizarProductos(productos);

    }

    public void validarRepetido(Producto producto) {
        GetPedidoUseCase getPedidoUseCase = new GetPedidoUseCaseImpl(pedidoRepository);
        Pedido pedido = getPedidoUseCase.ejecutar();

        if(pedido == null){
            view.irProductoDetalle(producto);
        }else{
            boolean isRepetido = pedido.existeProducto(producto.getCodigoProducto());
            if(!isRepetido)
                view.irProductoDetalle(producto);
            else
                view.showInfo("Producto Repetido");

        }

    }
}
