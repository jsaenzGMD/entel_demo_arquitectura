package pe.entel.android.plantilla.presenter;

import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.plantilla.data.exception.NetworkConnectionException;
import pe.entel.android.plantilla.data.repository.VersionDataRepository;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.VersionRepository;
import pe.entel.android.plantilla.domain.usecase.ObtenerVersionUseCase;
import pe.entel.android.plantilla.domain.usecase.implementation.ObtenerVersionUseCaseImpl;
import pe.entel.android.plantilla.util.UtilitarioApp;
import pe.entel.android.plantilla.view.SplashView;
import pe.entel.android.plantilla.view.widget.ToastCustom;

/**
 * Created by rtamayov on 07/04/2017.
 */
public class SplashPresenter {

    private final SplashView view;


    public SplashPresenter(SplashView view) {
        this.view = view;
    }

    public void iniciar() {
        verificarPermisos();
    }

    public void continuar() {
        iniciarSubscriptor();
        obtenerVersion();
    }

    private void iniciarSubscriptor() {
        view.iniciarSubscriptor();
    }

    private void obtenerVersion() {
        VersionRepository versionRepository = new VersionDataRepository(view.getContext());
        ObtenerVersionUseCase obtenerVersionUseCase = new ObtenerVersionUseCaseImpl(versionRepository);

        obtenerVersionUseCase.ejecutar(new ObtenerVersionUseCase.Callback() {
            @Override
            public void obtenida() {
                Log.v("PRESENTER", "obtenerVersion: " + "Suite Encontrada");
                view.irLogin();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                String mensaje = errorBundle.getErrorMessage();

                if (mensaje == null || mensaje.equals("")) {
                    mensaje = errorBundle.getException().getClass().getName();

                    if (errorBundle.getException().getClass().isInstance(new NetworkConnectionException())) {
                        mensaje = "Fuera de cobertura";
                    }
                }
                Log.e("PRESENTER", "obtenerVersion: " + mensaje);
                view.irLogin();
            }
        });

    }

    public void verificarPermisos() {
        String[] permisos = UtilitarioApp.obtenerPermisos(view.getContext());
        List<String> permisosASolicitar = new ArrayList<>();

        for (int i = 0; i < permisos.length; i++) {
            boolean isPermisoAceptado = ContextCompat.checkSelfPermission(view.getContext(), permisos[i]) == PackageManager.PERMISSION_GRANTED;

            if (!isPermisoAceptado) {
                permisosASolicitar.add(permisos[i]);
                Log.v("SplashPresenter", "Permiso no Aceptado: " + permisos[i]);
            }
        }

        if (permisosASolicitar.size() > 0) {
            String[] lstPermisos = new String[permisosASolicitar.size()];
            lstPermisos = permisosASolicitar.toArray(lstPermisos);
            view.solicitarPermiso(lstPermisos);
        } else {
            continuar();
        }
    }


    public void informarRechazo() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Existen permisos denegados, la aplicación no puede iniciar");
        UtilitarioApp.mostrarToast(view.getContext(), stringBuilder.toString(), ToastCustom.TYPE_ERROR);
        view.finalizar();
    }
}
