package pe.entel.android.plantilla.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.util.EntelConfig;
import pe.entel.android.plantilla.view.activity.MensajeActivity;
import pe.entel.android.plantilla.view.activity.MenuActivity;

/**
 * Created by rtamayov on 19/04/2017.
 */
public class MensajeNotifier extends IntentService {

    private NotificationManager ioNotifyManager;
    public final static int CODIGO_NOTIFICACION_ESTADO = 133707736;

    public MensajeNotifier() {
        super("MensajeNotifier");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ioNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String lsMensaje = intent.getStringExtra(EntelConfig.PUSH_BUNDLE_MENSAJE).substring(1);
        String lsCodMensaje = intent.getStringExtra(EntelConfig.PUSH_BUNDLE_CODMENSAJE);
        showNotificacion(lsMensaje, true);
    }

    public void showNotificacion(String mensaje, boolean flgSonido) {
        Intent loIntent = new Intent(this, MensajeActivity.class);
        loIntent.putExtra(EntelConfig.PUSH_BUNDLE_MENSAJE,mensaje);

        int liCodigoNotificacion = CODIGO_NOTIFICACION_ESTADO;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, loIntent,
                //Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                // |
                PendingIntent.FLAG_UPDATE_CURRENT
                        | PendingIntent.FLAG_ONE_SHOT
        );
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        mBuilder.setSmallIcon(R.drawable.notifi_logo_entel);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_em));

        mBuilder.setContentTitle(getString(R.string.notificacion_titulo));
        mBuilder.setColor(getResources().getColor(R.color.colorPrimaryDark));
        mBuilder.setContentText(getString(R.string.app_name));

        // The subtext, which appears under the text on newer devices.
        // This will show-up in the devices with Android 4.2 and above only
        mBuilder.setSubText(getString(R.string.notificacion_estado_sub_contenido));

        mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(mensaje));

        mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);


        Notification notificacion = buildNotification(mBuilder,flgSonido);

        ioNotifyManager.notify(liCodigoNotificacion, notificacion);

    }

    private Notification buildNotification(NotificationCompat.Builder mBuilder, boolean flgSonido) {
        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.flags |= Notification.FLAG_INSISTENT;
        notification.flags |= Notification.FLAG_NO_CLEAR;

        notification.ledARGB = Color.RED;
        notification.ledOnMS = 500;
        notification.ledOffMS = 500;
        if (flgSonido)
            notification.sound = Uri.parse("android.resource://" + getPackageName() + "/raw/nexus_4");
        else
            notification.sound=null;

        long[] liVibrador = new long[50];
        for (int i = 0; i < liVibrador.length; i++) {
            if (i % 2 > 0) {
                liVibrador[i] = 1500;
            } else {
                liVibrador[i] = 800;
            }
        }
        notification.vibrate = liVibrador;
        notification.when = System.currentTimeMillis();
        return notification;
    }
}
