package pe.entel.android.plantilla.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import pe.entel.android.plantilla.util.EntelConfig;
import pe.entel.android.plantilla.util.UtilitarioApp;

/**
 * Created by rtamayov on 19/04/2017.
 */
public class PushBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context poContext, Intent poIntent) {
        Bundle loBundle = poIntent.getExtras();

        String topic = loBundle.getString(EntelConfig.PUSH_BUNDLE_TOPICNAME);
        String topicValidacion = UtilitarioApp.fnReadParamProperty(poContext, "MQTT_TOPIC_PREFIX") + "/" + UtilitarioApp.fnNumEquipoTest(poContext);

        Log.v("PUSH", "topic; " + topic);
        Log.v("PUSH", "topicValidacion; " + topicValidacion);

        if (topicValidacion.equals(topic)) {
            String codigo = loBundle.getString(EntelConfig.PUSH_BUNDLE_CODMENSAJE);
            String mensaje = loBundle.getString(EntelConfig.PUSH_BUNDLE_MENSAJE);
            Log.v("PUSH", "codigo; " + codigo);
            Log.v("PUSH", "mensaje; " + mensaje);

            Intent loIntent = new Intent(poContext, MensajeNotifier.class);
            loIntent.putExtra(EntelConfig.PUSH_BUNDLE_MENSAJE,mensaje);
            loIntent.putExtra(EntelConfig.PUSH_BUNDLE_CODMENSAJE,codigo);
            poContext.startService(loIntent);

        }

    }
}
