package pe.entel.android.plantilla.util;

import android.hardware.Camera.Size;

import java.util.Comparator;

/**
 * Clase que implementa un {@link Comparator}[{@link Size}]
 * @author jroeder
 *
 */
public class CustomComparator implements Comparator<Size> {
	
    @Override
    public int compare(Size o1, Size o2) {
    	if (o1.width < o2.width) return 1;
    	else return -1;
    		
    }
}