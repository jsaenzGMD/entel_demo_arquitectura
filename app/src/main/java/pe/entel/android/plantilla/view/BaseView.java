package pe.entel.android.plantilla.view;

import android.content.Context;
import android.content.Intent;

/**
 * Created by rtamayov on 07/04/2017.
 */
public interface BaseView {
    Context getContext();
    Intent getIntent();
    void showloading(String message);
    void hideloading();
    void showError(String message);
    void showCorrect(String message);
    void showInfo(String message);
    void finalizar();

    void irOperadorInvalido();

    void validarSubscriptor();
    void terminarAplicacion();


}
