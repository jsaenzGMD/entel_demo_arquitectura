package pe.entel.android.plantilla.view;

import android.graphics.Bitmap;

/**
 * Created by rtamayov on 17/04/2017.
 */
public interface FotoView extends BaseView{

    void mostrarPreview();

    void hideCamera();

    void preview(Bitmap bMapRotate);

    void saveFotoOk();

    void mostrarEnvio();

    void saveFotoError();

    void showCamera();
}
