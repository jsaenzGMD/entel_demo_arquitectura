package pe.entel.android.plantilla.view;

/**
 * Created by rtamayov on 11/04/2017.
 */
public interface LoginView extends BaseView{
    void actualizarVersion(String version);

    void irPreferenciar(int perfil);

    void irMenu();

    void actualizarUsuario(String codigo, String clave);
}
