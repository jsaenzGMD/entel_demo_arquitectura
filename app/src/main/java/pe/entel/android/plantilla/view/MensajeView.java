package pe.entel.android.plantilla.view;

/**
 * Created by rtamayov on 20/04/2017.
 */
public interface MensajeView extends BaseView {
    void actualizarMensaje(String mensaje);
}
