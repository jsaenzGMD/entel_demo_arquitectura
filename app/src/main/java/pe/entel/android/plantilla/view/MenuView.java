package pe.entel.android.plantilla.view;

import java.util.List;

import pe.entel.android.controlesentel.dashboard.model.EntidadDashboard;
import pe.entel.android.plantilla.model.MenuModel;

/**
 * Created by rtamayov on 11/04/2017.
 */
public interface MenuView extends BaseView {
    void actualizarMenu(List<MenuModel> lista);

    void generarDashboard(String tituloCantidadTotal, String DescripcionCantidadTotal, List<EntidadDashboard> lista, int anchoElementoLeyenda, int altoElementoLeyenda);

    void generarSkeeBar(int montoTotal, int montoActual);

    void abrirDialogoSalir();


    void irPedido();

    void irFoto();
}
