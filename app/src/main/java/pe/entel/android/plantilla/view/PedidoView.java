package pe.entel.android.plantilla.view;

import java.util.List;

import pe.entel.android.controlesentel.busqueda.model.ListaBusqueda;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.model.ProductoModel;
import pe.entel.android.plantilla.model.UsuarioModel;

/**
 * Created by rtamayov on 13/04/2017.
 */

public interface PedidoView extends BaseView {
    void irMenu();

    void actualizarToolbar(String cantProductos, String cantItems, String cantMonto);

    void verDetalle(UsuarioModel usuario, String mensaje, List<Producto> lstProductoModels);

    void actualizarLista(List<Producto> productos);

    void irProductoLista(List<ListaBusqueda> list, int tipoList);

    void irProductoDetalle(Producto producto);
}
