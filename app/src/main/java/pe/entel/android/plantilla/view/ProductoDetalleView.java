package pe.entel.android.plantilla.view;

import pe.entel.android.plantilla.model.ProductoModel;

/**
 * Created by rtamayov on 14/04/2017.
 */

public interface ProductoDetalleView extends BaseView {
    void irMenu();

    void irPedido();

    void actualizarMonto(String valor);

    void iniciarPantalla(ProductoModel producto);
}
