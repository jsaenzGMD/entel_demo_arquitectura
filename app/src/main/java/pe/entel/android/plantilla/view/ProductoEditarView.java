package pe.entel.android.plantilla.view;

import pe.entel.android.plantilla.domain.entity.Producto;

/**
 * Created by rtamayov on 14/04/2017.
 */

public interface ProductoEditarView extends BaseView {

    void irMenu();

    void irPedido();

    void actualizarMonto(String valor);

    void iniciarPantalla(Producto producto);
}
