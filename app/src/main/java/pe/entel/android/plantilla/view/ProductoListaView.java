package pe.entel.android.plantilla.view;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Producto;

/**
 * Created by rtamayov on 14/04/2017.
 */

public interface ProductoListaView extends BaseView {
    void irMenu();

    void actualizarProductos(List<Producto> productos);

    void irProductoDetalle(Producto producto);
}
