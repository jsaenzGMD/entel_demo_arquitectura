package pe.entel.android.plantilla.view;

/**
 * Created by rtamayov on 07/04/2017.
 */
public interface SplashView extends BaseView{

    void iniciarSubscriptor();

    void irLogin();

    void solicitarPermiso(String[] permisos);
}
