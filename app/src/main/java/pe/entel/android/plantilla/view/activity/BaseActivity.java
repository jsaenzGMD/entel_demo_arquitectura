package pe.entel.android.plantilla.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import pe.entel.android.plantilla.navigation.Navigator;
import pe.entel.android.plantilla.presenter.BasePresenter;
import pe.entel.android.plantilla.util.UtilitarioApp;
import pe.entel.android.plantilla.view.BaseView;
import pe.entel.android.plantilla.view.widget.ToastCustom;
import pe.entel.android.verification.util.Service;

/**
 * Created by rtamayov on 07/04/2017.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView {


    BasePresenter presenter;
    ProgressDialog progressDialog;
    public final String TAG = getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(TAG,"Inicio Actividad");
        super.onCreate(savedInstanceState);
        presenter = new BasePresenter(this);
        presenter.iniciar();
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

    @Override
    public Intent getIntent() {
        return super.getIntent();
    }

    @Override
    public void finalizar() {
        finish();
    }

    @Override
    public void showloading(String mensaje) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(mensaje);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideloading() {
        progressDialog.dismiss();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    @Override
    public void showError(String message) {
        UtilitarioApp.mostrarToast(this, message, ToastCustom.TYPE_ERROR);
    }

    @Override
    public void showCorrect(String message) {
        UtilitarioApp.mostrarToast(this, message, ToastCustom.TYPE_CORRECTO);
    }

    @Override
    public void showInfo(String message) {
        UtilitarioApp.mostrarToast(this, message, ToastCustom.TYPE_INFO);
    }

    @Override
    public void irOperadorInvalido() {
        Navigator.navigateToOperadorInvalido(this);
    }

    @Override
    public void validarSubscriptor() {
        Service.validateActivity(this);
    }

    @Override
    public void terminarAplicacion() {
        finish();
        System.exit(0);
    }


}
