package pe.entel.android.plantilla.view.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.presenter.FotoPresenter;
import pe.entel.android.plantilla.util.EntelConfig;
import pe.entel.android.plantilla.view.FotoView;
import pe.entel.android.util.camarapersonalizada.view.fragment.media.CameraFragment;

/**
 * Created by rtamayov on 17/04/2017.
 */
public class FotoActivity extends BaseActivity implements FotoView , CameraFragment.PerformerCallback, CameraFragment.TipoCamaraCallback {

    FotoPresenter presenter;

    @InjectView(R.id.toma_foto_ib_enviar)
    ImageButton ibEnviarFoto;
    @InjectView(R.id.toma_foto_iv_preview)
    ImageView ivPreview;

    CameraFragment cameraFragment;

    private boolean grabada = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto);
        ButterKnife.inject(this);
        cameraFragment = (CameraFragment)getSupportFragmentManager().findFragmentById(R.id.toma_foto_camera_fragment);
        presenter = new FotoPresenter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(grabada){
            presenter.prepararPreview();
            ibEnviarFoto.setVisibility(View.VISIBLE);
        }else{
            ibEnviarFoto.setVisibility(View.GONE);
        }
    }

    @Override
    public void whenIsInPreview() {
        ibEnviarFoto.setVisibility(ImageView.VISIBLE);
    }

    @Override
    public void savePhoto(byte[] data) {
        Log.v(TAG,"savePhoto");
        presenter.grabarFoto(data);
    }

    @Override
    public int getTipoCamara() {
        return presenter.getTipoCamara();
    }

    @Override
    public void setTipoCamara(int tipo) {
        presenter.setTipoCamara(tipo);
    }

    @Override
    public void mostrarPreview() {
        ibEnviarFoto.setVisibility(ImageView.VISIBLE);
    }

    @Override
    public void hideCamera() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
        if (!cameraFragment.isHidden()) {
            ft.hide(cameraFragment);
        }
        ft.commit();
    }

    @Override
    public void preview(Bitmap bMapRotate) {
        ivPreview.setVisibility(ImageView.VISIBLE);
        ivPreview.setImageBitmap(null);
        ivPreview.setImageBitmap(bMapRotate);
        ivPreview.invalidate();
    }

    @Override
    public void saveFotoOk() {
        grabada = true;
    }

    @Override
    public void mostrarEnvio() {
        whenIsInPreview();
    }

    @Override
    public void saveFotoError() {
        grabada = false;
        ibEnviarFoto.setVisibility(ImageView.GONE);
    }

    @Override
    public void showCamera() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
        if (cameraFragment.isHidden()) {
            ft.show(cameraFragment);
        }
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        presenter.eliminarFoto();
        super.onBackPressed();
    }

    @OnClick(R.id.toma_foto_ib_enviar)
    public void enviarFoto(){
        Intent intent = new Intent();
        intent.putExtra(EntelConfig.IDENTIFICADOR,presenter.getIdentificador());
        setResult(RESULT_OK, intent);
        finish();
    }

    public static Intent getCallingIntent(Context context){
        return new Intent(context,FotoActivity.class);
    }
}
