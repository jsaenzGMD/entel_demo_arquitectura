package pe.entel.android.plantilla.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.navigation.Navigator;
import pe.entel.android.plantilla.presenter.LoginPresenter;
import pe.entel.android.plantilla.view.LoginView;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class LoginActivity extends BaseActivity implements LoginView {

    LoginPresenter presenter;

    @InjectView(R.id.loginUser_txtUsuario)
    TextInputEditText txtUsuario;
    @InjectView(R.id.loginUser_txtPassword)
    TextInputEditText txtClave;
    @InjectView(R.id.login_version)
    TextView txtVersion;

    private final int CONSMENCONFIGURACION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);
        presenter = new LoginPresenter(this);
        presenter.iniciar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, CONSMENCONFIGURACION, 0,
                getString(R.string.login_description_administracion)).setIcon(
                R.drawable.ic_my_library_books);
        return true;
    }


    @OnClick(R.id.login_btn_ingresar)
    public void validarUsuario() {

        String codigo = txtUsuario.getText().toString();
        String clave = txtClave.getText().toString();
        presenter.validarUsuario(codigo, clave);
    }

    @OnClick(R.id.login_config)
    public void presionarAdministracion() {
        presenter.validarPerfil(txtUsuario.getText().toString(), txtUsuario.getText().toString());
    }

    @Override
    public void actualizarVersion(String version) {
        txtVersion.setText(version);
    }

    @Override
    public void irPreferenciar(int perfil) {
        Navigator.navigateToPreferencias(this,perfil);
    }

    @Override
    public void irMenu() {
        Navigator.navigateToMenu(this);
    }

    @Override
    public void actualizarUsuario(String codigo, String clave) {
        txtUsuario.setText(codigo);
        txtClave.setText(clave);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case CONSMENCONFIGURACION:
                presenter.validarPerfil(txtUsuario.getText().toString(), txtUsuario.getText().toString());
                break;

            default:
                break;
        }
        return false;
    }

    public static Intent getCallingIntent(Context context){
        return new Intent(context,LoginActivity.class);
    }
}
