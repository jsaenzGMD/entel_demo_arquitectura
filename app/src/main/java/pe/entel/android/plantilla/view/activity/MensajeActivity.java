package pe.entel.android.plantilla.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.presenter.MensajePresenter;
import pe.entel.android.plantilla.presenter.MenuPresenter;
import pe.entel.android.plantilla.view.MensajeView;

/**
 * Created by rtamayov on 20/04/2017.
 */
public class MensajeActivity extends BaseActivity implements MensajeView{


    MensajePresenter presenter;

    @InjectView(R.id.mensaje_texto)
    TextView txtMensaje;

    @InjectView(R.id.mensaje_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensaje);
        ButterKnife.inject(this);
        getViews();
        presenter = new MensajePresenter(this);

        presenter.iniciar();


    }

    private void getViews() {
        configToolBar();
    }

    private void configToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Mensaje");
        toolbar.setTitleTextColor(getResources().getColor(R.color.color_white));
    }


    @Override
    public void actualizarMensaje(String mensaje) {
        txtMensaje.setText(mensaje);
    }

    @OnClick(R.id.mensaje_btn)
    public void finalizarMensaje() {
        finish();
    }

    public static Intent getCallingIntent(Context context){
        return new Intent(context,MensajeActivity.class);
    }
}
