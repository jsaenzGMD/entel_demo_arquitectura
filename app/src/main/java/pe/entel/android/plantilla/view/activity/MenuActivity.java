package pe.entel.android.plantilla.view.activity;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pe.entel.android.controlesentel.dashboard.model.EntidadDashboard;
import pe.entel.android.controlesentel.dashboard.view.fragment.DashboardFragment;
import pe.entel.android.controlesentel.generic.view.fragment.SkeeBarFragment;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.model.MenuModel;
import pe.entel.android.plantilla.navigation.Navigator;
import pe.entel.android.plantilla.presenter.MenuPresenter;
import pe.entel.android.plantilla.util.EntelConfig;
import pe.entel.android.plantilla.view.MenuView;
import pe.entel.android.plantilla.view.adapter.MenuPrincipalAdapter;

import static java.security.AccessController.getContext;


/**
 * Created by rtamayov on 11/04/2017.
 */
public class MenuActivity extends BaseActivity implements MenuView, NavigationView.OnNavigationItemSelectedListener {

    MenuPresenter presenter;


    @InjectView(R.id.drawer_layout)
    DrawerLayout drawer;

    ActionBarDrawerToggle toggle;

    @InjectView(R.id.nav_view)
    NavigationView navigationView;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.menuPrincipal_text)
    TextView text;

    @InjectView(R.id.menu_recyclerPincipal)
    RecyclerView recyclerPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.inject(this);
        getViews();
        presenter = new MenuPresenter(this);
        presenter.iniciar(getApplicationContext());
    }

    private void getViews() {
        configToolBar();
    }

    private void configToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_titulo));

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    /**
     * Inflar la barra lateral con las opciones de menú asignadas.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options_lateral, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @SuppressWarnings("StatementWithEmptyBody")

    /**
     * Se muestran las opciones que se tendrá en la barra lateral, y el fragment correspondiente por cada una de éstas.
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_inicio) {
            presenter.iniciarPedido();
        } else if (id == R.id.nav_foto) {
            this.irFoto();
        } else if (id == R.id.nav_sincronizar) {
            presenter.obtenerMaestros();
        } else if (id == R.id.nav_pendientes) {
            presenter.enviarPendientes();
        } else if (id == R.id.nav_salir) {
            abrirDialogoSalir();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void generarDashboard(String tituloCantidadTotal, String DescripcionCantidadTotal, List<EntidadDashboard> lista, int anchoElementoLeyenda, int altoElementoLeyenda) {

        /**
         * Este método permite generar el dahboard y la leyenda de los elementos que éste incluye, además se mostrará el SkkeeBar en la parte inferior del dashboard, para ello se le enviará el montoTotal y el montoActual.
         */
        try {
            FragmentTransaction ftGrafico = getFragmentManager().beginTransaction();
            ftGrafico.replace(R.id.menuPrincipal_grafico, DashboardFragment.newInstance(getContext(), tituloCantidadTotal, DescripcionCantidadTotal, lista, anchoElementoLeyenda, altoElementoLeyenda));
            ftGrafico.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void generarSkeeBar(int montoTotal, int montoActual) {

        /**
         * Se ingresan los mosntos como:
         * montoTotal = Cantidad total en el SkeeBar
         * montoActual = Monto actual el cual mostrará la porción ocupada del monto total.
         */
        try {
            FragmentTransaction ftSkeeBar = getFragmentManager().beginTransaction();
            ftSkeeBar.replace(R.id.menuPrincipal_skeeBar, SkeeBarFragment.newInstance(montoTotal, montoActual));
            ftSkeeBar.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void actualizarMenu(List<MenuModel> lista) {

        try {
            recyclerPrincipal.setHasFixedSize(true);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerPrincipal.setLayoutManager(layoutManager);

            MenuPrincipalAdapter menuPrincipalAdapter = new MenuPrincipalAdapter(getApplicationContext(), lista, new MenuPrincipalAdapter.onItemClickListener() {
                @Override
                public void opcionSeleccionada(MenuModel menuModel) {
                    presenter.realizarAccion(menuModel.getId());
                }
            });
            recyclerPrincipal.setAdapter(menuPrincipalAdapter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void abrirDialogoSalir() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        irLogin();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Html.fromHtml("<font color='#696969'>"+"¿Desea salir de la aplicación?"+"</font>")).setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public void irLogin() {
        finish();
        Navigator.navigateToLoginSalir(this);
    }

    @Override
    public void irPedido() {
        Navigator.navigateToPedido(this);
    }

    @Override
    public void irFoto() {
        Navigator.navigateToFoto(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EntelConfig.CONSIDCAMARA && resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            Log.v(TAG,"CONSIDCAMARA && RESULT_OK");

            String identificador = bundle.getString(EntelConfig.IDENTIFICADOR);
            presenter.enviarFoto(identificador);

        } else {
            showError(getString(R.string.string_mensaje_se_cancelo_toma_foto));

        }
    }

    public static Intent getCallingIntent(Context context){
        return new Intent(context,MenuActivity.class);
    }
}
