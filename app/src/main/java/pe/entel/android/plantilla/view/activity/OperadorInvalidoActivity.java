package pe.entel.android.plantilla.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import pe.entel.android.plantilla.R;

/**
 * Created by asoto on 20/02/2015.
 */
public class OperadorInvalidoActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operador_invalido);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public static Intent getCallingIntent(Context context){
        return new Intent(context,OperadorInvalidoActivity.class);
    }
}
