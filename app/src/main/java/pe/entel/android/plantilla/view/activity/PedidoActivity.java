package pe.entel.android.plantilla.view.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pe.entel.android.controlesentel.busqueda.model.EntidadBusqueda;
import pe.entel.android.controlesentel.busqueda.model.ListaBusqueda;
import pe.entel.android.controlesentel.multiselect.model.EntidadMultiSelect;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.model.Producto1Model;
import pe.entel.android.plantilla.model.ProductoModel;
import pe.entel.android.plantilla.model.UsuarioModel;
import pe.entel.android.plantilla.navigation.Navigator;
import pe.entel.android.plantilla.presenter.PedidoPresenter;
import pe.entel.android.plantilla.view.PedidoView;
import pe.entel.android.plantilla.view.adapter.CarritoAdapter;
import pe.entel.android.plantilla.view.adapter.ListaLayoutManager;
import pe.entel.android.plantilla.view.dialog.PedidoFinFormularioDialog;

/**
 * Created by rtamayov on 13/04/2017.
 */

public class PedidoActivity extends BaseActivity implements PedidoView, View.OnLongClickListener, View.OnClickListener {

    PedidoPresenter presenter;

    @InjectView(R.id.collapsing_toolbar_pedido)
    CollapsingToolbarLayout collapsingToolbar;
    @InjectView(R.id.toolbar_pedido)
    Toolbar toolbar;
    @InjectView(R.id.tvResCantidadProductos)
    TextView tvResCantidadProductos;
    @InjectView(R.id.ivIcono)
    ImageView ivIcono;
    @InjectView(R.id.tvDesCantidadProductos)
    TextView tvDesCantidadProductos;
    @InjectView(R.id.tvDesTotalItems)
    TextView tvDesTotalItems;
    @InjectView(R.id.tvResTotalItems)
    TextView tvResTotalItems;
    @InjectView(R.id.tvDesMontoTotal)
    TextView tvDesMontoTotal;
    @InjectView(R.id.tvResMontoTotal)
    TextView tvResMontoTotal;

    @InjectView(R.id.pedido_lista)
    RecyclerView lstProductos;
    @InjectView(R.id.pedido_fab)
    FloatingActionButton pedido_fab;
    @InjectView(R.id.fab_opcion_pedidos)
    FloatingActionButton fab_opcion_pedidos;
    @InjectView(R.id.fab_opcion_NoVisita)
    FloatingActionButton fab_opcion_NoVisita;
    @InjectView(R.id.fab_opcion_Cobranza)
    FloatingActionButton fab_opcion_Cobranza;
    @InjectView(R.id.layoutFabPedidos)
    LinearLayout layoutFabPedidos;
    @InjectView(R.id.layoutFabNoVisita)
    LinearLayout layoutFabNoVisita;
    @InjectView(R.id.layoutFabCobranza)
    LinearLayout layoutFabCobranza;
    @InjectView(R.id.btnRigthBottomActivity)
    Button btnRigthBottomActivity;
    @InjectView(R.id.btnLeftBottomActivity)
    Button btnLetfBottomActivity;

    @InjectView(R.id.includeLayoutFloatButton)
            LinearLayout includeLayoutFloatButton;
    @InjectView(R.id.nestedScrollLista)
    NestedScrollView nestedScrollLista;

    ListaLayoutManager listaLayoutManager;
    CarritoAdapter pedidoProductoAdapter;
    private boolean fabExpanded = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);
        ButterKnife.inject(this);
        presenter = new PedidoPresenter(this);
        pedido_fab.setOnLongClickListener(this);
        layoutFabPedidos.setOnClickListener(this);
        layoutFabNoVisita.setOnClickListener(this);
        layoutFabCobranza.setOnClickListener(this);
        btnRigthBottomActivity.setOnClickListener(this);
        btnLetfBottomActivity.setOnClickListener(this);

        boolean mIsScrolling;
        boolean mIsTouching;

        nestedScrollLista.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY+10 > oldScrollY+10) {
                    YoYo.with(Techniques.FadeOut)
                            .duration(900)
                            .playOn(includeLayoutFloatButton);
                    includeLayoutFloatButton.setVisibility(View.GONE);
                } else {
                    if (includeLayoutFloatButton.getVisibility() != View.VISIBLE) {
                        YoYo.with(Techniques.FadeInUp)
                                .duration(900)
                                .playOn(includeLayoutFloatButton);

                    }
                    includeLayoutFloatButton.setVisibility(View.VISIBLE);
                }
            }
        });

        getViews();
        closeSubMenusFab();
    }

    private void getViews() {
        configToolBar();
        listaLayoutManager = new ListaLayoutManager(this);
        if(lstProductos != null)
            lstProductos.setLayoutManager(listaLayoutManager);
    }


    private void configToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.action_bar_home);
        toolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirDialogoTerminar();
            }
        });
        collapsingToolbar.setTitle(getString(R.string.pedido_titulo));
        collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.color_white));
        collapsingToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        ivIcono.setImageResource(R.drawable.ic_canasta);
        tvDesCantidadProductos.setText("Productos");
        tvDesTotalItems.setText("Total de Items");
        tvDesMontoTotal.setText("Monto Total");
    }

    @Override
    public void actualizarToolbar(String cantProductos, String cantItems, String cantMonto){
        tvResCantidadProductos.setText(cantProductos);
        tvResTotalItems.setText(cantItems);
        tvResMontoTotal.setText("S/."+cantMonto);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pedido, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                super.onOptionsItemSelected(item);
                break;
        }
        return true;
    }

    private void abrirDialogoTerminar() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        presenter.abandonarPedido();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Html.fromHtml("<font color='#696969'>"+"Si regresa, perdera la informacion ingresada. Desea continuar?"+"</font>")).setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void irMenu() {
        Navigator.navigateToMenu(this);
    }

    @Override
    public void actualizarLista(List<Producto> productos) {
        if(productos!=null){
            if(pedidoProductoAdapter == null){
                pedidoProductoAdapter = new CarritoAdapter(this, productos);
            }else{
                pedidoProductoAdapter.setProductos(productos);
            }
            pedidoProductoAdapter.setOnItemClickListener(new CarritoAdapter.OnItemClickListener() {
                @Override
                public void onProductoItemEditar(Producto producto) {
                    editarProducto(producto);
                }
                @Override
                public void onProductoItemEliminar(Producto producto) {
                    eliminarProducto(producto);
                }
            });
            lstProductos.setAdapter(pedidoProductoAdapter);

        }
    }

    private void editarProducto(Producto producto) {
        Navigator.navigateToProductoEditarWithProduct(this,producto);
    }

    private void eliminarProducto(Producto producto) {
        presenter.eliminarProducto(producto);
    }

    @OnClick(R.id.pedido_fab)
    public void agregarPedido() {
        if (fabExpanded == true){
            closeSubMenusFab();
        } else {
            openSubMenusFab();
        }
    }

    private void closeSubMenusFab(){
        layoutFabPedidos.setVisibility(View.INVISIBLE);
        layoutFabNoVisita.setVisibility(View.INVISIBLE);
        layoutFabCobranza.setVisibility(View.INVISIBLE);
        pedido_fab.setImageResource(R.drawable.float_button_icon_add);
        fabExpanded = false;
    }

    private void openSubMenusFab(){
        layoutFabPedidos.setVisibility(View.VISIBLE);
        layoutFabNoVisita.setVisibility(View.VISIBLE);
        layoutFabCobranza.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.FadeIn)
                .duration(400)
                .playOn(layoutFabPedidos);
        YoYo.with(Techniques.FlipInX)
                .duration(400)
                .playOn(layoutFabPedidos);
        YoYo.with(Techniques.FadeIn)
                .duration(400)
                .playOn(layoutFabNoVisita);
        YoYo.with(Techniques.FlipInX)
                .duration(400)
                .playOn(layoutFabNoVisita);
        YoYo.with(Techniques.FadeIn)
                .duration(400)
                .playOn(layoutFabCobranza);
        YoYo.with(Techniques.FlipInX)
                .duration(300)
                .playOn(layoutFabCobranza);

        pedido_fab.setImageResource(R.drawable.float_button_icon_close);

        fabExpanded = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.actualizarLista();
    }

    public static Intent getCallingIntent(Context context){
        return new Intent(context,PedidoActivity.class);
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public void onClick(View v) {
        closeSubMenusFab();
        switch (v.getId()){
            case R.id.layoutFabPedidos:
                presenter.irProductoLista();
                break;
            case R.id.layoutFabNoVisita:
                presenter.irProductoLista();
                break;
            case R.id.layoutFabCobranza:
                presenter.irProductoLista();
                break;
            case R.id.btnRigthBottomActivity:
                presenter.verDetalle();
                break;
            case R.id.btnLeftBottomActivity:
                presenter.verDetalle();
                break;
        }

    }

    @Override
    public void irProductoLista(List<ListaBusqueda> list, int tipoList){
        Navigator.navigateToProductoLista(this,list,tipoList);
    }

    @Override
    public void irProductoDetalle(Producto producto) {
        Navigator.navigateToProductoDetalleWithProduct(this,producto);
    }

    @Override
    public void verDetalle(UsuarioModel usuario, String mensaje, List<Producto> lstProductoModels){

        if(lstProductoModels.size() >0) {

            PedidoFinFormularioDialog pedidoFinFormularioDialog = new PedidoFinFormularioDialog(PedidoActivity.this, usuario, mensaje, lstProductoModels, new PedidoFinFormularioDialog.OnPedidoFinClickListener() {
                @Override
                public void onButtonClick(List<Producto> listProductoModelList) {
                    presenter.enviarPedido(PedidoActivity.this);
                }
            });
            pedidoFinFormularioDialog.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 &&
                resultCode == RESULT_OK) {

            Bundle bundle = data.getExtras();
            int tipoList = bundle.getInt("tipoList");
            int tipoClick = bundle.getInt("tipoClick");
            EntidadBusqueda item = (EntidadBusqueda) bundle.getSerializable("item");
            int position = bundle.getInt("position");

            if (tipoList == 2) {
                if (tipoClick == 1) {
                    presenter.validarRepetido(item);
                } else if (tipoClick == 2) {
                    Toast.makeText(getContext(), item.getTitulo().toString() + "-" + position + "-click Long - TABS", Toast.LENGTH_SHORT).show();
                }
            }

        }else if (requestCode == 100 &&
                resultCode == RESULT_OK) {
            irMenu();
        }else if (requestCode == 100 && resultCode ==0){
            irMenu();
        }
    }
}
