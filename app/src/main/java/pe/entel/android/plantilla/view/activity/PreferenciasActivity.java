package pe.entel.android.plantilla.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import java.util.Properties;

import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.data.net.Url;
import pe.entel.android.plantilla.navigation.Navigator;
import pe.entel.android.plantilla.util.EntelConfig.EnumRolPreferencia;
import pe.entel.android.plantilla.util.UtilitarioApp;
import pe.entel.android.verification.util.Service;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class PreferenciasActivity extends PreferenceActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onCreatePreferenceFragment();
    }

    private void onCreatePreferenceFragment() {
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferenciasFragment())
                .commit();
    }

    public static class PreferenciasFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

        SharedPreferences ioSharedPreferences;
        private EditTextPreference txtUrl;
        private Preference preActualizar;
        private String isUrlActualizar;
        private String isIpServer;

        public PreferenciasFragment() {

        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferencias);

            Bundle loExtras = getActivity().getIntent().getExtras();
            Properties loProperties = UtilitarioApp.readProperties(getActivity());

            isIpServer = loProperties.getProperty("IP_SERVER");
            isUrlActualizar = loProperties.getProperty("URL_ACTUALIZAR");

            int liEnuOrdinal = loExtras.getInt(getString(R.string.sp_permiso), 1);

            EnumRolPreferencia loEnumRolPreferencia = EnumRolPreferencia.values()[liEnuOrdinal];
            preActualizar = getPreferenceScreen().findPreference(getString(R.string.keypre_actualizar));
            preActualizar.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference arg0) {
                    UtilitarioApp.mostrarToast(getActivity(), "Descargando actualización");
                    Navigator.navigateToUpdateApp(getActivity(),isUrlActualizar);
                    return true;
                }
            });


            txtUrl = (EditTextPreference) getPreferenceScreen().findPreference(getString(R.string.keypre_url));


            if (txtUrl == null || txtUrl.getText() == null || txtUrl.getText().equals("")) {
                txtUrl.setText(isIpServer);
            }

            ioSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            if (loEnumRolPreferencia == EnumRolPreferencia.USUARIO) {
                txtUrl.setEnabled(false);
            }
        }


        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if(key.equals(getString(R.string.keypre_url)))
            {
                txtUrl.setSummary(ioSharedPreferences.getString(key, isIpServer));
                Service.updatePreferenceRuta(getActivity(), Url.obtenerRuta(getActivity(), Url.EnumUrl.VERIFICAR));

            }
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }


    }
    public static Intent getCallingIntent(Context context){
        return new Intent(context,PreferenciasActivity.class);
    }

}
