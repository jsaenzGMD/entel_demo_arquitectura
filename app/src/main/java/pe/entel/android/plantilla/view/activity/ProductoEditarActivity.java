package pe.entel.android.plantilla.view.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import pe.entel.android.controlesentel.controlesDinamicos.view.EditTextCustom;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.model.ProductoModel;
import pe.entel.android.plantilla.navigation.Navigator;
import pe.entel.android.plantilla.presenter.ProductoDetallePresenter;
import pe.entel.android.plantilla.presenter.ProductoEditarPresenter;
import pe.entel.android.plantilla.view.ProductoEditarView;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class ProductoEditarActivity extends BaseActivity implements ProductoEditarView, TextWatcher {

    ProductoEditarPresenter presenter;

    @InjectView(R.id.producto_detalle_toolbar)
    Toolbar toolbar;

    @InjectView(R.id.productodetalle_txtNombre)
    TextView txtNombre;

    @InjectView(R.id.productodetalle_txtCodigo)
    TextView txtCodigo;

    @InjectView(R.id.productodetalle_txtStock)
    TextView txtStock;

    @InjectView(R.id.productodetalle_editTxtPrecio)
    EditTextCustom editTxtPrecio;

    @InjectView(R.id.productodetalle_editTxtCantidad)
    EditTextCustom editTxtCantidad;

    @InjectView(R.id.productodetalle_editTxtBonificacion)
    EditTextCustom editTxtBonificacion;

    @InjectView(R.id.productodetalle_editTxtSubTotal)
    EditTextCustom editTxtSubTotal;

    @InjectView(R.id.productodetalle_editTxtDescuento)
    EditTextCustom editTxtDescuento;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto_detalle);
        ButterKnife.inject(this);
        getViews();
        presenter = new ProductoEditarPresenter(this);
        presenter.iniciar();
        editTxtCantidad.getEditTextCustom().addTextChangedListener(this);
    }

    private void getViews() {
        configToolBar();
    }

    private void configToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.productoeditar_titulo));
        toolbar.setNavigationIcon(R.drawable.action_bar_home);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirDialogoTerminar();
            }
        });
        toolbar.setTitleTextColor(getResources().getColor(R.color.color_white));
    }

    private void abrirDialogoTerminar() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        presenter.abandonarPedido();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Html.fromHtml("<font color='#696969'>"+"Si regresa, perdera la informacion ingresada. Desea continuar?"+"</font>")).setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public void iniciarPantalla(Producto producto) {
        editTxtPrecio.setMaxLength(20);
        editTxtCantidad.setMaxLength(20);
        editTxtBonificacion.setMaxLength(20);
        editTxtSubTotal.setMaxLength(20);
        editTxtDescuento.setMaxLength(20);

        txtCodigo.setText(producto.getCodigoProducto());
        txtNombre.setText(producto.getNombre());
        txtStock.setText(producto.getStock());
        editTxtPrecio.getEditTextCustom().setText(producto.getPrecio());
        editTxtCantidad.getEditTextCustom().setText(producto.getCantidad());
        editTxtBonificacion.getEditTextCustom().setText(producto.getBonificacion());
        editTxtDescuento.getEditTextCustom().setText(producto.getDescuento());

        editTxtPrecio.getEditTextCustom().setEnabled(false);
        editTxtBonificacion.getEditTextCustom().setEnabled(false);
        editTxtSubTotal.getEditTextCustom().setEnabled(false);
        editTxtDescuento.getEditTextCustom().setEnabled(false);
        presenter.actualizarMonto(editTxtCantidad.getEditTextCustom().getText().toString().trim());
    }

    @Override
    public void actualizarMonto(String valor) {
        editTxtSubTotal.getEditTextCustom().setText(valor);
    }


    @OnClick(R.id.productopedido_btnfinalizar)
    public void guardarProducto(){
        presenter.guardarProducto(editTxtCantidad.getEditTextCustom().getText().toString().trim(),editTxtSubTotal.getEditTextCustom().getText().toString().trim());
    }

    @Override
    public void irMenu() {
        Navigator.navigateToMenuWithFlag(this,Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }

    @Override
    public void irPedido() {
        Navigator.navigateToPedidoWhithFlag(this,Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }

    public static Intent getCallingIntent(Context context){
        return new Intent(context,ProductoEditarActivity.class);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        presenter.actualizarMonto(editTxtCantidad.getEditTextCustom().getText().toString().trim());
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
