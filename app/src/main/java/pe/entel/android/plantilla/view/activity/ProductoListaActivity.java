package pe.entel.android.plantilla.view.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pe.entel.android.controlesentel.busqueda.model.EntidadBusqueda;
import pe.entel.android.controlesentel.busqueda.model.ListaBusqueda;
import pe.entel.android.controlesentel.busqueda.view.activity.BusquedaActivity;
import pe.entel.android.controlesentel.busqueda.view.fragment.SearchFragment;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.navigation.Navigator;
import pe.entel.android.plantilla.presenter.ProductoListaPresenter;
import pe.entel.android.plantilla.util.UtilitarioApp;
import pe.entel.android.plantilla.view.ProductoListaView;
import pe.entel.android.plantilla.view.adapter.ProductoAdapter;
import pe.entel.android.plantilla.view.adapter.ListaLayoutManager;
import pe.entel.android.plantilla.view.widget.ToastCustom;
import pe.entel.android.verification.util.Service;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class ProductoListaActivity extends BusquedaActivity implements ProductoListaView {
    ProductoListaPresenter presenter;

    @InjectView(R.id.producto_lista_toolbar)
    Toolbar toolbar;
    @InjectView(R.id.productolista_txtTexto)
    EditText txtTexto;
    @InjectView(R.id.productolista_rbtCodigo)
    RadioButton rbtCodigo;
    @InjectView(R.id.productolista_rbtNombre)
    RadioButton rbtNombre;
    @InjectView(R.id.productolista_lista)
    RecyclerView lstProductos;

    ListaLayoutManager listaLayoutManager;
    ProductoAdapter productoAdapter;
    ProgressDialog progressDialog;


    int PosicionTab = 0;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private static String[] TITLES;
    private int COUNT_OPTIONS_TOOLBAR;
    private List<ListaBusqueda> list = null;
    private int tipoList;
    private TabLayout tabLayout;
    private LinearLayout tabLayoutSingle;
    private TextView titleSingle;

    private Map<Integer, SearchFragment> fragments = new HashMap<Integer, SearchFragment>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);
        ButterKnife.inject(this);
        //getViews();
        presenter = new ProductoListaPresenter(this);

        invalidateOptionsMenu();

        titleSingle = (TextView) findViewById(pe.entel.android.controlesentel.R.id.idTitleSingle);
        Intent intent = getIntent();
        list = (List<ListaBusqueda>) intent.getSerializableExtra(paramList);
        tipoList = intent.getIntExtra(paramTipoList,1);

        tabLayout = (TabLayout) findViewById(pe.entel.android.controlesentel.R.id.tab_layout);
        tabLayoutSingle = (LinearLayout) findViewById(pe.entel.android.controlesentel.R.id.tab_layout_single);

        TITLES = new String[list.size()];
        if (list.size() == 1) {//Un solo fragment
            TITLES = new String[]{list.get(0).getTitle()};
            COUNT_OPTIONS_TOOLBAR = TITLES.length;

            tabLayout.setVisibility(View.GONE);
            tabLayoutSingle.setVisibility(View.GONE);
        } else if (list.size() > 1) {//Mas de un fragment
            for (int i = 0; i < list.size(); i++) {
                ListaBusqueda item = list.get(i);
                TITLES[i] = item.getTitle();
            }
            COUNT_OPTIONS_TOOLBAR = TITLES.length;

            tabLayout.setVisibility(View.VISIBLE);
            tabLayoutSingle.setVisibility(View.GONE);
        }


        for (int i = 0; i < list.size(); i++) {
            ListaBusqueda item = list.get(i);

            SearchFragment mFragment = SearchFragment.newInstance(i, 1, item.getList(), item.getAttibute());

            mFragment.mListener = new SearchFragment.OnListFragmentInteractionListener() {
                @Override
                public void onListFragmentClickInteraction(EntidadBusqueda item) {
                    Intent intent=new Intent();
                    intent.putExtra("tipoList", tipoList);
                    intent.putExtra("tipoClick", 1);
                    intent.putExtra("item", (Serializable) item);
                    intent.putExtra("position", PosicionTab);
                    setResult(RESULT_OK, intent);
                    onBackPressed();
                }

                @Override
                public void onListFragmentPressedInteraction(EntidadBusqueda item) {
                    Intent intent=new Intent();
                    intent.putExtra("tipoList", tipoList);
                    intent.putExtra("tipoClick", 2);
                    intent.putExtra("item", (Serializable) item);
                    intent.putExtra("position", PosicionTab);
                    setResult(RESULT_OK, intent);
                    onBackPressed();
                }
            };
            fragments.put(i, mFragment);
            /*FRAGMENT TAB*/
        }

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), COUNT_OPTIONS_TOOLBAR, fragments);
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(pe.entel.android.controlesentel.R.id.pager);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(mViewPager);

        /**Detectar tabs seleccionados*/
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        ///cambios
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                PosicionTab = position;
                String value = searchView.getQuery().toString();
                fragments.get(PosicionTab).filter(value);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        customIconTab();
        //ingrese cuando es version menor a 21
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            initTab();
        }
    }

    /**
     * Cambiar el nombre de los Tabs que contienen a cada uno de los fragments, esto se usa en una version menor al sdk 21
     */
    void initTab() {
        if (COUNT_OPTIONS_TOOLBAR == 1) {
            titleSingle.setText(TITLES[0].toString());
        } else if (COUNT_OPTIONS_TOOLBAR < 1) {
            for (int i = 0; i < COUNT_OPTIONS_TOOLBAR; i++) {
                View view = tabLayout.getTabAt(i).getCustomView();
                ((TextView) view.findViewById(pe.entel.android.controlesentel.R.id.tab_options)).setText(TITLES[i].toString());
            }
        }
    }

    /**
     * Cambiar el nombre de los Tabs que contienen a cada uno de los fragments, esto se usa en una version mayor al sdk 21
     */
    public void customIconTab() {
        for (int i = 0; i < COUNT_OPTIONS_TOOLBAR; i++) {
            View view = getLayoutInflater().from(this).inflate(pe.entel.android.controlesentel.R.layout.tab_item_selected_toolbar, null, false);
            TextView textView = (TextView) view.findViewById(pe.entel.android.controlesentel.R.id.tab_options);
            textView.setText(TITLES[i].toString());
            tabLayout.getTabAt(i).setCustomView(view);
        }
    }
    /**
     * Genera la cantidad de Fragments a mostrar.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;
        Map<Integer, SearchFragment> response;

        public SectionsPagerAdapter(FragmentManager fm, int mNumOfTabs, Map<Integer, SearchFragment> response) {
            super(fm);
            this.mNumOfTabs = mNumOfTabs;
            this.response = response;
        }

        @Override
        public Fragment getItem(int position) {
            return response.get(position);
        }

        /**
         *Retorna la cantidad de elementos a mostrar en el Toolbar
         */
        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    private void getViews() {
        listaLayoutManager = new ListaLayoutManager(this);
        if(lstProductos != null)
            lstProductos.setLayoutManager(listaLayoutManager);
        configToolBar();
    }

    private void configToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.productolista_titulo));
        toolbar.setNavigationIcon(R.drawable.action_bar_home);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirDialogoTerminar();
            }
        });
    }

    private void abrirDialogoTerminar() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        presenter.abandonarPedido();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Html.fromHtml("<font color='#696969'>"+"Si regresa, perdera la informacion ingresada. Desea continuar?"+"</font>")).setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @Override
    public void irMenu() {
        Navigator.navigateToMenuWithFlag(this,Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }

    @Override
    public void actualizarProductos(List<Producto> productos) {

        if(productos!=null){
            if(productoAdapter == null){
                productoAdapter = new ProductoAdapter(this, productos);
            }else{
                productoAdapter.setProductos(productos);
            }
            productoAdapter.setOnItemClickListener(new ProductoAdapter.OnItemClickListener() {
                @Override
                public void onProductoItemClicked(Producto producto) {
                    presenter.validarRepetido(producto);


                }
            });
            lstProductos.setAdapter(productoAdapter);

        }

    }

    @Override
    public void irProductoDetalle(Producto producto) {
        Navigator.navigateToProductoDetalleWithProduct(this,producto);
    }

    @OnClick(R.id.productolista_btnBuscar)
    public void buscarProducto() {
        boolean isCodigo = false;

        if(rbtCodigo.isChecked())
            isCodigo = true;

        presenter.buscarProducto(isCodigo,txtTexto.getText().toString().trim());

    }

    public static Intent getCallingIntent(Context context){
        return new Intent(context,ProductoListaActivity.class);
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

    @Override
    public void showloading(String mensaje) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(mensaje);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideloading() {
        progressDialog.dismiss();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    @Override
    public void showError(String message) {
        UtilitarioApp.mostrarToast(this, message, ToastCustom.TYPE_ERROR);
    }

    @Override
    public void showCorrect(String message) {
        UtilitarioApp.mostrarToast(this, message, ToastCustom.TYPE_CORRECTO);
    }

    @Override
    public void showInfo(String message) {
        UtilitarioApp.mostrarToast(this, message, ToastCustom.TYPE_INFO);
    }

    @Override
    public void finalizar() {
        finish();
    }

    @Override
    public void irOperadorInvalido() {
        Navigator.navigateToOperadorInvalido(this);
    }

    @Override
    public void validarSubscriptor() {
        Service.validateActivity(this);
    }

    @Override
    public void terminarAplicacion() {
        finish();
        System.exit(0);
    }
}
