package pe.entel.android.plantilla.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;


import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.data.net.Url;
import pe.entel.android.plantilla.data.net.Url.EnumUrl;
import pe.entel.android.plantilla.navigation.Navigator;
import pe.entel.android.plantilla.presenter.SplashPresenter;
import pe.entel.android.plantilla.view.SplashView;
import pe.entel.android.push.service.ConfiguracionPush;
import pe.entel.android.push.service.PushServicePaho;
import pe.entel.android.verification.util.Service;

/**
 * Created by rtamayov on 07/04/2017.
 */
public class SplashActivity extends BaseActivity implements SplashView{

    SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        presenter = new SplashPresenter(this);
        presenter.iniciar();
    }

    @Override
    public void iniciarSubscriptor() {
        Service.startServiceSplash(this, Url.obtenerRuta(this, EnumUrl.VERIFICAR));
    }

    @Override
    public void irLogin() {
        Navigator.navigateToLogin(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startPushClient();
    }

    private void startPushClient(){
        Intent loIntent = new Intent(this, PushServicePaho.class);
        loIntent.setAction(ConfiguracionPush.ACTION_FASTSTART);
        this.startService(loIntent);
    }

    @Override
    public void solicitarPermiso(String[] permisos) {
        ActivityCompat.requestPermissions(this,permisos,0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for(int i=0; i<permissions.length;i++){
            Log.v("SplashActivity",permissions[i] + " " + grantResults[i]);
        }
        presenter.informarRechazo();

    }


}
