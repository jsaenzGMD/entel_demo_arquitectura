package pe.entel.android.plantilla.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import pe.entel.android.plantilla.presenter.UpdateAppPresenter;
import pe.entel.android.plantilla.view.UpdateAppView;

/**
 * Created by rtamayov on 03/04/2017.
 */
public class UpdateAppActivity extends BaseActivity implements UpdateAppView {

    UpdateAppPresenter presenter;

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new UpdateAppPresenter(this);
        Intent intent = getIntent();
        url = intent.getExtras().getString("URL_APK");
        presenter.updateApp(url);
    }


    public static Intent getCallingIntent(Context context){
        return new Intent(context,UpdateAppActivity.class);
    }
}
