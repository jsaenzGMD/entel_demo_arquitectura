package pe.entel.android.plantilla.view.adapter;

import android.content.Context;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.domain.entity.Producto;

/**
 * Created by malzamoraj on 19/09/2017.
 */

public class CarritoAdapter extends RecyclerView.Adapter<CarritoAdapter.ViewHolder> {

    private Context mCtx;
    private List<Producto> productos;
    private OnItemClickListener onItemClickListener;

    public CarritoAdapter( Context mCtx, List<Producto> productos) {
        this.productos = productos;
        this.mCtx = mCtx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_producto, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CarritoAdapter.ViewHolder holder, int position) {
        final Producto producto = productos.get(position);

        if(producto.getEstado()==1){
            holder.ivIcono.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_icon_producto_stock));
        }else if(producto.getEstado()==2){
            holder.ivIcono.setImageDrawable(mCtx.getResources().getDrawable(R.drawable.ic_icon_producto_stock));
        }
        holder.tvResProducto.setText(producto.getNombre());
        holder.tvResSubTotal.setText(producto.getMonto());
        holder.tvResPrecioUnitario.setText(producto.getPrecio());
        holder.tvResStock.setText(producto.getStock());
        holder.tvResCantidad.setText(producto.getCantidad());
        holder.tvResDescuento.setText(producto.getDescuento());
        holder.tvResBonificacion.setText(producto.getBonificacion());

        holder.tvOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirMenuEditarEliminar(holder,producto);
            }
        });

        holder.ivIcono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirMenuEditarEliminar(holder,producto);
            }
        });

    }

    public void abrirMenuEditarEliminar(ViewHolder holder, final Producto producto){
        Context wrapper = new ContextThemeWrapper(mCtx, R.style.CambioTexto);
        //creating a popup menu
        PopupMenu popup = new PopupMenu(wrapper, holder.tvOptions);
        //inflating menu from xml resource
        popup.inflate(R.menu.menu_option_producto);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu1:
                        if(onItemClickListener != null){
                            onItemClickListener.onProductoItemEditar(producto);
                        }
                        break;
                    case R.id.menu2:
                        if(onItemClickListener != null){
                            onItemClickListener.onProductoItemEliminar(producto);
                        }
                        break;
                }
                return false;
            }
        });


        //displaying the popup
        MenuPopupHelper menuHelper = new MenuPopupHelper(mCtx, (MenuBuilder) popup.getMenu(),  holder.tvOptions);
        menuHelper.setForceShowIcon(true);
        menuHelper.show();

    }


    @Override
    public int getItemCount() {
        return productos.size();
    }

    private void validateProductos(List<Producto> productoList) {
        if (productoList == null) {
            throw new IllegalArgumentException("The track list cannot be null");
        }
    }

    public void setOnItemClickListener (OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setProductos(List<Producto> productos){
        this.validateProductos(productos);
        this.productos = productos;
        this.notifyDataSetChanged();

    }

    public interface OnItemClickListener {
        void onProductoItemEditar(Producto producto);
        void onProductoItemEliminar(Producto producto);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivIcono;
        public TextView tvResProducto;
        public TextView tvOptions;
        public TextView tvResSubTotal;
        public TextView tvResPrecioUnitario;
        public TextView tvResStock;
        public TextView tvResCantidad;
        public TextView tvResDescuento;
        public TextView tvResBonificacion;

        public ViewHolder(View itemView) {
            super(itemView);

            ivIcono = (ImageView) itemView.findViewById(R.id.ivIconoCarrito);
            tvResProducto = (TextView) itemView.findViewById(R.id.tvResProducto);
            tvOptions = (TextView) itemView.findViewById(R.id.tvOptions);
            tvResSubTotal = (TextView) itemView.findViewById(R.id.tvResSubTotal);
            tvResPrecioUnitario = (TextView) itemView.findViewById(R.id.tvResPrecioUnitario);
            tvResStock = (TextView) itemView.findViewById(R.id.tvResStock);
            tvResCantidad = (TextView) itemView.findViewById(R.id.tvResCantidad);
            tvResDescuento = (TextView) itemView.findViewById(R.id.tvResDescuento);
            tvResBonificacion = (TextView) itemView.findViewById(R.id.tvResBonificacion);
        }
    }
}