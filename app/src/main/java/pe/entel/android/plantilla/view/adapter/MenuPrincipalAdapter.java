package pe.entel.android.plantilla.view.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.model.MenuModel;

/**
 * Created by rtamayov on 12/04/2017.
 */
public class MenuPrincipalAdapter extends RecyclerView.Adapter<MenuPrincipalAdapter.ViewHolder> {

    Context context;
    List<MenuModel> lista;
    onItemClickListener listener;

    public interface onItemClickListener {
        void opcionSeleccionada(MenuModel menuModel);
    }

    /**
     * Constructor que permite la recepción de datos.
     * @param context Contexto.
     * @param lista Lista de elementos recibidos.
     * @param onItemClickListener Listener por el cual se enviará posteriormente la respuesta.
     */
    public MenuPrincipalAdapter(Context context, List<MenuModel> lista, onItemClickListener onItemClickListener) {
        this.context = context;
        this.lista = lista;
        this.listener = onItemClickListener;
    }

    @Override
    public MenuPrincipalAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view, listener);
        return viewHolder;
    }

    /**
     * Permite insertar los datos haia los controles mapeados.
     * @param position Posicion dentro de la lista en la cual se encuentra el control.
     */
    @Override
    public void onBindViewHolder(MenuPrincipalAdapter.ViewHolder holder, final int position) {
        if (lista.get(position).getTitulo() == "Sincronizar") {
            holder.tv.setTextColor(ContextCompat.getColor(context, R.color.color_seleccionado));
        }

        holder.tv.setText(lista.get(position).getTitulo());
        holder.imageOpcion.setImageResource(lista.get(position).getImagen());
        holder.imageOpcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.opcionSeleccionada(lista.get(position));
            }
        });

    }

    /**
     * Cantidad de elementos totales que contiene la lista.
     */
    @Override
    public int getItemCount() {
        return this.lista.size();
    }

    /**
     * Mapeo de los controles.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public onItemClickListener listener;
        TextView tv;
        ImageView imageOpcion;

        public ViewHolder(View itemView, onItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            tv = (TextView) itemView.findViewById(R.id.menuitem_text);
            imageOpcion = (ImageView) itemView.findViewById(R.id.imgOpcion);

        }
    }
}
