package pe.entel.android.plantilla.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.domain.entity.Producto;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class PedidoProductoAdapter extends RecyclerView.Adapter<PedidoProductoAdapter.PedidoProductoViewHolder> {

    private List<Producto> productos;
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onProductoItemClicked(Producto producto);
    }

    public PedidoProductoAdapter(Context context, List<Producto> productos) {
        this.validateProductos(productos);
        this.productos = productos;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PedidoProductoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.layoutInflater.inflate(R.layout.row_producto, parent, false);
        PedidoProductoViewHolder viewHolder = new PedidoProductoViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PedidoProductoViewHolder holder, int position) {
        final Producto producto = this.productos.get(position);
        holder.textViewTitle.setText(producto.getCodigoProducto() + " - " + producto.getNombre());
        holder.textViewDetailDiferenciador.setText("Cantidad: " + producto.getCantidad() +" Monto: "+producto.getMonto());
        holder.itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                if(onItemClickListener != null){
                    onItemClickListener.onProductoItemClicked(producto);
                }
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (this.productos != null && !this.productos.isEmpty()) {
            count = this.productos.size();
        }
        return count;
    }

    private void validateProductos(List<Producto> productoList) {
        if (productoList == null) {
            throw new IllegalArgumentException("The track list cannot be null");
        }
    }

    public void setOnItemClickListener (OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setProductos(List<Producto> productos){
        this.validateProductos(productos);
        this.productos = productos;
        this.notifyDataSetChanged();

    }

    static class PedidoProductoViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.title)
        TextView textViewTitle;

        @InjectView(R.id.detail_diferenciador)
        TextView textViewDetailDiferenciador;

        public PedidoProductoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
