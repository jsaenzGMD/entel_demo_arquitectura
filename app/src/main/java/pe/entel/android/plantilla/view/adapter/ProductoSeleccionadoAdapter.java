package pe.entel.android.plantilla.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.domain.entity.Producto;

/**
 * Created by ahernandezde on 21/09/2017.
 */

public class ProductoSeleccionadoAdapter extends RecyclerView.Adapter<ProductoSeleccionadoAdapter.ViewHolder> {
    private Context ctx;
    private List<Producto> listProductoModel = null;

    public ProductoSeleccionadoAdapter(Context ctx, List<Producto> listProductoModel) {
        this.ctx = ctx;
        this.listProductoModel = listProductoModel;
    }

    @Override
    public ProductoSeleccionadoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.design_item_pedido,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProductoSeleccionadoAdapter.ViewHolder holder, int position) {
        Producto producto = listProductoModel.get(position);
        holder.txtProducto.setText(producto.getNombre().toString());
        holder.txtPrecio.setText("S/."+producto.getMonto().toString());

    }

    @Override
    public int getItemCount() {
        return listProductoModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtProducto, txtPrecio;
        public ViewHolder(View itemView) {
            super(itemView);
            txtProducto = (TextView) itemView.findViewById(R.id.txtProducto);
            txtPrecio = (TextView) itemView.findViewById(R.id.txtPrecio);
        }
    }
}
