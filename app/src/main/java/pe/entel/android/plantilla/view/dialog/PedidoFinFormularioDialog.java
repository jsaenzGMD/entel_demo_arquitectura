package pe.entel.android.plantilla.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pe.entel.android.plantilla.R;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.model.UsuarioModel;
import pe.entel.android.plantilla.view.adapter.ProductoSeleccionadoAdapter;

/**
 * Created by ahernandezde on 21/09/2017.
 */

public class PedidoFinFormularioDialog extends Dialog implements View.OnClickListener {

    private Context ctx;
    private OnPedidoFinClickListener mListener;
    private List<Producto> listProductoList = null;
    private UsuarioModel usuario = null;
    private String mensaje = null;

    @InjectView(R.id.imgClienteDialog)
    ImageView imgClienteDialog;
    @InjectView(R.id.txtNombreClienteDialog)
    TextView txtNombreClienteDialog;
    @InjectView(R.id.txtCodigoClienteDialog)
    TextView txtCodigoClienteDialog;
    @InjectView(R.id.txtMensajeDialog)
    TextView txtMensajeDialog;
    @InjectView(R.id.rcvListPedidosDialog)
    RecyclerView rcvListPedidos;
    @InjectView(R.id.txtTotalItemsDialog)
    TextView txtTotalItemsDialog;
    @InjectView(R.id.txtMontoTotalDialog)
    TextView txtMontoTotalDialog;
    @InjectView(R.id.txtFechaDialog)
    TextView txtFechaDialog;
    @InjectView(R.id.btnCancelarDialog)
    Button btnCancelarDialog;
    @InjectView(R.id.btnAceptarDialog)
    Button btnAceptarDialog;

    public interface OnPedidoFinClickListener {
        void onButtonClick(List<Producto> listProductoModelList);
    }


    public PedidoFinFormularioDialog(Context context, UsuarioModel usuario, String mensaje, List<Producto> listProducto, OnPedidoFinClickListener mListener) {
        super(context);
        this.ctx = context;
        this.usuario = usuario;
        this.mensaje = mensaje;
        this.listProductoList = listProducto;
        this.mListener = mListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_pedido_fin_formulario);
        ButterKnife.inject(this);

        btnCancelarDialog.setOnClickListener(this);
        btnAceptarDialog.setOnClickListener(this);
        actualizarProductos(listProductoList);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAceptarDialog:
                mListener.onButtonClick(listProductoList);
                break;
            case R.id.btnCancelarDialog:
                this.dismiss();
                break;
        }
    }

    public void actualizarProductos(List<Producto> lstProductoList) {
        if (lstProductoList != null) {
            txtNombreClienteDialog.setText(usuario.getNombre().toString());
            txtCodigoClienteDialog.setText("Código: "+usuario.getCodigo().toString());
            txtMensajeDialog.setText(mensaje);
            rcvListPedidos.setLayoutManager(new LinearLayoutManager(ctx));
            ProductoSeleccionadoAdapter productoSeleccionadoAdapter = new ProductoSeleccionadoAdapter(ctx, lstProductoList);
            rcvListPedidos.setAdapter(productoSeleccionadoAdapter);
            txtTotalItemsDialog.setText(String.valueOf(lstProductoList.size()));
            double monto = 0.00;
            for(Producto producto: lstProductoList){
                monto = monto + Double.valueOf(producto.getMonto().toString());
            }
            txtMontoTotalDialog.setText("S/."+monto);
            txtFechaDialog.setText(android.text.format.DateFormat.format("dd/MM/yyyy hh:mm:ss", new java.util.Date()));
        }
    }
}
