package pe.entel.android.plantilla.data.dao;

import com.orm.SugarRecord;

/**
 * Created by rtamayov on 18/04/2017.
 */
public class FotoDao extends SugarRecord {

    private String idUsuario;
    private String identificador;
    private String flgEnviado;

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFlgEnviado() {
        return flgEnviado;
    }

    public void setFlgEnviado(String flgEnviado) {
        this.flgEnviado = flgEnviado;
    }
}
