package pe.entel.android.plantilla.data.dao;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class PedidoDao extends SugarRecord {

    private String idUsuario;
    private String latitud;
    private String longitud;
    private String fecha;
    private String flgEnviado;

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFlgEnviado() {
        return flgEnviado;
    }

    public void setFlgEnviado(String flgEnviado) {
        this.flgEnviado = flgEnviado;
    }
}
