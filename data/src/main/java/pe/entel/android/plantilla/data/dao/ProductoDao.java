package pe.entel.android.plantilla.data.dao;


import com.orm.SugarRecord;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class ProductoDao extends SugarRecord{

    private String idProducto;
    private String codigoProducto;
    private String nombre;
    private String precio;


    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
