package pe.entel.android.plantilla.data.entity;

import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by rtamayov on 18/04/2017.
 */
public class FotoEntity extends PhotoEntity{

    private static final String TAG = FotoEntity.class.getName();

    private String idUsuario;
    private byte[] imagen;

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public void agregarDatos(DataOutputStream outputStream) throws IOException {
        Log.v(TAG,"identificador"+this.identificador);
        Log.v(TAG,"idUsuario"+this.idUsuario);
        outputStream.writeLong(Long.parseLong(this.identificador));
        outputStream.writeInt(Integer.parseInt(this.idUsuario));
    }


}
