package pe.entel.android.plantilla.data.entity;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by rtamayov on 20/04/2017.
 */
public abstract class PhotoEntity {

    public String identificador;
    public String raiz;

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getRaiz() {
        return raiz;
    }

    public void setRaiz(String raiz) {
        this.raiz = raiz;
    }

    public abstract void agregarDatos(DataOutputStream outputStream) throws IOException;
}
