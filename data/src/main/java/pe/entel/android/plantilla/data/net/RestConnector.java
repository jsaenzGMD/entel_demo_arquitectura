package pe.entel.android.plantilla.data.net;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by asoto on 30/01/2015.
 */
public abstract class RestConnector {
    public static final String REST_RESULT = "pe.entel.android.plantilla.data.net.service.REST_RESULT";
    private ResultReceiver mReceiver;
    public RestConnector(){
            mReceiver = new ResultReceiver(new Handler()) {

                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    if (resultData != null
                            && resultData.containsKey(REST_RESULT)) {
                        if (resultData.size() > 1) {
                            onRESTResultBundle(resultCode,
                                    resultData);
                        } else
                            onRESTResult(resultCode,
                                    resultData.getString(REST_RESULT));
                    } else {
                        onRESTResult(resultCode, null);
                    }
                }

            };
    }
    public ResultReceiver getResultReceiver()
    {
        return mReceiver;
    }

    // Implementers of this Fragment will handle the result here.
    abstract public void onRESTResult(int code, String result);
    abstract public void onRESTResultBundle(int code, Bundle result);

}
