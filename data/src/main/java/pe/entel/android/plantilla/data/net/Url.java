package pe.entel.android.plantilla.data.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Properties;

import pe.entel.android.plantilla.data.R;
import pe.entel.android.plantilla.data.util.UtilitarioData;

/**
 * Created by rtamayov on 10/04/2017.
 */
public class Url {

    public static enum EnumUrl {
        LOGIN, SINCRONIZACION, PEDIDO, FOTO,MAESTRO,
        VERIFICAR, VERSION
    }

    public static String obtenerRuta(Context poContext, EnumUrl poEnumUrl) {
        Properties mProperties;
        mProperties = UtilitarioData.readPropertiesFromData(poContext, "params.properties");
        String ipServer = mProperties.getProperty("IP_SERVER");

        String lsUrl = "";
        SharedPreferences loSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(poContext);

        String lsRuta = "http://"
                + loSharedPreferences.getString(
                poContext.getString(R.string.keypre_url),
                ipServer) + "/";

        if (poEnumUrl == EnumUrl.LOGIN) {
            lsUrl = lsRuta + "ValidarUsuario";
        } else if (poEnumUrl == EnumUrl.SINCRONIZACION) {
            lsUrl = lsRuta + "Sincronizacion";
        }  else if (poEnumUrl == EnumUrl.MAESTRO) {
            lsUrl = lsRuta + "Maestro";
        }  else if (poEnumUrl == EnumUrl.PEDIDO) {
            lsUrl = lsRuta + "InsertarPedido";
        } else if (poEnumUrl == EnumUrl.FOTO) {
            lsUrl = lsRuta + "InsertarFoto";
        } else if (poEnumUrl == EnumUrl.VERIFICAR) {
            lsUrl = lsRuta + "Verificar";
        } else if (poEnumUrl == EnumUrl.VERSION) {
            lsUrl = lsRuta + "Version";
        }
        return lsUrl;
    }

    public static String obtenerBase(Context poContext) {
        Properties mProperties;
        mProperties = UtilitarioData.readPropertiesFromData(poContext, "params.properties");
        String ipServer = mProperties.getProperty("IP_SERVER");

        String lsUrl = "";
        SharedPreferences loSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(poContext);

        String lsRuta = "http://"
                + loSharedPreferences.getString(
                poContext.getString(R.string.keypre_url),
                ipServer) + "/";


        return lsRuta;
    }


}
