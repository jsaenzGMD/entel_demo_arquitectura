package pe.entel.android.plantilla.data.net.message;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alexander on 01/02/2015.
 */
public class Response<T>
{
    public static int CODE_OK = 1;
    public static int CODE_ERROR = 0;

    public Response(){
        setMessage("");
        setCode(CODE_OK);
    }
    @SerializedName("PayLoad")
    private T payLoad;
    @SerializedName("Message")
    private String message;
    @SerializedName("Code")
    private int code;


    public T getPayLoad() {
        return payLoad;
    }

    public void setPayLoad(T payLoad) {
        this.payLoad = payLoad;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}