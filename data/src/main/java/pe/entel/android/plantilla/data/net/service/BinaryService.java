package pe.entel.android.plantilla.data.net.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import pe.entel.android.plantilla.data.R;
import pe.entel.android.plantilla.data.entity.FotoEntity;
import pe.entel.android.plantilla.data.entity.PhotoEntity;
import pe.entel.android.plantilla.data.repository.datasource.generic.FileStore;

/**
 * Created by asoto on 02/03/2015.
 */
public class BinaryService extends Service {
    private static final String TAG = BinaryService.class.getName();

    public static final String EXTRA_ONLY_A_PARAM = "pe.entel.android.plantilla.data.net.service.EXTRA_ONLY_A_PARAM";
    public static final String EXTRA_RESULT_RECEIVER = "pe.entel.android.plantilla.data.net.service.EXTRA_RESULT_RECEIVER";

    public static final String REST_RESULT = "pe.entel.android.plantilla.data.net.service.REST_RESULT";

    private PowerManager.WakeLock ioWakeLock;
    private Gson gson;
    private String url_to_sendPhoto = "";
    private FileStore fileStore;
    private Thread netThread;
    private SendProcess sendProcess;
    private PhotoEntity photoEntity;
    private ResultReceiver receiver;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        gson = new Gson();

        fileStore = new FileStore(this);
        sendProcess = new SendProcess();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        sendPhoto(intent);
        return Service.START_NOT_STICKY;
    }

    private void sendPhoto(Intent intent) {
        Uri action = intent.getData();
        Bundle extras = intent.getExtras();

        if (extras == null || action == null) {
            // Extras contain our ResultReceiver and data is our REST action.
            // So, without these components we can't do anything useful.
            Log.e(TAG, "You did not pass extras or data with the Intent.");
            return;
        }
        url_to_sendPhoto = action.toString();
        String jsonPhotoEntity = extras.getString(EXTRA_ONLY_A_PARAM);
        photoEntity = gson.fromJson(jsonPhotoEntity, FotoEntity.class);
        receiver = extras.getParcelable(EXTRA_RESULT_RECEIVER);

        Log.v(TAG, "llego solcitud de envio de fotos");
        if (netThread == null || !netThread.isAlive()) {
            Log.d(TAG, "llego solcitud de envio de fotos 1");
            netThread = new Thread(sendProcess);
            Log.d(TAG, "llego solcitud de envio de fotos 2");
            netThread.start();
        }
    }


    public void uploadFile(PhotoEntity photoEntity) {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        try {
            URL url = new URL(url_to_sendPhoto);
            Log.v(TAG, url_to_sendPhoto);

            connection = (HttpURLConnection) url.openConnection();
            // Allow Inputs & Outputs
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            // Enable POST method
            connection.setRequestMethod("POST");
            // para idientificar el contenido  nuevo a diferencia de anterior
            connection.setRequestProperty("Content-Type", "application/octet-stream");

            outputStream = new DataOutputStream(connection.getOutputStream());
            photoEntity.agregarDatos(outputStream);

            fileStore.writeToStream(outputStream, photoEntity.getRaiz(), photoEntity.getIdentificador());
            Log.v(TAG, "e llego solcitud de envio de fotos " + connection.getResponseCode() + " " + connection.getResponseMessage());

            InputStream inputStream;
            if (connection.getResponseCode() == 200) {
                inputStream = connection.getInputStream();
            } else
                inputStream = connection.getErrorStream();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String lsResultado = bufferedReader.readLine();

            outputStream.flush();
            outputStream.close();
            inputStream.close();

            Bundle resultData = new Bundle();
            resultData.putString(REST_RESULT, lsResultado);

            receiver.send(connection.getResponseCode(), resultData);


        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(TAG, ex.getMessage() + "");

            Bundle b = new Bundle();
            b.putString(BinaryService.REST_RESULT, ex.getMessage());

            receiver.send(0, b);
        }
    }

    private class SendProcess implements Runnable {

        @Override
        public void run() {
            uploadFile(photoEntity);
            terminarProceso();
        }
    }

    private void terminarProceso() {
        subCerrarWakeLock();
        stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Abre un WakeLock al sistema al servicio ejecutándose.
     *
     * @param poContext
     */
    private void subAbrirWakeLock(Context poContext) {
        try {
            if (ioWakeLock == null) {
                //Logger.d("Inicio acquire wakelock");
                PowerManager pm = (PowerManager) poContext
                        .getSystemService(POWER_SERVICE);
                ioWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                        poContext.getString(R.string.app_name));
            } else {
                //Logger.d("No se creo WakeLock: ya existia.");
            }

            if (!ioWakeLock.isHeld()) {
                ioWakeLock.acquire();
                //Logger.d("Fin acquire wakelock");
            } else {
                //Logger.d("Wake lock no estaba liberado.");
            }
        } catch (Exception e) {
            //Logger.e("Error al abrir WakeLock");
            //Logger.e(e);
        }
    }

    /**
     * Cierra el WakeLock asignado al sistema.
     */
    private void subCerrarWakeLock() {
        try {
            if (ioWakeLock.isHeld()) {
                //Logger.d("Inicio release wakelock");
                try {
                    ioWakeLock.release();
                } catch (Throwable t) {
                    //Logger.e(t);
                }
                //Logger.d("Fin release wakelock");
            } else {
                //Logger.d("Wakelock ya estaba liberado.");
            }
        } catch (Exception e) {
            //Logger.e("Error al liberar WakeLock");
            //Logger.e(e);
        }
    }

}
