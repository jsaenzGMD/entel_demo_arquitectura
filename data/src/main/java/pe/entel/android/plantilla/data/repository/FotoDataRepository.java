package pe.entel.android.plantilla.data.repository;

import android.content.Context;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.plantilla.data.dao.FotoDao;
import pe.entel.android.plantilla.data.entity.FotoEntity;
import pe.entel.android.plantilla.data.exception.RepositoryErrorBundle;
import pe.entel.android.plantilla.data.repository.datasource.application.FotoApplicationStore;
import pe.entel.android.plantilla.data.repository.datasource.cloud.FotoCloudStore;
import pe.entel.android.plantilla.data.repository.datasource.local.FotoLocalStore;
import pe.entel.android.plantilla.data.repository.datasource.preference.FotoPreferenceStore;
import pe.entel.android.plantilla.data.repository.datasource.generic.FileStore;
import pe.entel.android.plantilla.data.util.UtilitarioData;
import pe.entel.android.plantilla.domain.entity.Photo;
import pe.entel.android.plantilla.domain.repository.FotoRepository;

/**
 * Created by rtamayov on 17/04/2017.
 */
public class FotoDataRepository implements FotoRepository {

    final Context context;

    public FotoDataRepository(Context context) {
        this.context = context;
    }

    @Override
    public void guardarFoto(final String idUsuario, final String identificador, byte[] data, final GuardarFotoCallback guardarFotoCallback) {
        FileStore fileStore = new FileStore(context);
        FotoApplicationStore applicationStore = new FotoApplicationStore(context, fileStore);
        final FotoLocalStore fotoLocalStore = new FotoLocalStore();

        applicationStore.guardarFoto(identificador, data, new FotoApplicationStore.GuardarFotoCallback() {
            @Override
            public void onGuardado() {
                fotoLocalStore.guardarFoto(idUsuario,identificador);
                guardarFotoCallback.onGuardado();
            }

            @Override
            public void onError(Exception e) {
                guardarFotoCallback.onError(new RepositoryErrorBundle(e));
            }
        });
    }

    @Override
    public void enviarFoto(final String identificador, final EnviarFotoCallback enviarFotoCallback) {
        FileStore fileStore = new FileStore(context);
        FotoCloudStore fotoCloudStore= new FotoCloudStore(context);

        final FotoApplicationStore applicationStore = new FotoApplicationStore(context,fileStore);
        final FotoLocalStore localStore = new FotoLocalStore();
        final FotoDao fotoDao = localStore.obtenerFoto(identificador);

        FotoEntity fotoEntity = new FotoEntity();
        fotoEntity.setIdUsuario(fotoDao.getIdUsuario());
        fotoEntity.setIdentificador(identificador);
        fotoEntity.setRaiz(UtilitarioData.getRaiz(context));

        fotoCloudStore.enviarFoto(fotoEntity, new FotoCloudStore.EnviarFotoCallback() {
            @Override
            public void onEnviada(String respuesta) {
                fotoDao.setFlgEnviado("T");
                localStore.guardarFoto(fotoDao);
                applicationStore.eliminarFoto(identificador);
                enviarFotoCallback.onEnviado(respuesta);
            }

            @Override
            public void onError(Exception e) {
                enviarFotoCallback.onError(new RepositoryErrorBundle(e));
            }
        });
    }

    @Override
    public void eliminarFoto(String identificador) {
        FileStore fileStore = new FileStore(context);
        FotoApplicationStore applicationStore = new FotoApplicationStore(context, fileStore);
        FotoLocalStore fotoLocalStore = new FotoLocalStore();

        applicationStore.eliminarFoto(identificador);
        fotoLocalStore.borrarFoto(identificador);
    }

    @Override
    public String obtenerRutaFoto(String identificador) {
        FileStore fileStore = new FileStore(context);
        FotoApplicationStore applicationStore = new FotoApplicationStore(context, fileStore);
        return applicationStore.obtenerFotoRuta(identificador);
    }

    @Override
    public int getTipoFoto() {
        FotoPreferenceStore store = new FotoPreferenceStore(context);
        return store.getTipoCamara();
    }

    @Override
    public void setTipoFoto(int tipo) {
        FotoPreferenceStore store = new FotoPreferenceStore(context);
        store.setTipoCamara(tipo);
    }

    @Override
    public List<Photo> listarFotosPendientes() {
        List<Photo> fotos = new ArrayList<>();

        ModelMapper modelMapper = new ModelMapper();
        FileStore fileStore = new FileStore(context);
        FotoLocalStore localStore = new FotoLocalStore();
        FotoApplicationStore applicationStore = new FotoApplicationStore(context,fileStore);

        List<FotoDao> listaFotos = localStore.listarFotosPendientes();

        for (FotoDao fotoDao :listaFotos) {
            boolean existe = applicationStore.existeFoto(fotoDao.getIdentificador());
            if(existe){
                fotos.add(modelMapper.map(fotoDao,Photo.class));
            }

        }

        return fotos;
    }
}
