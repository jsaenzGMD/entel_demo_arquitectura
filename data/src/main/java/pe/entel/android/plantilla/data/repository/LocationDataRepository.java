package pe.entel.android.plantilla.data.repository;

import android.content.Context;
import android.location.Location;

import java.text.SimpleDateFormat;
import java.util.Date;

import pe.entel.android.plantilla.data.util.UtilitarioData;
import pe.entel.android.plantilla.domain.entity.Localizacion;
import pe.entel.android.plantilla.domain.repository.LocationRepository;
import pe.entel.android.util.obtencionlocalizacionlocationmanager.datasource.LocationApplicationStore;

/**
 * Created by rtamayov on 12/04/2017.
 */
public class LocationDataRepository implements LocationRepository{

    final Context context;

    public LocationDataRepository(Context context) {
        this.context = context;
    }


    @Override
    public void obtenerLocalizacion(final ObtenerLocalizacionCallback obtenerLocalizacionCallback) {


        LocationApplicationStore store = new LocationApplicationStore(context);
        store.obtenerLocalizacion(new LocationApplicationStore.ObtenerLocalizacionCallback() {
            @Override
            public void obtuvo(Location location) {
                Localizacion  localizacion = null;

                if(location!= null){
                    localizacion = new Localizacion();
                    localizacion.setLatitud(location.getLatitude() + "");
                    localizacion.setLongitud(location.getLongitude() + "");
                    localizacion.setVelocidad(location.getSpeed() + "");
                    localizacion.setGpsOrigen(UtilitarioData.parseOrigen(location));
                    localizacion.setGpsPrecision(location.getAccuracy()+"");
                    SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
                    localizacion.setFechaMovil(df.format(new Date()));
                }



                obtenerLocalizacionCallback.obtener(localizacion);
            }
        });



    }
}
