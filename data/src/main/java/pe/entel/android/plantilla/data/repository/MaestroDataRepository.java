package pe.entel.android.plantilla.data.repository;

import android.content.Context;

import java.io.InputStream;

import pe.entel.android.plantilla.data.exception.RepositoryErrorBundle;
import pe.entel.android.plantilla.data.repository.datasource.local.MaestroLocalStore;
import pe.entel.android.plantilla.data.repository.datasource.cloud.MaestroCloudStore;
import pe.entel.android.plantilla.domain.repository.MaestroRepository;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class MaestroDataRepository implements MaestroRepository {

    final Context context;

    public MaestroDataRepository(Context context) {
        this.context = context;
    }


    @Override
    public void sincronizarMaestros(String usuario, final SincronizarMaestrosCallback sincronizarMaestrosCallback) {

        MaestroCloudStore maestroCloudStore = new MaestroCloudStore(context);
        maestroCloudStore.sincronizarMaestros(usuario, new MaestroCloudStore.SincronizarMaestrosCallback() {

            @Override
            public void onSincronizacion(String mensaje, InputStream inputStream) {

                MaestroLocalStore maestroLocalStore = new MaestroLocalStore();
                maestroLocalStore.insertarQuery(inputStream);

                sincronizarMaestrosCallback.onSincronizado(mensaje);
            }

            @Override
            public void onError(Exception e) {
                sincronizarMaestrosCallback.onError(new RepositoryErrorBundle(e));
            }
        });

    }

    @Override
    public void borrarTodo() {
        MaestroLocalStore store = new MaestroLocalStore();
        store.borrarTodo();
    }
}
