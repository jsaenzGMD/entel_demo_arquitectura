package pe.entel.android.plantilla.data.repository;

import android.content.Context;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import pe.entel.android.plantilla.data.dao.PedidoDao;
import pe.entel.android.plantilla.data.dao.PedidoDetalleDao;
import pe.entel.android.plantilla.data.exception.RepositoryErrorBundle;
import pe.entel.android.plantilla.data.repository.datasource.local.PedidoLocalStore;
import pe.entel.android.plantilla.data.repository.datasource.preference.PedidoPreferenceStore;
import pe.entel.android.plantilla.data.repository.datasource.cloud.PedidoCloudStore;
import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;

/**
 * Created by rtamayov on 13/04/2017.
 */

public class PedidoDataRepository implements PedidoRepository {

    final Context context;

    public PedidoDataRepository(Context context) {
        this.context = context;
    }


    @Override
    public void setPedido(Pedido pedido) {
        PedidoPreferenceStore store = new PedidoPreferenceStore(context);
        store.setPedido(pedido);
    }

    @Override
    public Pedido getPedido() {
        PedidoPreferenceStore store = new PedidoPreferenceStore(context);
        return store.getPedido();
    }

    @Override
    public void guardarPedido(Pedido pedido) {
        PedidoLocalStore store = new PedidoLocalStore();
        ModelMapper modelMapper = new ModelMapper();
        store.guardarPedido(modelMapper.map(pedido,PedidoDao.class));

    }

    @Override
    public List<Pedido> listarPedidosPendientes() {

        PedidoLocalStore store = new PedidoLocalStore();
        ModelMapper modelMapper = new ModelMapper();
        Type listTypePedido = new TypeToken<List<Pedido>>() {}.getType();
        Type listTypeProducto = new TypeToken<List<Producto>>() {}.getType();

        List<Pedido> pedidos = modelMapper.map(store.listarPedidoPendientes(),listTypePedido);


        for (Pedido pedido : pedidos ) {
            List<PedidoDetalleDao> detalleDaos = store.listarPedidoDetalle(pedido.getId());
            List<Producto> productos = modelMapper.map(detalleDaos,listTypeProducto);
            pedido.setDetalles(productos);
        }

        return pedidos;
    }

    @Override
    public void enviarPedido(Pedido pedido, final EnviarPedidoCallback callback) {
        ModelMapper modelMapper = new ModelMapper();
        Type listType = new TypeToken<List<PedidoDetalleDao>>() {}.getType();

        PedidoCloudStore pedidoCloudStore = new PedidoCloudStore(context);

        final PedidoLocalStore localStore = new PedidoLocalStore();
        final PedidoDao pedidoDao = modelMapper.map(pedido,PedidoDao.class);

        localStore.guardarPedido(pedidoDao);

        List<PedidoDetalleDao> pedidoDetalleDaos = modelMapper.map(pedido.getDetalles(),listType);

        for (PedidoDetalleDao pedidoDetalleDao : pedidoDetalleDaos) {
            pedidoDetalleDao.setIdPedido(pedidoDao.getId());
            localStore.guardarProducto(pedidoDetalleDao);
        }

        pedidoCloudStore.enviarPedido(pedido, new PedidoCloudStore.EnviarPedidoCallback() {
            @Override
            public void onEnviado(String mensaje) {
                pedidoDao.setFlgEnviado("T");
                localStore.guardarPedido(pedidoDao);
                callback.onEnviado(mensaje);
            }

            @Override
            public void onError(Exception e) {
                callback.onError(new RepositoryErrorBundle(e));
            }
        });


    }
}
