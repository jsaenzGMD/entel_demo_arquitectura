package pe.entel.android.plantilla.data.repository;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;


import java.lang.reflect.Type;
import java.util.List;

import pe.entel.android.plantilla.data.dao.ProductoDao;
import pe.entel.android.plantilla.data.repository.datasource.local.ProductoLocalStore;
import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.domain.repository.ProductoRepository;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class ProductoDataRepository implements ProductoRepository{
    @Override
    public List<Producto> listarProductosXCodigo(String codigo) {
        ProductoLocalStore store =  new ProductoLocalStore();
        ModelMapper modelMapper = new ModelMapper();
        Type listType = new TypeToken<List<Producto>>() {}.getType();

        List<ProductoDao> productoDaos = store.listarProductoXCodigo(codigo);
        return modelMapper.map(productoDaos,listType);
    }

    @Override
    public List<Producto> listarProductosXNombre(String nombre) {
        ProductoLocalStore store =  new ProductoLocalStore();
        ModelMapper modelMapper = new ModelMapper();
        Type listType = new TypeToken<List<Producto>>() {}.getType();

        List<ProductoDao> productoDaos = store.listarProductoXNombre(nombre);
        return modelMapper.map(productoDaos,listType);
    }

    @Override
    public List<Producto> listarProductos() {
        ProductoLocalStore store =  new ProductoLocalStore();
        ModelMapper modelMapper = new ModelMapper();
        Type listType = new TypeToken<List<Producto>>() {}.getType();

        List<ProductoDao> productoDaos = store.listarProducto();
        return modelMapper.map(productoDaos,listType);
    }
}
