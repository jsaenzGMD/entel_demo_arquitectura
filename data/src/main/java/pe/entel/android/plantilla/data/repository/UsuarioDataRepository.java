package pe.entel.android.plantilla.data.repository;

import android.content.Context;

import pe.entel.android.plantilla.data.exception.RepositoryErrorBundle;
import pe.entel.android.plantilla.data.repository.datasource.preference.UsuarioPreferenceStore;
import pe.entel.android.plantilla.data.repository.datasource.cloud.UsuarioCloudStore;
import pe.entel.android.plantilla.domain.entity.Usuario;
import pe.entel.android.plantilla.domain.repository.UsuarioRepository;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class UsuarioDataRepository implements UsuarioRepository{

    final Context context;

    public UsuarioDataRepository(Context context) {
        this.context = context;
    }

    @Override
    public void validarUsuario(String codigo, String clave, final ValidarUsuarioCallback validarUsuarioCallback) {

        UsuarioCloudStore usuarioCloudStore = new UsuarioCloudStore(context);
        usuarioCloudStore.validarUsuario(codigo, clave, new UsuarioCloudStore.ValidarUsuarioCallback() {
            @Override
            public void onValidar(String mensaje, Usuario usuario) {
                validarUsuarioCallback.onValidado(mensaje, usuario);
            }

            @Override
            public void onError(Exception e) {
                validarUsuarioCallback.onError(new RepositoryErrorBundle(e));
            }
        });

    }

    @Override
    public void setUsuario(Usuario usuario) {
        UsuarioPreferenceStore store = new UsuarioPreferenceStore(context);
        store.setUsuario(usuario);

    }

    @Override
    public Usuario getUsuario() {
        UsuarioPreferenceStore store = new UsuarioPreferenceStore(context);
        return store.getUsuario();
    }
}
