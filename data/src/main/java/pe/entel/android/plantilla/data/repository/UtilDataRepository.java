package pe.entel.android.plantilla.data.repository;

import android.content.Context;

import pe.entel.android.plantilla.data.repository.datasource.application.UtilApplicationStore;
import pe.entel.android.plantilla.domain.repository.UtilRepository;

/**
 * Created by rtamayov on 10/04/2017.
 */
public class UtilDataRepository implements UtilRepository {

    final Context context;

    public UtilDataRepository(Context context) {
        this.context = context;
    }

    @Override
    public boolean haySenal() {
        UtilApplicationStore store = new UtilApplicationStore(context);
        return store.haySenal();
    }

}
