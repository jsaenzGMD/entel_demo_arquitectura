package pe.entel.android.plantilla.data.repository.datasource.application;

import android.content.Context;

import pe.entel.android.plantilla.data.repository.datasource.generic.FileStore;
import pe.entel.android.plantilla.data.util.UtilitarioData;

/**
 * Created by rtamayov on 17/04/2017.
 */
public class FotoApplicationStore {

    final Context context;
    final FileStore fileStore;
    final String raiz;

    public FotoApplicationStore(Context context, FileStore fileStore) {
        this.context = context;
        this.fileStore = fileStore;
        raiz = UtilitarioData.getRaiz(context);
    }

    public void guardarFoto(String identificador, byte[] data, GuardarFotoCallback guardarFotoCallback ){

        try {
            fileStore.storeFileImage(data,raiz,identificador);
        } catch (Exception e) {
           guardarFotoCallback.onError(e);
        }

        guardarFotoCallback.onGuardado();
    }

    public String obtenerFotoRuta(String identificador){
        return fileStore.fnRutGrabado(raiz,identificador);
    }

    public void eliminarFoto(String identificador){
        fileStore.deleteOnlyAFile(raiz,identificador);
    }

    public boolean existeFoto(String identificador){
        return fileStore.existFile(raiz,identificador);
    }


    public interface GuardarFotoCallback{
        void onGuardado();
        void onError(Exception e);
    }
}
