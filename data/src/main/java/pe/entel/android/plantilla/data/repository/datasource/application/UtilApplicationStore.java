package pe.entel.android.plantilla.data.repository.datasource.application;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import pe.entel.android.plantilla.data.R;

/**
 * Created by rtamayov on 10/04/2017.
 */
public class UtilApplicationStore {

    final Context context;

    public UtilApplicationStore(Context context) {
        this.context = context;
    }


    public boolean haySenal() {
        boolean lbResultado;
        boolean lbCon3g = false;
        boolean lbconData = false;
        boolean lbWifi = false;

        TelephonyManager loTelephonyManager; //Para ver redes 3G
        ConnectivityManager loConnectivityManager; //Para ver conectividad en general
        NetworkInfo loNetworkInfo;  //Para ver la informacion de la red (Usado con ConnectivityManager)
        loConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        loTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        Log.v("Application", "Operador Net:" + loTelephonyManager.getNetworkOperator());
        Log.v("Application", "Operador Net Name:" + loTelephonyManager.getNetworkOperatorName());
        Log.v("Application", "Operador Sim:" + loTelephonyManager.getSimOperator());
        Log.v("Application", "Operador Sim Name:" + loTelephonyManager.getSimOperatorName());

        if (loTelephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED) {
            lbCon3g = true;

        } else if (loTelephonyManager.getDataState() == TelephonyManager.DATA_DISCONNECTED) {
            lbCon3g = false;
        }
        try {
            //Verifica data
            loNetworkInfo = loConnectivityManager.getActiveNetworkInfo();
            if (loNetworkInfo.isConnected()) {
                lbconData = true;
            } else {
                lbconData = false;
            }

        } catch (SecurityException e) {
            lbconData = false;
        } catch (NullPointerException e) {
            lbconData = false;

        }
        try {
            //verifica wifi
            NetworkInfo loNetworkInfo2 = loConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (loNetworkInfo2.isConnected()) {
                lbWifi = true;
            } else {
                lbWifi = false;
            }
        } catch (SecurityException e) {
            Log.e("Utilitario", e.getMessage());
            lbWifi = false;
        } catch (NullPointerException e) {
            Log.e("Utilitario", e.getMessage());
            lbWifi = false;

        }
        lbResultado = lbCon3g || lbconData || lbWifi;
        return lbResultado;
    }


}
