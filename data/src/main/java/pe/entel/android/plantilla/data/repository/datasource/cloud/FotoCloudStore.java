package pe.entel.android.plantilla.data.repository.datasource.cloud;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import pe.entel.android.plantilla.data.entity.FotoEntity;
import pe.entel.android.plantilla.data.exception.NetworkConnectionException;
import pe.entel.android.plantilla.data.net.RestConnector;
import pe.entel.android.plantilla.data.net.Url;
import pe.entel.android.plantilla.data.net.message.Response;
import pe.entel.android.plantilla.data.net.service.BinaryService;
import pe.entel.android.plantilla.data.util.UtilitarioData;

/**
 * Created by rtamayov on 18/04/2017.
 */
public class FotoCloudStore {

    final Context context;
    EnviarFotoCallback enviarFotoCallback;


    public FotoCloudStore(Context context) {
        this.context = context;
    }


    public void enviarFoto(FotoEntity fotoEntity, final EnviarFotoCallback enviarFotoCallback ){
        this.enviarFotoCallback = enviarFotoCallback;

        if (UtilitarioData.isThereInternetConnection(context)) {
            Gson gson = new Gson();

            Intent intent = new Intent(context, BinaryService.class);
            intent.setData(Uri.parse(Url.obtenerRuta(context,
                    Url.EnumUrl.FOTO)));
            Log.v("URL", Url.obtenerRuta(context, Url.EnumUrl.FOTO));
            intent.putExtra(BinaryService.EXTRA_ONLY_A_PARAM, gson.toJson(fotoEntity));
            intent.putExtra(BinaryService.EXTRA_RESULT_RECEIVER, enviarFoto.getResultReceiver());
            context.startService(intent);
        } else {
            enviarFotoCallback.onError(new NetworkConnectionException("Fuera de cobertura"));
        }

    }

    private RestConnector enviarFoto = new RestConnector() {
        @Override
        public void onRESTResult(int code, String result) {
            EnviarFotoCallback callback = enviarFotoCallback;

            if (code == 200 && result != null) {
                Gson gson = new Gson();
                Response<String> respuesta;

                try {
                    Type type = new TypeToken<Response<String>>() {
                    }.getType();
                    respuesta = gson.fromJson(result, type);
                    Log.v("RESPONSE", gson.toJson(respuesta));
                    if (respuesta.getCode() == Response.CODE_OK) {
                        callback.onEnviada(respuesta.getMessage());
                    } else if (respuesta.getCode() == Response.CODE_ERROR) {
                        callback.onError(new Exception(respuesta.getMessage()));
                    }

                } catch (Exception e) {
                    callback.onError(e);
                }

            } else {
                callback.onError(new Exception(result));
            }
        }

        @Override
        public void onRESTResultBundle(int code, Bundle result) {

        }
    };


    public interface EnviarFotoCallback{
        void onEnviada(String respuesta);
        void onError(Exception e);
    }
}
