package pe.entel.android.plantilla.data.repository.datasource.cloud;

import android.content.Context;
import android.util.Log;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import okhttp3.ResponseBody;
import pe.entel.android.plantilla.data.exception.NetworkConnectionException;
import pe.entel.android.plantilla.data.net.Url;
import pe.entel.android.plantilla.data.net.message.Response;
import pe.entel.android.plantilla.data.util.UtilitarioData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.QueryMap;

/**
 * Created by rtamayov on 03/05/2017.
 */
public class MaestroCloudStore {
    final Context context;
    final Retrofit retrofit;

    public MaestroCloudStore(Context context) {
        this.context = context;
        retrofit = new Retrofit.Builder()
                .baseUrl(Url.obtenerBase(context))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public void sincronizarMaestros(String usuario, final SincronizarMaestrosCallback callback) {
        if (UtilitarioData.isThereInternetConnection(context)) {

            MaestroRetroService service = retrofit.create(MaestroRetroService.class);
            Map<String, String> data = new HashMap<>();
            data.put("usuario", usuario);

            Call<ResponseBody> repos = service.sincronizarMaestros(data);

            Log.v("URL", Url.obtenerRuta(context, Url.EnumUrl.MAESTRO));
            Log.v("usuario", usuario);


            repos.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    if (response.code() == 200 && response.message() != null) {

                        try {
                            okhttp3.Headers headers = response.headers();
                            int idResultado = Integer.parseInt(headers.get("IdResultado"));
                            String resultado = headers.get("Resultado");

                            if (idResultado == Response.CODE_OK) {
                                InputStream loInputStream = response.body().byteStream();
                                loInputStream = new GZIPInputStream(loInputStream);
                                callback.onSincronizacion(resultado, loInputStream);

                            } else if (idResultado == Response.CODE_ERROR) {
                                callback.onError(new Exception(resultado));
                            }

                        } catch (Exception e) {
                            callback.onError(e);
                        }

                    } else {
                        callback.onError(new Exception(response.message()));
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    callback.onError(new Exception(t.getMessage()));
                }
            });


        } else {
            callback.onError(new NetworkConnectionException("Fuera de cobertura"));
        }
    }


    public interface MaestroRetroService {
        @Headers("Accept-Encoding: gzip")
        @GET("Maestro")
        Call<ResponseBody> sincronizarMaestros(@QueryMap Map<String, String> options);
    }

    public interface SincronizarMaestrosCallback {
        void onSincronizacion(String mensaje, InputStream inputStream);

        void onError(Exception e);
    }
}
