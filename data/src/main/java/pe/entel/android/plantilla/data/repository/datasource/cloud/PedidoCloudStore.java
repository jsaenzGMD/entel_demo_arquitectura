package pe.entel.android.plantilla.data.repository.datasource.cloud;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import pe.entel.android.plantilla.data.exception.NetworkConnectionException;
import pe.entel.android.plantilla.data.net.Url;
import pe.entel.android.plantilla.data.net.message.Response;
import pe.entel.android.plantilla.data.util.UtilitarioData;
import pe.entel.android.plantilla.domain.entity.Pedido;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by rtamayov on 03/05/2017.
 */
public class PedidoCloudStore {
    final Context context;
    final Retrofit retrofit;

    public PedidoCloudStore(Context context) {
        this.context = context;
        retrofit = new Retrofit.Builder()
                .baseUrl(Url.obtenerBase(context))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public void enviarPedido(Pedido pedido, final EnviarPedidoCallback callback){

        if(UtilitarioData.isThereInternetConnection(context)){

            PedidoRetroService service = retrofit.create(PedidoRetroService.class);

            Call<Response<String>> repos = service.enviarPedido(pedido);
            Log.v("URL", Url.obtenerRuta(context, Url.EnumUrl.PEDIDO));
            Log.v("REQUEST", new Gson().toJson(pedido));

            repos.enqueue(new Callback<Response<String>>() {
                @Override
                public void onResponse(Call<Response<String>> call, retrofit2.Response<Response<String>> response) {
                    if (response.code() == 200 && response.message() != null) {
                        Response<String> respuesta;
                        try {
                            respuesta = response.body();
                            Log.v("RESPONSE", new Gson().toJson(respuesta));
                            if (respuesta.getCode() == Response.CODE_OK) {
                                callback.onEnviado(respuesta.getMessage());
                            } else if (respuesta.getCode() == Response.CODE_ERROR) {
                                callback.onError(new Exception(respuesta.getMessage()));
                            }
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }else{
                        callback.onError(new Exception(response.message()));
                    }
                }

                @Override
                public void onFailure(Call<Response<String>> call, Throwable t) {
                    callback.onError(new Exception(t.getMessage()));
                }
            });



        }else{
            callback.onError(new NetworkConnectionException("Fuera de cobertura"));
        }



    }

    public interface PedidoRetroService {
        @POST("InsertarPedido")
        Call<Response<String>> enviarPedido(@Body Pedido pedido);
    }

    public interface EnviarPedidoCallback{
        void onEnviado(String mensaje);
        void onError(Exception e);
    }

}
