package pe.entel.android.plantilla.data.repository.datasource.cloud;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import pe.entel.android.plantilla.data.exception.NetworkConnectionException;
import pe.entel.android.plantilla.data.net.Url;
import pe.entel.android.plantilla.data.net.message.Response;
import pe.entel.android.plantilla.data.util.UtilitarioData;
import pe.entel.android.plantilla.domain.entity.Usuario;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by rtamayov on 03/05/2017.
 */
public class UsuarioCloudStore {

    final Context context;
    final Retrofit retrofit;

    public UsuarioCloudStore(Context context) {
        this.context = context;
        retrofit = new Retrofit.Builder()
                .baseUrl(Url.obtenerBase(context))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void validarUsuario(String codigo, String clave, final ValidarUsuarioCallback validarUsuarioCallback){

        if(UtilitarioData.isThereInternetConnection(context)){

            UsuarioRetroService service = retrofit.create(UsuarioRetroService.class);

            Map<String, String> data = new HashMap<>();
            data.put("codigo", codigo);
            data.put("clave", clave);

            Call<Response<Usuario>> repos = service.validarUsuario(data);

            Log.v("URL", Url.obtenerRuta(context, Url.EnumUrl.LOGIN));
            Log.v("codigo", codigo);
            Log.v("clave", clave);

            repos.enqueue(new Callback<Response<Usuario>>() {
                @Override
                public void onResponse(Call<Response<Usuario>> call, retrofit2.Response<Response<Usuario>> response) {

                    if (response.code() == 200 && response.message() != null) {

                        Response<Usuario> respuesta;
                        try {
                            respuesta = response.body();
                            Log.v("RESPONSE", new Gson().toJson(respuesta));
                            if (respuesta.getCode() == Response.CODE_OK) {
                                validarUsuarioCallback.onValidar(respuesta.getMessage(), respuesta.getPayLoad());
                            } else if (respuesta.getCode() == Response.CODE_ERROR) {
                                validarUsuarioCallback.onError(new Exception(respuesta.getMessage()));
                            }

                        } catch (Exception e) {
                            validarUsuarioCallback.onError(e);
                        }

                    }else{
                        validarUsuarioCallback.onError(new Exception(response.message()));
                    }
                }
                @Override
                public void onFailure(Call<Response<Usuario>> call, Throwable t) {
                    validarUsuarioCallback.onError(new Exception(t.getMessage()));
                }
            });


        }else{
            validarUsuarioCallback.onError(new NetworkConnectionException("Fuera de cobertura"));
        }
    }

    public interface UsuarioRetroService {
        @GET("ValidarUsuario")
        Call<Response<Usuario>> validarUsuario(@QueryMap Map<String, String> options);
    }

    public interface ValidarUsuarioCallback{
        void onValidar(String mensaje,Usuario usuario);
        void onError(Exception e);



    }
}
