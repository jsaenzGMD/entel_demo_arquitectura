package pe.entel.android.plantilla.data.repository.datasource.cloud;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import pe.entel.android.plantilla.data.exception.NetworkConnectionException;
import pe.entel.android.plantilla.data.net.Url;
import pe.entel.android.plantilla.data.net.message.Response;
import pe.entel.android.plantilla.data.util.UtilitarioData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by rtamayov on 03/05/2017.
 */
public class VersionCloudStore {

    final Context context;
    final Retrofit retrofit;


    public VersionCloudStore(Context context) {
        this.context = context;
        retrofit = new Retrofit.Builder()
                .baseUrl(Url.obtenerBase(context))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void obtenerVersion(final ObtenerVersionCallback obtenerVersionCallback){

        if(UtilitarioData.isThereInternetConnection(context)){

            VersionRetroService service = retrofit.create(VersionRetroService.class);
            Call<Response<String>> repos = service.obtenerVersion();

            Log.v("URL", Url.obtenerRuta(context, Url.EnumUrl.VERSION));

            repos.enqueue(new Callback<Response<String>>() {
                @Override
                public void onResponse(Call<Response<String>> call, retrofit2.Response<Response<String>> response) {
                    if (response.code() == 200 && response.message() != null) {
                        Response<String> respuesta;
                        try {
                            respuesta = response.body();
                            Log.v("RESPONSE", new Gson().toJson(respuesta));
                            if (respuesta.getCode() == Response.CODE_OK){
                                obtenerVersionCallback.obtenida(respuesta.getPayLoad());
                            }else if (respuesta.getCode() == Response.CODE_ERROR) {
                                obtenerVersionCallback.onError(new Exception(respuesta.getMessage()));
                            }
                        } catch (Exception e) {
                            obtenerVersionCallback.onError(e);
                        }
                    }else{
                        obtenerVersionCallback.onError(new Exception(response.message()));
                    }
                }

                @Override
                public void onFailure(Call<Response<String>> call, Throwable t) {
                    obtenerVersionCallback.onError(new Exception(t.getMessage()));

                }
            });

        }else{
            obtenerVersionCallback.onError(new NetworkConnectionException("Fuera de cobertura"));
        }

    }




    public interface VersionRetroService {
        @GET("Version")
        Call<Response<String>> obtenerVersion();
    }

    public interface ObtenerVersionCallback {
        void obtenida(String version);
        void onError(Exception e);
    }
}
