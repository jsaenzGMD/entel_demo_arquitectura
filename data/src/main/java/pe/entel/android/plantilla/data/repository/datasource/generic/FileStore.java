package pe.entel.android.plantilla.data.repository.datasource.generic;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import pe.entel.android.plantilla.data.util.UtilitarioData;

/**
 * Created by asoto on 02/03/2015.
 */
public class FileStore {

    private static final String TAG = FileStore.class.getName();
    private Context context;
    public FileStore(Context context){
        this.context = context;
    }

    private static final int MEDIA_TYPE_IMAGE = 1;


    public void storeFileImage(byte[] data, String root, String path) throws Exception {

        Uri uri = GestorPicture.fnSavePath(context, root,path+".jpg");
        Log.e(TAG,"=================");
        Log.e(TAG,path+".jpg");
        Log.e(TAG,"***************");
        File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE,uri);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(pictureFile);
            fos.write(data);
        }finally {
            if (fos != null)
             fos.close();
        }

    }
    private File getOutputMediaFile(int type, Uri ioUriPicture) {
        File loFile = new File(ioUriPicture.getPath());
        return loFile;
    }

    public void writeToStream(DataOutputStream outputStream, String root, String path) throws IOException {

        int bytesRead, bytesAvailable, bufferSize;
        int maxBufferSize = 1 * 1024 * 1024;
        byte[] buffer;

        FileInputStream fileInputStream = null;
        try {
            File loFile = new File(fnRutGrabado(root, path));
            String pathToFile = loFile.getPath();


            fileInputStream = new FileInputStream(new File(pathToFile));

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }
        }finally {
            if (fileInputStream != null)
              fileInputStream.close();
        }

    }
    public String fnRutGrabado(String raiz, String ruta){
        String newArchivo = ruta+".jpg";
        String lsRuta = "";
        /*newArchivo = StringUtils.replace(ruta, "|", "#");
        newArchivo = StringUtils.replace(newArchivo, "/", "&");
        newArchivo = StringUtils.replace(newArchivo, ":", "$");*/
        lsRuta = Environment.getExternalStorageDirectory()
                + UtilitarioData.fnNomCarFoto(raiz) + newArchivo;
        return lsRuta;
    }
    public void deleteOnlyAFile(String raiz, String ruta){
        File loFile = new File(fnRutGrabado(raiz, ruta));
        loFile.mkdirs();
        if (loFile.exists()) {
            loFile.delete();
        }
    }


    public boolean encontrarFoto(String raiz, final String pathFile){
        boolean encontrado = false;

        FileFilter loFileFilter = new FileFilter() {
            public boolean accept(File file) {
                Log.v(FileStore.TAG,"existe: " + pathFile + " ///// " + file.getPath());
                return file.isFile();
            }
        };

        String lsCarpeta = Environment.getExternalStorageDirectory() + UtilitarioData.fnNomCarFoto(raiz);
        File loFileDirectorio = new File(lsCarpeta);

        File[] files = loFileDirectorio.listFiles(loFileFilter);

        if (files.length > 0) {
            for (int i = 0; i < files.length; i++) {
                encontrado = true;
            }
        }

        return encontrado;
    }


    public boolean existFile(String raiz, final String ruta){
        File loFile = new File(fnRutGrabado(raiz, ruta));
        loFile.mkdirs();
        if (loFile.exists()) {
            return true;
        }

        return false;
    }




    public void deleteFiles(String raiz, final String prefijoMayor){
        FileFilter loFileFilter = new FileFilter() {
            public boolean accept(File file) {
                return file.isFile()&&file.getName().compareTo(prefijoMayor)<=0;
            }
        };

        String lsCarpeta = Environment.getExternalStorageDirectory() + UtilitarioData.fnNomCarFoto(raiz);
        File loFileDirectorio = new File(lsCarpeta);

        File[] files = loFileDirectorio.listFiles(loFileFilter);
        if (files.length > 0) {
            for (int i = 0; i < files.length; i++) {
                Log.v(TAG," BORRANDO FOTO ----> " + files[i].getName());
                files[i].delete();
            }
        } else{
            Log.v(TAG,"no hay fotos para borrar");
        }
    }


}
