package pe.entel.android.plantilla.data.repository.datasource.generic;

/**
 * Created by asoto on 02/03/2015.
 */

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;

import pe.entel.android.plantilla.data.util.UtilitarioData;

/**
 * Clase gestor de im&#225;genes para la captura de fotos
 * @author jroeder
 * @version 1.1
 */
public class GestorPicture {
    /**
     * Colecci&#243;n enumerada de las resoluciones de c&#225;mara: VGA, 1Mpx, 1.3Mpx, 2Mpx, 3Mpx y 5Mpx
     */
    public static enum EnumResolution {MPX_VGA,MPX_1,MPX_1_3,MPX_2,MPX_3,MPX_5};
    /**
     * Colecci&#243;n enumerada de las posiciones de la captura de foto: Vertical y Horizontal
     */
    public static enum EnumPosition {VERTICAL,HORIZONTAL};
    /**
     * Constante del par&#225;metro de Ancho de captura de foto
     */
    public final static int WIDTH = 0;
    /**
     * Constante del par&#243;metro de Altura de captura de foto
     */
    public final static int HEIGHT = 1;

    /**
     * Retorna el ancho y altura en un arreglo de enteros de una resoluci&#243;n y posici&#243;n
     * @param poResolution Resoluci&#243;n VGA, 1Mpx, 1.3Mpx, 2Mpx, 3Mpx y 5Mpx
     * @param poPosition Posici&#243;n Vertical y Horizontal
     * @return <b>int[2]</b> Arreglo con Ancho y Altura. Se usan las constantes WIDTH y HEIGHT
     * como &#237;ndices para obtener los valores. Retorna null en caso se ingresen valores
     * que no pertenecen a la colecci&#243;n
     */
    public static int[] fnResolution(EnumResolution poResolution, EnumPosition poPosition){
        int[] loArrResolution = new int[2];
        if(poResolution == EnumResolution.MPX_1){
            loArrResolution[0] = 1024;
            loArrResolution[1] = 768;
            if(poPosition == EnumPosition.VERTICAL){
                loArrResolution[0] = 768;
                loArrResolution[1] = 1024;
            }
            return loArrResolution;
        }else if(poResolution == EnumResolution.MPX_1_3){
            loArrResolution[0] = 1280;
            loArrResolution[1] = 960;
            if(poPosition == EnumPosition.VERTICAL){
                loArrResolution[0] = 960;
                loArrResolution[1] = 1280;
            }
            return loArrResolution;
        }else if(poResolution == EnumResolution.MPX_2){
            loArrResolution[0] = 1600;
            loArrResolution[1] = 1200;
            if(poPosition == EnumPosition.VERTICAL){
                loArrResolution[0] = 1200;
                loArrResolution[1] = 1600;
            }
            return loArrResolution;
        }else if(poResolution == EnumResolution.MPX_3){
            loArrResolution[0] = 2048;
            loArrResolution[1] = 1536;
            if(poPosition == EnumPosition.VERTICAL){
                loArrResolution[0] = 1536;
                loArrResolution[1] = 2048;
            }
            return loArrResolution;
        }else if(poResolution == EnumResolution.MPX_5){
            loArrResolution[0] = 2592;
            loArrResolution[1] = 1944;
            if(poPosition == EnumPosition.VERTICAL){
                loArrResolution[0] = 1944;
                loArrResolution[1] = 2592;
            }
            return loArrResolution;
        }else if(poResolution == EnumResolution.MPX_VGA){
            loArrResolution[0] = 640;
            loArrResolution[1] = 480;
            if(poPosition == EnumPosition.VERTICAL){
                loArrResolution[0] = 480;
                loArrResolution[1] = 640;
            }
            return loArrResolution;
        }
        return null;
    }


    /**
     * Retorn el valor del Par&#225;metro de Ancho o Altura seg&#250;n la constante, resoluci&#243;n y posici&#243;n seleccionada
     * @param piWidthHeight Constante WIDTH o HEIGHT
     * @param poResolution Resoluci&#243;n VGA, 1Mpx, 1.3Mpx, 2Mpx, 3Mpx y 5Mpx
     * @param poPosition Posici&#243;n Vertical y Horizontal
     * @return <b>int</b> Valor del Ancho o Altura de la resoluci&#243;n seleccionada. Retorna -1 en caso se ingrese un
     * par&#225;metro no correspondiente
     */
    public static int fnResolution(int piWidthHeight, EnumResolution poResolution, EnumPosition poPosition){
        try{
            if(piWidthHeight == GestorPicture.WIDTH
                    || piWidthHeight == GestorPicture.HEIGHT)
                return GestorPicture.fnResolution(poResolution, poPosition)[piWidthHeight];
            return -1;
        }catch (Exception e) {
            return -1;
        }
    }

    /**
     * Crea un objeto del tipo {@link Uri}, haciendo referencia a la ruta de la captura de foto.
     * Utiliza la carpeta por defecto de foto de la aplicaci&#243;n.
     * @param poContext Contexto de la aplicaci&#243;n
     * @param piResourceAppName &#205;ndice del recurso del nombre de la aplicaci&#243;n (R.string.app_name)
     * @param psFileName Nombre del archivo de captura de foto
     * @return {@link Uri} Objeto con referencia a la captura de foto
     * @throws Exception
     */
    public static Uri fnSavePath(Context poContext, int piResourceAppName, String psFileName) throws Exception {
        Log.v("GestorPicture", "fnSavePath " + psFileName);
        return GestorPicture.fnSavePath(poContext, piResourceAppName, psFileName, psFileName.contains(poContext.getString(piResourceAppName)));
    }

    /**
     * Crea un objeto del tipo {@link Uri}, haciendo referencia a la ruta de la captura de foto.
     * Utiliza la carpeta por defecto de foto de la aplicaci&#243;n.
     * @param poContext Contexto de la aplicaci&#243;n
     * @param piResourceAppName &#205;ndice del recurso del nombre de la aplicaci&#243;n (R.string.app_name)
     * @param psFileName Nombre del archivo de captura de foto
     * @param pathComplete Verifica si el nombre de archivo contiene la ruta completa
     * <ul>
     * <li><b>true</b> En caso tenga la ruta completa</li>
     * <li><b>false</b> En caso tenga la ruta incompleta</li>
     * </ul>
     * @return {@link Uri} Objeto con referencia a la captura de foto
     * @throws Exception
     */
    public static Uri fnSavePath(Context poContext, int piResourceAppName, String psFileName, boolean pathComplete) throws Exception {
        Log.v("GestorPicture", "fnSavePath " + psFileName);
        String lsPath = "";
        lsPath = Environment.getExternalStorageDirectory()+ UtilitarioData.fnNomCarFoto(poContext.getString(piResourceAppName)) ;
        File loDirectory=new File(lsPath);
        loDirectory.mkdirs();
        lsPath = !pathComplete? (lsPath + psFileName) : psFileName;
        File loFile = new File(lsPath);
        Uri loUri= Uri.fromFile(loFile);
        Log.v("PRO", "image's path: " + lsPath);
        return loUri;
    }

    /**
     * Crea un objeto del tipo {@link Uri}, haciendo referencia a la ruta de la captura de foto.
     * @param psFolderName Nombre de la carpeta de foto
     * @param psFileName Nombre del archivo de captura de foto
     * @return {@link Uri} Objeto con referencia a la captura de foto
     * @throws Exception
     */
    public static Uri fnSavePath(String psFolderName, String psFileName) throws Exception {
        Log.v("GestorPicture", "fnSavePath " + psFileName);
        return GestorPicture.fnSavePath(psFolderName, psFileName, psFileName.contains(psFolderName));
    }

    /**
     * Crea un objeto del tipo {@link Uri}, haciendo referencia a la ruta de la captura de foto.
     * @param psFolderName Nombre de la carpeta de foto
     * @param psFileName Nombre del archivo de captura de foto
     * @param pathComplete Verifica si el nombre de archivo contiene la ruta completa
     * <ul>
     * <li><b>true</b> En caso tenga la ruta completa</li>
     * <li><b>false</b> En caso tenga la ruta incompleta</li>
     * </ul>
     * @return {@link Uri} Objeto con referencia a la captura de foto
     * @throws Exception
     */
    public static Uri fnSavePath(String psFolderName, String psFileName, boolean pathComplete) throws Exception {
        Log.v("GestorPicture", "fnSavePath " + psFileName);
        String lsPath = "";
        lsPath = Environment.getExternalStorageDirectory()+ psFolderName ;
        File loDirectory=new File(lsPath);
        loDirectory.mkdirs();
        lsPath = !pathComplete? (lsPath + psFileName) : psFileName;
        File loFile = new File(lsPath);
        Uri loUri= Uri.fromFile(loFile);
        Log.v("PRO", "image's path: " + lsPath);
        return loUri;
    }

    /**
     * Re-Dimensiona la captura de foto a una nueva resoluci&#243;n en base a una proporci&#243;n, retornando un objeto del tipo {@link Bitmap} con la imagen con nuevas dimensiones
     * @param poUri Objeto del tipo {@link Uri} con referencia a la captura de foto
     * @param poResolutionHigh Resoluci&#243;n m&#225;s cercana a la cual se desea Redimensionar
     * @param pbUpdateFile Bandera que indica si se actualiza la captura de foto original en la ruta del {@link Uri} especificado
     * @return {@link Bitmap} Objeto de la captura de foto con las nuevas dimensiones
     * @throws Exception
     */
    public static Bitmap fnReSizeBitmap(Uri poUri, EnumResolution poResolutionHigh, boolean pbUpdateFile) throws Exception {
        return GestorPicture.fnReSizeBitmap(poUri.getPath(), poResolutionHigh, pbUpdateFile);
    }

    /**
     * Re-Dimensiona la captura de foto a una nueva resoluci&#243;n en base a una proporci&#243;n, retornando un objeto del tipo {@link Bitmap} con la imagen con nuevas dimensiones
     * @param psPath Ruta de archivo de la captura de foto
     * @param poResolutionHigh Resoluci&#243;n m&#225;s cercana a la cual se desea Redimensionar
     * @param pbUpdateFile Bandera que indica si se actualiza la captura de foto original en la ruta del {@link Uri} especificado
     * @return {@link Bitmap} Objeto de la captura de foto con las nuevas dimensiones
     * @throws Exception
     */
    public static Bitmap fnReSizeBitmap(String psPath, EnumResolution poResolutionHigh, boolean pbUpdateFile) throws Exception {
        Log.v("GestorPicture", "fnReSizeBitmap " + psPath);

        BitmapFactory.Options loBitmapOptionsOriginal = new BitmapFactory.Options();
        loBitmapOptionsOriginal.inJustDecodeBounds = true;
        loBitmapOptionsOriginal.inSampleSize = 1;

        BitmapFactory.decodeFile(psPath, loBitmapOptionsOriginal);

        int liWidthHigh, liHeightHigh;
        int liWidthLow, liHeightLow;

        int liResolutionOrdinal = 0;
        if (poResolutionHigh.ordinal() >= 0){
            liResolutionOrdinal = poResolutionHigh.ordinal() - 1;
        }

        EnumResolution loResolutionInferior = EnumResolution.values()[liResolutionOrdinal];

        if(loBitmapOptionsOriginal.outWidth >= loBitmapOptionsOriginal.outHeight){
            Log.v("PRO", "HORIZONTAL");
            liWidthHigh = GestorPicture.fnResolution(GestorPicture.WIDTH, poResolutionHigh, EnumPosition.HORIZONTAL);
            liHeightHigh = GestorPicture.fnResolution(GestorPicture.HEIGHT, poResolutionHigh, EnumPosition.HORIZONTAL);
        }else{
            Log.v("PRO", "VERTICAL");
            liWidthHigh = GestorPicture.fnResolution(GestorPicture.WIDTH, poResolutionHigh, EnumPosition.VERTICAL);
            liHeightHigh = GestorPicture.fnResolution(GestorPicture.HEIGHT, poResolutionHigh, EnumPosition.VERTICAL);
        }

        Log.v("PRO", "Resolucion Superior: " + String.valueOf(poResolutionHigh));
        Log.v("PRO", "liWidthHigh = " + liWidthHigh);
        Log.v("PRO", "liHeightHigh = " + liHeightHigh);

        if(loBitmapOptionsOriginal.outWidth >= loBitmapOptionsOriginal.outHeight){
            Log.v("PRO", "HORIZONTAL MENOR");
            liWidthLow = GestorPicture.fnResolution(GestorPicture.WIDTH, loResolutionInferior, EnumPosition.HORIZONTAL);
            liHeightLow = GestorPicture.fnResolution(GestorPicture.HEIGHT, loResolutionInferior, EnumPosition.HORIZONTAL);
        }else{
            Log.v("PRO", "VERTICAL MENOR");
            liWidthLow = GestorPicture.fnResolution(GestorPicture.WIDTH, loResolutionInferior, EnumPosition.VERTICAL);
            liHeightLow = GestorPicture.fnResolution(GestorPicture.HEIGHT, loResolutionInferior, EnumPosition.VERTICAL);
        }

        Log.v("PRO", "Resolucion Inferior: " + String.valueOf(poResolutionHigh));
        Log.v("PRO", "liWidthLow = " + liWidthLow);
        Log.v("PRO", "liHeightLow = " + liHeightLow);

        BitmapFactory.Options loBitmapOptionsHigh = new BitmapFactory.Options();
        BitmapFactory.Options loBitmapOptionsLow = new BitmapFactory.Options();

        int liWidthRatio = (int) ((float)loBitmapOptionsOriginal.outWidth /(float) liWidthHigh);
        int liHeightRatio = (int) ((float)loBitmapOptionsOriginal.outHeight /(float) liHeightHigh);

        loBitmapOptionsHigh.inSampleSize = liHeightRatio < liWidthRatio ? (liHeightRatio != 0? liHeightRatio : 1) : (liWidthRatio != 0? liWidthRatio : 1);

        Log.v("PRO", "Superior inSampleSize: " + loBitmapOptionsHigh.inSampleSize);

        int widthRatioMenor = (int) ((float)loBitmapOptionsOriginal.outWidth /(float) liWidthLow);
        int heightRatioMenor = (int) ((float)loBitmapOptionsOriginal.outHeight /(float) liHeightLow);

        loBitmapOptionsLow.inSampleSize = heightRatioMenor < widthRatioMenor ? heightRatioMenor : widthRatioMenor;

        Log.v("PRO", "Inferior inSampleSize: " + loBitmapOptionsLow.inSampleSize);

        double ldWidthHigh = loBitmapOptionsOriginal.outWidth / loBitmapOptionsHigh.inSampleSize;
        double ldWidthLow = loBitmapOptionsOriginal.outWidth / loBitmapOptionsLow.inSampleSize;

        double ldDifHigh = liWidthHigh - ldWidthHigh;
        double ldDifLow = liWidthHigh - ldWidthLow;

        Bitmap loImageNew;
        if (ldDifHigh < ldDifLow){
            loImageNew = BitmapFactory.decodeFile(psPath, loBitmapOptionsHigh);
        }else{
            loImageNew = BitmapFactory.decodeFile(psPath, loBitmapOptionsLow);
        }

        loBitmapOptionsOriginal= null;

        Log.v("PRO", "loImageNew.getWidth() = " + loImageNew.getWidth());
        Log.v("PRO", "loImageNew.getHeight() = " + loImageNew.getHeight());

        if(pbUpdateFile){
            File loFile = new File(psPath);
            loFile.mkdirs();
            if(loFile.exists()){
                Log.v("PRO", "Existe. Borramos imagen anterior");
                loFile.delete();
            }

            FileOutputStream loFOUT = new FileOutputStream(loFile);
            Log.v("PRO","se comprime foto: " + loImageNew.compress(Bitmap.CompressFormat.JPEG, 100, loFOUT));
            loFOUT.flush();
            loFOUT.close();
        }

        return loImageNew;
    }

    /**
     * Escala la captura de foto, retornando un objeto del tipo {@link Bitmap} con la imagen con nuevas dimensiones
     * @param poUri Objeto del tipo {@link Uri} con referencia a la captura de foto
     * @param poResolution Resoluci&#243;n al cual se desea Redimensionar
     * @param pbUpdateFile Bandera que indica si se actualiza la captura de foto original en la ruta del {@link Uri} especificado
     * @return {@link Bitmap} Objeto de la captura de foto con las nuevas dimensiones
     * @throws Exception
     */
    public static Bitmap fnScaleBitmap(Uri poUri, EnumResolution poResolution, boolean pbUpdateFile) throws Exception {
        return GestorPicture.fnScaleBitmap(poUri.getPath(), poResolution, pbUpdateFile);
    }

    /**
     * Escala la captura de foto, retornando un objeto del tipo {@link Bitmap} con la imagen con nuevas dimensiones
     * @param psPath Ruta de archivo de la captura de foto
     * @param poResolution poResolution Resoluci&#243;n al cual se desea Redimensionar
     * @param pbUpdateFile Bandera que indica si se actualiza la captura de foto original en la ruta del {@link Uri} especificado
     * @return {@link Bitmap} Objeto de la captura de foto con las nuevas dimensiones
     * @throws Exception
     */
    public static Bitmap fnScaleBitmap(String psPath, EnumResolution poResolution, boolean pbUpdateFile) throws Exception {
        Log.v("GestorPicture", "fnScaleBitmap " + psPath);

        Bitmap loImageOriginal = BitmapFactory.decodeFile(psPath);
        Log.v("PRO", "loOriginalImage.getWidth() = " + loImageOriginal.getWidth());
        Log.v("PRO", "loOriginalImage.getHeight() = " + loImageOriginal.getHeight());

        int liWidth, liHeight;

        if(loImageOriginal.getWidth() >= loImageOriginal.getHeight()){
            Log.v("PRO", "HORIZONTAL");
            liWidth = GestorPicture.fnResolution(GestorPicture.WIDTH, poResolution, EnumPosition.HORIZONTAL);
            liHeight = GestorPicture.fnResolution(GestorPicture.HEIGHT, poResolution, EnumPosition.HORIZONTAL);
        }else{
            Log.v("PRO", "VERTICAL");
            liWidth = GestorPicture.fnResolution(GestorPicture.WIDTH, poResolution, EnumPosition.VERTICAL);
            liHeight = GestorPicture.fnResolution(GestorPicture.HEIGHT, poResolution, EnumPosition.VERTICAL);
        }

        Log.v("PRO", "liWidth = " + liWidth);
        Log.v("PRO", "liHeight = " + liHeight);

        Bitmap loImageNew = Bitmap.createScaledBitmap(loImageOriginal,liWidth,liHeight,false);
        Log.v("PRO", "loNewImage.getWidth() = " + loImageNew.getWidth());
        Log.v("PRO", "loNewImage.getHeight() = " + loImageNew.getHeight());

        if(pbUpdateFile){
            File loFile = new File(psPath);
            loFile.mkdirs();
            if(loFile.exists()){
                Log.v("PRO", "Existe. Borramos imagen anterior");
                loFile.delete();
            }

            FileOutputStream loFOUT = new FileOutputStream(loFile);
            Log.v("PRO","se comprime foto: " + loImageNew.compress(Bitmap.CompressFormat.JPEG, 100, loFOUT));
            loFOUT.flush();
            loFOUT.close();
        }

        return loImageNew;
    }

    /**
     * Escala la captura de foto, retornando un objeto del tipo {@link Bitmap} con la imagen con nuevas dimensiones
     * @param poBitmap Objeto del tipo Bitmap de imagen la cual se desea redimensionar
     * @param psPath Ruta de archivo en la cual se desea almacenar la imagen redimensionada. Si es NULL o caracter
     * vac&#237;o, no se realizar&#225; grabado en archivo
     * @param poResolution Resoluci&#243;n a la cual se desea redimensionar
     * @return {@link Bitmap} Objeto de la captura de foto con las nuevas dimensiones
     * @throws Exception
     */
    public static Bitmap fnScaleBitmap(Bitmap poBitmap, String psPath, EnumResolution poResolution) throws Exception {
        Log.v("GestorPicture", "fnScaleBitmap ");

        Log.v("PRO", "poBitmap.getWidth() = " + poBitmap.getWidth());
        Log.v("PRO", "poBitmap.getHeight() = " + poBitmap.getHeight());

        int liWidth, liHeight;

        if(poBitmap.getWidth()>=poBitmap.getHeight()){
            Log.v("PRO", "HORIZONTAL");
            liWidth = GestorPicture.fnResolution(GestorPicture.WIDTH, poResolution, EnumPosition.HORIZONTAL);
            liHeight = GestorPicture.fnResolution(GestorPicture.HEIGHT, poResolution, EnumPosition.HORIZONTAL);
        }else{
            Log.v("PRO", "VERTICAL");
            liWidth = GestorPicture.fnResolution(GestorPicture.WIDTH, poResolution, EnumPosition.VERTICAL);
            liHeight = GestorPicture.fnResolution(GestorPicture.HEIGHT, poResolution, EnumPosition.VERTICAL);
        }

        Log.v("PRO", "liWidth = " + liWidth);
        Log.v("PRO", "liHeight = " + liHeight);

        Bitmap loImageNew = Bitmap.createScaledBitmap(poBitmap,liWidth,liHeight,false);
        Log.v("PRO", "loNewImage.getWidth() = " + loImageNew.getWidth());
        Log.v("PRO", "loNewImage.getHeight() = " + loImageNew.getHeight());

        if(psPath != null){
            if(!psPath.equals("")){
                File loFile = new File(psPath);
                loFile.mkdirs();
                if(loFile.exists()){
                    Log.v("PRO", "Existe. Borramos imagen anterior");
                    loFile.delete();
                }

                FileOutputStream loFOUT = new FileOutputStream(loFile);
                Log.v("PRO","se comprime foto: " + loImageNew.compress(Bitmap.CompressFormat.JPEG, 100, loFOUT));
                loFOUT.flush();
                loFOUT.close();
            }
        }

        return loImageNew;
    }

    /**
     * Escala la captura de foto, retornando un objeto del tipo {@link Bitmap} con la imagen con nuevas dimensiones
     * @param poBitmap Objeto del tipo Bitmap de imagen la cual se desea redimensionar
     * @param poResolution Resoluci&#243;n a la cual se desea redimensionar
     * @return {@link Bitmap} Objeto de la captura de foto con las nuevas dimensiones
     * @throws Exception
     */
    public static Bitmap fnScaleBitmap(Bitmap poBitmap, EnumResolution poResolution) throws Exception {
        Log.v("GestorPicture", "fnScaleBitmap ");

        Log.v("PRO", "poBitmap.getWidth() = " + poBitmap.getWidth());
        Log.v("PRO", "poBitmap.getHeight() = " + poBitmap.getHeight());

        int liWidth, liHeight;

        if(poBitmap.getWidth()>=poBitmap.getHeight()){
            Log.v("PRO", "HORIZONTAL");
            liWidth = GestorPicture.fnResolution(GestorPicture.WIDTH, poResolution, EnumPosition.HORIZONTAL);
            liHeight = GestorPicture.fnResolution(GestorPicture.HEIGHT, poResolution, EnumPosition.HORIZONTAL);
        }else{
            Log.v("PRO", "VERTICAL");
            liWidth = GestorPicture.fnResolution(GestorPicture.WIDTH, poResolution, EnumPosition.VERTICAL);
            liHeight = GestorPicture.fnResolution(GestorPicture.HEIGHT, poResolution, EnumPosition.VERTICAL);
        }

        Log.v("PRO", "liWidth = " + liWidth);
        Log.v("PRO", "liHeight = " + liHeight);

        Bitmap loImageNew = Bitmap.createScaledBitmap(poBitmap,liWidth,liHeight,false);
        Log.v("PRO", "loImageNew.getWidth() = " + loImageNew.getWidth());
        Log.v("PRO", "loImageNew.getHeight() = " + loImageNew.getHeight());

        return loImageNew;
    }

    /**
     * Crea un {@link Intent} para captura de foto utilizando la acci&#243;n {@link MediaStore#ACTION_IMAGE_CAPTURE} del {@link MediaStore}
     * @param poContext Contexto de la aplicaci&#243;n
     * @param piResourceAppName Indice del recurso del nombre de la aplicaci&#243;n (R.string.app_name)
     * @param psImageName Nombre de la imagen o captura de foto
     * @return {@link Intent} con la acci&#243;n de captura de foto
     * @throws Exception
     */
    public static Intent fnIntentPicture(Context poContext, int piResourceAppName, String psImageName) throws Exception {
        Log.v("PRO", "fnIntentPicture");
        ContentValues loValues = new ContentValues();
        loValues.put(MediaStore.Images.Media.TITLE, psImageName);
        Uri loCapturedImageURI = GestorPicture.fnSavePath(poContext, piResourceAppName, psImageName);
        Log.v("PRO", "path: " + loCapturedImageURI.getEncodedPath());
        Intent loIntentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        loIntentPicture.putExtra(MediaStore.EXTRA_OUTPUT, loCapturedImageURI);
        return loIntentPicture;
    }

    /**
     * Crea un {@link Intent} para captura de foto utilizando la acci&#243;n {@link MediaStore#ACTION_IMAGE_CAPTURE} del {@link MediaStore}
     * @param poURIPicture Objeto {@link Uri} con referencia a la captura de foto
     * @return {@link Intent} con la acci&#243;n de captura de foto
     * @throws Exception
     */
    public static Intent fnIntentPicture(Uri poURIPicture) throws Exception {
        Log.v("PRO", "fnIntentPicture");
        Intent loIntentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        loIntentPicture.putExtra(MediaStore.EXTRA_OUTPUT, poURIPicture);
        Log.v("PRO", "path: " + poURIPicture.getEncodedPath());
        return loIntentPicture;
    }

    /**
     * Rota una imagen en {@link Bitmap}, para verse siempre en vertical. </br>Es invocado siempre despu&#233;s de la captura de imagen.
     * @param psPath Ruta de la foto o imagen {@link Bitmap}
     * @return {@link Bitmap}
     * @throws Exception
     */
    public static Bitmap fnRotateBitmap(String psPath) throws Exception {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[32 * 1024];
        options.inPreferredConfig = Bitmap.Config.RGB_565;

        Bitmap bMap = BitmapFactory.decodeFile(psPath, options);

        ExifInterface exif = new ExifInterface(psPath);

        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        int rotationInDegrees = fnExifToDegrees(rotation);

        Bitmap bMapRotate;
        if (rotationInDegrees != 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(rotationInDegrees);
            bMapRotate = Bitmap.createBitmap(bMap, 0, 0, bMap.getWidth(),
                    bMap.getHeight(), matrix, true);
        } else
            bMapRotate = Bitmap.createScaledBitmap(bMap, bMap.getWidth(),
                    bMap.getHeight(), true);

        File loFile = new File(psPath);
        loFile.mkdirs();
        if(loFile.exists()){
            loFile.delete();
        }

        FileOutputStream loFOUT = new FileOutputStream(loFile);

        Log.v("XXX","se comprime foto: " + bMapRotate.compress(Bitmap.CompressFormat.JPEG, 100, loFOUT));

        loFOUT.flush();
        loFOUT.close();

        return bMapRotate;
    }
    /**
     * Retorna la orientaci&#243;n en grados.
     * @param exifOrientation Valor de Atributo de Orientaci&#243;n de la imagen
     * @return <b>int</b> orientaci&#243;n en grados
     */
    private static int fnExifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }

    /**
     * Crea un objeto del tipo {@link Uri}, haciendo referencia a la ruta de la captura de foto.
     * Utiliza la carpeta por defecto de foto de la aplicaci&#243;n.
     * @param poContext Contexto de la aplicaci&#243;n
     * @param psResourceAppName nombre de la aplicaci&#243;n (R.string.app_name)
     * @param psFileName Nombre del archivo de captura de foto
     * @return {@link Uri} Objeto con referencia a la captura de foto
     * @throws Exception
     */
    public static Uri fnSavePath(Context poContext, String psResourceAppName, String psFileName) throws Exception {
        Log.v("GestorPicture", "fnSavePath " + psFileName);
        return GestorPicture.fnSavePath(poContext, psResourceAppName, psFileName, psFileName.contains(psResourceAppName));
    }
    /**
     * Crea un objeto del tipo {@link Uri}, haciendo referencia a la ruta de la captura de foto.
     * Utiliza la carpeta por defecto de foto de la aplicaci&#243;n.
     * @param poContext Contexto de la aplicaci&#243;n
     * @param psResourceAppName nombre de la aplicaci&#243;n (R.string.app_name)
     * @param psFileName Nombre del archivo de captura de foto
     * @param pathComplete Verifica si el nombre de archivo contiene la ruta completa
     * <ul>
     * <li><b>true</b> En caso tenga la ruta completa</li>
     * <li><b>false</b> En caso tenga la ruta incompleta</li>
     * </ul>
     * @return {@link Uri} Objeto con referencia a la captura de foto
     * @throws Exception
     */
    public static Uri fnSavePath(Context poContext, String psResourceAppName, String psFileName, boolean pathComplete) throws Exception {
        Log.v("GestorPicture", "fnSavePath " + psFileName);
        String lsPath = "";
        lsPath = Environment.getExternalStorageDirectory()+ UtilitarioData.fnNomCarFoto(psResourceAppName) ;
        File loDirectory=new File(lsPath);
        loDirectory.mkdirs();
        lsPath = !pathComplete? (lsPath + psFileName) : psFileName;
        File loFile = new File(lsPath);
        Uri loUri= Uri.fromFile(loFile);
        Log.v("PRO", "image's path: " + lsPath);
        return loUri;
    }

}
