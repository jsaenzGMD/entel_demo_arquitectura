package pe.entel.android.plantilla.data.repository.datasource.local;

import java.util.List;
import java.util.ArrayList;

import pe.entel.android.plantilla.data.dao.FotoDao;

/**
 * Created by rtamayov on 18/04/2017.
 */
public class FotoLocalStore {

    public void guardarFoto(String idUsuario, String identificador){
        FotoDao fotoDao = new FotoDao();
        fotoDao.setIdUsuario(idUsuario);
        fotoDao.setIdentificador(identificador);
        fotoDao.setFlgEnviado("F");
        fotoDao.save();
    }

    public void borrarFoto(String identificador){
        FotoDao.deleteAll(FotoDao.class,"identificador = ?",identificador);
    }

    public FotoDao obtenerFoto(String identificador){
        return FotoDao.find(FotoDao.class,"identificador = ?",identificador).get(0);
    }

    public void guardarFoto(FotoDao fotoDao){
        fotoDao.save();
    }

    public List<FotoDao> listarFotosPendientes(){
        return FotoDao.find(FotoDao.class,"flg_enviado = 'F'");
    }

}
