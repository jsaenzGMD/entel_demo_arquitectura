package pe.entel.android.plantilla.data.repository.datasource.local;

import android.util.Log;

import com.orm.SugarRecord;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import pe.entel.android.plantilla.data.dao.ProductoDao;

/**
 * Created by rtamayov on 11/04/2017.
 */

public class MaestroLocalStore {

    public void borrarTodo(){
        ProductoDao.deleteAll(ProductoDao.class);
    }

    public void insertarQuery(InputStream inputStream) {

        try{
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = r.readLine()) != null) {
                Log.v("MaestroLocalStore",line);
                SugarRecord.executeQuery(line);
            }
        }catch(Exception ex ){
            Log.e("MaestroLocalStore",ex.getMessage());
        }


    }
}
