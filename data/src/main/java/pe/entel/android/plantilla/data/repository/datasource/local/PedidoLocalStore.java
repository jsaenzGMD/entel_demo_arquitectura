package pe.entel.android.plantilla.data.repository.datasource.local;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.plantilla.data.dao.PedidoDao;
import pe.entel.android.plantilla.data.dao.PedidoDetalleDao;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class PedidoLocalStore {

    public void guardarPedido(PedidoDao pedido){
        pedido.save();
    }

    public List<PedidoDao> listarPedidoPendientes(){
        return PedidoDao.find(PedidoDao.class,"flg_enviado = 'F'");
    }

    public void guardarProducto(PedidoDetalleDao pedidoDetalleDao) {
        pedidoDetalleDao.save();
    }

    public List<PedidoDetalleDao> listarPedidoDetalle(Long idPedido){
        return PedidoDetalleDao.find(PedidoDetalleDao.class,"id_pedido = ?", String.valueOf(idPedido));
    }
}
