package pe.entel.android.plantilla.data.repository.datasource.local;

import java.util.List;

import pe.entel.android.plantilla.data.dao.ProductoDao;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class ProductoLocalStore {

    public List<ProductoDao> listarProductoXCodigo(String codigo){
        return ProductoDao.find(ProductoDao.class,"codigo_producto like '%"+codigo+"%'");
    }

    public List<ProductoDao> listarProductoXNombre(String nombre){
        return ProductoDao.find(ProductoDao.class,"nombre like '%"+nombre+"%'");
    }

    public List<ProductoDao> listarProducto(){
        return ProductoDao.listAll(ProductoDao.class);
    }

}
