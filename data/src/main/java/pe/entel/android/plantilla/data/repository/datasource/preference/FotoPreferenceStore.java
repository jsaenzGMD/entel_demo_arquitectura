package pe.entel.android.plantilla.data.repository.datasource.preference;

import android.content.Context;

import pe.entel.android.plantilla.data.repository.datasource.generic.PreferenceStore;

/**
 * Created by rtamayov on 17/04/2017.
 */
public class FotoPreferenceStore extends PreferenceStore {

    private final static String TIPO_CAMARA ="TIPO_CAMARA";

    public FotoPreferenceStore(Context context) {
        super(context);
    }

    public void setTipoCamara(int tipo) {
        saveOnSharePreferences(TIPO_CAMARA,tipo);

    }

    public int getTipoCamara() {
        return getPreferenceInt(TIPO_CAMARA);
    }
}
