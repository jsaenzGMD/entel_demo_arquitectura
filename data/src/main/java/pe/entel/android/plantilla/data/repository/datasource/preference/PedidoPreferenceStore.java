package pe.entel.android.plantilla.data.repository.datasource.preference;

import android.content.Context;

import com.google.gson.Gson;

import pe.entel.android.plantilla.data.repository.datasource.generic.PreferenceStore;
import pe.entel.android.plantilla.domain.entity.Pedido;

/**
 * Created by rtamayov on 13/04/2017.
 */

public class PedidoPreferenceStore extends PreferenceStore {


    private final static String PEDIDO = "PEDIDO";
    Gson gson;

    public PedidoPreferenceStore(Context context) {
        super(context);
        gson = new Gson();
    }

    public void setPedido(Pedido pedido) {
        if(pedido !=null)
            saveOnSharePreferences(PEDIDO, gson.toJson(pedido));
        else
            saveOnSharePreferences(PEDIDO, null);




    }

    public Pedido getPedido() {
        String json = getPreference(PEDIDO);

        if (json == null || json.equals(""))
            return null;
        else
            return gson.fromJson(json, Pedido.class);
    }
}
