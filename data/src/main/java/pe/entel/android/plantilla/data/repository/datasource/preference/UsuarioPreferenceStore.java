package pe.entel.android.plantilla.data.repository.datasource.preference;

import android.content.Context;

import com.google.gson.Gson;

import pe.entel.android.plantilla.data.repository.datasource.generic.PreferenceStore;
import pe.entel.android.plantilla.domain.entity.Usuario;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class UsuarioPreferenceStore extends PreferenceStore {

    private final static String USUARIO = "USUARIO";
    Gson gson;

    public UsuarioPreferenceStore(Context context) {
        super(context);
        gson = new Gson();
    }

    public void setUsuario(Usuario usuario) {
        saveOnSharePreferences(USUARIO, gson.toJson(usuario));
    }

    public Usuario getUsuario() {
        String json = getPreference(USUARIO);

        if (json == null || json.equals(""))
            return null;
        else
            return gson.fromJson(json, Usuario.class);
    }
}
