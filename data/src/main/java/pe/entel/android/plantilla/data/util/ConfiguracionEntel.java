package pe.entel.android.plantilla.data.util;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexander on 31/01/2015.
 */
public class ConfiguracionEntel {
	/*-----------------------------------------------------
	 MANEJO DE CONSTANTES
	 -----------------------------------------------------*/
    /**
     * Constante de respuesta de servidor OKNOMSG</br>
     * CONSRESSERVIDOROKNOMSG: 2
     * @deprecated
     */
    public final static int CONSRESSERVIDOROKNOMSG = 2;
    /**
     * Constante de respuesta de servidor OK</br>
     * CONSRESSERVIDOROK: 1
     * @deprecated
     */
    public final static int CONSRESSERVIDOROK=1;
    /**
     * Constante de respuesta de servidor ERROR</br>
     * CONSRESSERVIDORERROR: -1
     * @deprecated
     */
    public final static int CONSRESSERVIDORERROR=-1;
    /**
     * Constante de respuesta de servidor ALGUNERROR</br>
     * CONSRESSERVIDORALGUNERROR: 0
     * @deprecated
     */
    public final static int CONSRESSERVIDORALGUNERROR=0;

    public final static String CONSSPINNERSELECCIONAR="...";
    public final static String CONBUNDLEFILTRO="filtro";
    public final static String CONBUNDLEFOTO="foto";

    /**
     * Constante <b>TAG_COMM</b>
     * @deprecated
     */
    public final static String TAG_COMM = "COMM";
    /**
     * Constante de tipo de env&#237;o <b>POST</b> en conexi&#243;n BlueTooth
     * @deprecated
     */
    public final static String CONSTIPENVBLUETOOTHPOST = "POST";
    /**
     * Constante del tipo de env&#237;o <b>GET</b> en conexi&#243;n Bluetooth
     * @deprecated
     */
    public final static String CONSTIPENVBLUETOOTHGET = "GET";
    /**
     * Constante de respuesta de servidor FTP OK</br>
     * CONSERVIDORFTPOK: 1;
     * @deprecated
     */
    public final static int CONSERVIDORFTPOK = 1;
    /**
     * Constante de respuesta de servidor FTP ERROR</br>
     * CONSERVIDORFTPERROR: 0
     * @deprecated
     */
    public final static int CONSERVIDORFTPERROR = 0;


	/*-----------------------------------------------------
	 CONSTANTES SHARE PREFERENCAS (Nombe de las variables a guardar)
	-----------------------------------------------------*/
    /**
     * Constante de preferencia de Bluetooth</br>
     * CONSPRENOMBLUETOOTH: BluetoothDevice
     */
    public final static String CONSPRENOMBLUETOOTH="BluetoothDevice";

	/*-----------------------------------------------------
	 SINCRONIZACION
	 -----------------------------------------------------*/
    /**
     * Constante de nombre de archivo plano de sincronizaci&#243;n</br>
     * CONSSINNOMBRETXT: sincro.txt
     */
    public final static String CONSSINNOMBRETXT="sincro.txt";
    /**
     * Constante de nombre de carpeta de archivo plano de sincronizaci&#243;n</br>
     * CONSSINNOMBRECARPETA: /txt/
     */
    public final static String CONSSINNOMBRECARPETA="/txt/";
    /**
     * Constante de nombre de carpeta de im&#225;genes sincronizadas</br>
     * CONSIMGNOMBRECARPETA: /Img/
     */
    public final static String CONSIMGNOMBRECARPETA="/Img/";
    /**
     * Constante de nombre de carpeta de toma de fotos</br>
     * CONSFOTNOMBRECARPETA: /Foto/
     */
    public final static String CONSFOTNOMBRECARPETA="/Foto/";
    /**
     * Constante de nombre de zip de im&#225;genes sincronizadas</br>
     * CONSIMGNOMBREZIP: imagenes.zip
     */
    public final static String CONSIMGNOMBREZIP = "imagenes.zip";

	/*-----------------------------------------------------
	 PARAMETROS ASSETS
	 -----------------------------------------------------*/
    /**
     * Nombre del par&#225;metro en el <b>param.properties</b> del <b>Assets</b> que contiene la ruta de conexi&#243;n a servidor
     */
    public final static String IP_SERVER = "IP_SERVER";
    /**
     * Nombre del par&#225;metro en el <b>param.properties</b> del <b>Assets</b> que contiene la ruta de descarga del APK de la aplicaci&#243;n para actualizar
     */
    public final static String URL_ACTUALIZAR = "URL_ACTUALIZAR";

	/*-----------------------------------------------------
	 ROLES DE PREFERNCIAS
	 -----------------------------------------------------*/
    /**
     * Enumerable de Rol de Preferencia
     * @author jroeder
     *
     */
    public static enum EnumRolPreferencia {
        /**
         * Rol de Administrador. Puede modificar las preferencias
         */
        ADMINISTRADOR,
        /**
         * Rol de Usuario. Puede visualizar las preferencias
         */
        USUARIO
    };


	/*-----------------------------------------------------
	 TIPO DE BUSQUEDA
	 -----------------------------------------------------*/
    /**
     * Enumerable del tipo de b&#250;squeda
     * @author jroeder
     *
     */
    public static enum EnumTipBusqueda {NOMBRE,CODIGO,OTRO,OTRO2,OTRO3};

	/*-----------------------------------------------------
	 FLUJO, cuando se reutiliza varias actividades pero en diferentes flujos
	 -----------------------------------------------------*/
    /**
     * Enumerable de tipo de flujo en actividad.</br>
     * Es usado para reutilizar una actividad con diferentes flujos
     * @author jroeder
     *
     */
    public static enum EnumFlujo {FLUJO1,FLUJO2,FLUJO3,FLUJO4};

	/*-----------------------------------------------------
	 Estado de la transaccion
	 -----------------------------------------------------*/
    /**
     * Enumerable para el estado de objeto bean
     * @author jroeder
     *
     */
    public static enum EnumEstBean {NUEVO,NOENVIADO,ENVIADO};

	/*-----------------------------------------------------
	 TIPO DE CONECCIONES
	 -----------------------------------------------------*/
    /**
     * Enumerable del tipo de conexi&#243;n
     * @author jroeder
     *
     */
    public static enum EnumTipConexion {
        CONHTTP,
        CONBLUETOOTH;
        /**
         * Retorna el tipo de conexi&#243;n GET
         * @return {@link String}
         */
        public String getRequestGET(){
            return "GET";
        }
        /**
         * Retorna el tipo de conexi&#243;n POST
         * @return {@link String}
         */
        public String getRequestPOST(){
            return "POST";
        }
        /**
         * Retorna el {@link EnumTipConexion} correspondiente al ordinal del par&#225;metro.</br>
         * En caso el ordinal no exista, por defecto retorna {@link EnumTipConexion#CONHTTP CONHTTP}
         * @param ordinal - Ordinal del enumerable
         * @return {@link EnumTipConexion}
         */
        public static EnumTipConexion Get(int ordinal){
            try{
                return values()[ordinal];
            }catch(IndexOutOfBoundsException e){
                return CONHTTP;
            }
        }
    };

	/*------------------------------------------------------
	 * ASSETS\PROPERTIES
	 *-----------------------------------------------------*/
    /**
     * Nombre constante del archivos de propiedades en la carpeta <b>Assets</b></br>
     * CONSPROPERTYFILE: params.properties
     */
    public static String CONSPROPERTYFILE = "params.properties";


    /**
     * Clase enumerable con los valores de di&#225;logos en las actividades
     * <ul>
     * <li>EXCEPTION: -1</li>
     * <li>PROGRESS: -2</li>
     * <li>BACKUP: -3</li>
     * <li>ADMIN: -4</li>
     * </ul>
     * @author jroeder
     *
     */
    public static enum EnumDialog {
        /**
         * EXCEPTION: -1
         */
        EXCEPTION(-1),
        /**
         * PROGRESS: -2
         */
        PROGRESS(-2),
        /**
         * BACKUP: -3
         */
        BACKUP(-3),
        /**
         * ADMIN: -4
         */
        ADMIN(-4);

        private static final Map<Integer, EnumDialog> lookup = new HashMap<Integer, EnumDialog>();

        static {
            for(EnumDialog loEnum : EnumSet.allOf(EnumDialog.class)){
                lookup.put(loEnum.getValue(), loEnum);
            }
        }

        private int value;

        private EnumDialog(int value){
            this.value = value;
        }

        /**
         * Obtiene el valor del enumerable
         * @return int - valor del enumerable
         */
        public int getValue(){
            return value;
        }

        /**
         * Obtiene el enumerable dado un valor. Si el valor no coincide, retorna null
         * @param value - valor del enumerable
         * @return EnumDialog - Enumerable
         */
        public static EnumDialog get(int value){
            return lookup.get(value);
        }

        /**
         * Obtiene el enumerable dado el nombre del enumerable. Si el nombre no coincide, retorna null
         * @param name - nombre del enumerable
         * @return EnumDialog - Enumerable
         */
        public static EnumDialog get(String name){
            for(EnumDialog loEnum : lookup.values()){
                if(loEnum.toString().equals(name))
                    return loEnum;
            }
            return null;
        }
    }

    /**
     * Clase enumerable con los valores de respuesta del servidor http
     * <ul>
     * <li>ERROR: -1</li>
     * <li>ALGUNERROR: 0</li>
     * <li>OK: 1</li>
     * <li>OKNOMSG: 2</li>
     * </ul>
     * @author jroeder
     *
     */
    public static enum EnumServerResponse {
        /**
         * ERROR: -1
         */
        ERROR(-1),
        /**
         * ALGUNERROR: 0
         */
        ALGUNERROR(0),
        /**
         * OK: 1
         */
        OK(1),
        /**
         * OKNOMSG: 2
         */
        OKNOMSG(2);

        private static final Map<Integer, EnumServerResponse> lookup = new HashMap<Integer, EnumServerResponse>();

        static {
            for(EnumServerResponse loEnum : EnumSet.allOf(EnumServerResponse.class)){
                lookup.put(loEnum.getValue(), loEnum);
            }
        }

        private int value;

        private EnumServerResponse(int value){
            this.value = value;
        }

        /**
         * Obtiene el valor del enumerable
         * @return int - valor del enumerable
         */
        public int getValue(){
            return value;
        }

        /**
         * Obtiene el enumerable dado un valor. Si el valor no coincide, retorna null
         * @param value - valor del enumerable
         * @return EnumServerResponse - Enumerable
         */
        public static EnumServerResponse get(int value){
            return lookup.get(value);
        }

        /**
         * Obtiene el enumerable dado el nombre del enumerable. Si el nombre no coincide, retorna null
         * @param name - nombre del enumerable
         * @return EnumServerResponse - Enumerable
         */
        public static EnumServerResponse get(String name){
            for(EnumServerResponse loEnum : lookup.values()){
                if(loEnum.toString().equals(name))
                    return loEnum;
            }
            return null;
        }
    }
    /**
     * Clase enumerable con los valores de respuesta del servidor ftp
     * <ul>
     * <li>ERROR: -1</li>
     * <li>ALGUNERROR: 0</li>
     * <li>OK: 1</li>
     * <li>OKNOMSG: 2</li>
     * </ul>
     * @author jroeder
     *
     */
    public static enum EnumServerFTPResponse {
        /**
         * ERROR: 0
         */
        ERROR(0),
        /**
         * OK: 1
         */
        OK(1),
        /**
         * OKNOMSG: 2
         */
        OKNOMSG(2);

        private static final Map<Integer, EnumServerFTPResponse> lookup = new HashMap<Integer, EnumServerFTPResponse>();

        static {
            for(EnumServerFTPResponse loEnum : EnumSet.allOf(EnumServerFTPResponse.class)){
                lookup.put(loEnum.getValue(), loEnum);
            }
        }

        private int value;

        private EnumServerFTPResponse(int value){
            this.value = value;
        }

        /**
         * Obtiene el valor del enumerable
         * @return int - valor del enumerable
         */
        public int getValue(){
            return value;
        }

        /**
         * Obtiene el enumerable dado un valor. Si el valor no coincide, retorna null
         * @param value - valor del enumerable
         * @return EnumServerFTPResponse - Enumerable
         */
        public static EnumServerFTPResponse get(int value){
            return lookup.get(value);
        }

        /**
         * Obtiene el enumerable dado el nombre del enumerable. Si el nombre no coincide, retorna null
         * @param name - nombre del enumerable
         * @return EnumServerFTPResponse - Enumerable
         */
        public static EnumServerFTPResponse get(String name){
            for(EnumServerFTPResponse loEnum : lookup.values()){
                if(loEnum.toString().equals(name))
                    return loEnum;
            }
            return null;
        }
    }

    public final static String FECHADESPLIEGUE = "FECHADESPLIEGUE";


}
