package pe.entel.android.plantilla.domain.entity;

/**
 * Created by asoto on 27/02/2015.
 */
public class Localizacion {

    private String latitud;
    private String longitud;
    private String gpsPrecision;
    private String velocidad;
    private String fechaMovil;
    private String gpsOrigen;



    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(String velocidad) {
        this.velocidad = velocidad;
    }

    public String getFechaMovil() {
        return fechaMovil;
    }

    public void setFechaMovil(String fechaMovil) {
        this.fechaMovil = fechaMovil;
    }

    public String getGpsPrecision() {
        return gpsPrecision;
    }

    public void setGpsPrecision(String gpsPrecision) {
        this.gpsPrecision = gpsPrecision;
    }

    public String getGpsOrigen() {
        return gpsOrigen;
    }

    public void setGpsOrigen(String gpsOrigen) {
        this.gpsOrigen = gpsOrigen;
    }
}
