package pe.entel.android.plantilla.domain.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by rtamayov on 13/04/2017.
 */

public class Pedido {

    private Long id = null;
    private String idUsuario;
    private String latitud;
    private String longitud;
    private String fecha;
    private List<Producto> detalles;
    private String flgEnviado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFlgEnviado() {
        return flgEnviado;
    }

    public void setFlgEnviado(String flgEnviado) {
        this.flgEnviado = flgEnviado;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public List<Producto> getDetalles() {
        return detalles != null ? detalles : new ArrayList<Producto>();
    }

    public void setDetalles(List<Producto> detalles) {
        this.detalles = detalles;
    }

    public boolean existeProducto(String codigoProducto) {

        boolean flg = false;
        Producto bean;

        for (int i = detalles.size() - 1; i >= 0; i--) {
            bean = detalles.get(i);

            if (bean.getCodigoProducto().equals(String.valueOf(codigoProducto))) {
                flg = true;
                break;
            }
        }

        return flg;

    }

    public void ordenar() {
        if (detalles.size() > 0) {
            Collections.sort(detalles, new Comparator<Producto>() {
                @Override
                public int compare(final Producto object1, final Producto object2) {
                    return object1.getCodigoProducto().compareTo(object2.getCodigoProducto());
                }
            });
        }
    }

    public void eliminarProducto(String codigoProducto) {


        Producto bean;

        for (int i = detalles.size() - 1; i >= 0; i--) {
            bean = detalles.get(i);

            if (bean.getCodigoProducto().equals(String.valueOf(codigoProducto))) {
                detalles.remove(i);
                break;
            }
        }


    }

    public void editarProducto(Producto producto) {

        Producto bean;

        for (int i = detalles.size() - 1; i >= 0; i--) {
            bean = detalles.get(i);

            if (bean.getCodigoProducto().equals(String.valueOf(producto.getCodigoProducto()))) {
                detalles.set(i, producto);
                break;
            }
        }


    }


}
