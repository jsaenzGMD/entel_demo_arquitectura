package pe.entel.android.plantilla.domain.entity;

/**
 * Created by rtamayov on 19/04/2017.
 */
public class Photo {

    private Long id = null;
    private String idUsuario;
    private String identificador;
    private String flgEnviado;

    public String getFlgEnviado() {
        return flgEnviado;
    }

    public void setFlgEnviado(String flgEnviado) {
        this.flgEnviado = flgEnviado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
}
