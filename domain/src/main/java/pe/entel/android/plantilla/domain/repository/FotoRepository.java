package pe.entel.android.plantilla.domain.repository;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Photo;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 17/04/2017.
 */
public interface FotoRepository {

    void guardarFoto(String idUsuario, String identificador, byte[] data, GuardarFotoCallback guardarFotoCallback);

    void enviarFoto(String identificador, EnviarFotoCallback enviarFotoCallback);

    void eliminarFoto(String identificador);

    String obtenerRutaFoto(String identificador);

    int getTipoFoto();

    void setTipoFoto(int tipo);

    List<Photo> listarFotosPendientes();

    interface GuardarFotoCallback {
        void onGuardado();
        void onError(ErrorBundle errorBundle);
    }

    interface EnviarFotoCallback{
        void onEnviado(String mensaje);
        void onError(ErrorBundle errorBundle);
    }

}
