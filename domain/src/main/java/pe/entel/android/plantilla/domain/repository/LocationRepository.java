package pe.entel.android.plantilla.domain.repository;

import pe.entel.android.plantilla.domain.entity.Localizacion;

/**
 * Created by rtamayov on 12/04/2017.
 */
public interface LocationRepository {

    void obtenerLocalizacion(ObtenerLocalizacionCallback obtenerLocalizacionCallback);

    interface ObtenerLocalizacionCallback {
        void obtener(Localizacion localizacion);
    }
}
