package pe.entel.android.plantilla.domain.repository;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 11/04/2017.
 */
public interface MaestroRepository {

    void sincronizarMaestros(String usuario, SincronizarMaestrosCallback sincronizarMaestrosCallback);
    void borrarTodo();

    interface SincronizarMaestrosCallback {
        void onSincronizado(String strResult);

        void onError(ErrorBundle errorBundle);
    }
}
