package pe.entel.android.plantilla.domain.repository;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 13/04/2017.
 */

public interface PedidoRepository {

    void setPedido(Pedido pedido);
    Pedido getPedido();

    void guardarPedido(Pedido pedido);
    List<Pedido> listarPedidosPendientes();

    void enviarPedido(Pedido pedido, EnviarPedidoCallback callback);



    interface EnviarPedidoCallback{
        void onEnviado(String mensaje);
        void onError(ErrorBundle errorBundle);
    }
}
