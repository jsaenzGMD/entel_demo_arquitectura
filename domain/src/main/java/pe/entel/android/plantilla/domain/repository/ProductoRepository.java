package pe.entel.android.plantilla.domain.repository;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Producto;

/**
 * Created by rtamayov on 14/04/2017.
 */

public interface ProductoRepository {
    List<Producto> listarProductosXCodigo(String codigo);
    List<Producto> listarProductosXNombre(String nombre);
    List<Producto> listarProductos();
}
