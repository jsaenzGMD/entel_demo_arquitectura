package pe.entel.android.plantilla.domain.repository;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 03/04/2017.
 */
public interface UpdateAppRepository {

    void updateApp(String url, UpdateAppCallback updateAppCallback);

    interface UpdateAppCallback {
        void onUpdatedApp();

        void onError(ErrorBundle errorBundle);
    }
}
