package pe.entel.android.plantilla.domain.repository;

import pe.entel.android.plantilla.domain.entity.Usuario;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 11/04/2017.
 */
public interface UsuarioRepository {

    void validarUsuario(String codigo, String clave, ValidarUsuarioCallback validarUsuarioCallback);
    void setUsuario(Usuario usuario);
    Usuario getUsuario();

    interface ValidarUsuarioCallback {
        void onValidado(String mensaje, Usuario usuario);
        void onError(ErrorBundle errorBundle);
    }
}
