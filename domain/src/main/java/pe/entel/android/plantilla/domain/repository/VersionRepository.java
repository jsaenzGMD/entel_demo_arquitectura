package pe.entel.android.plantilla.domain.repository;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 10/04/2017.
 */
public interface VersionRepository {
    void obtenerVersion(ObtenerVersionCallback obtenerVersionCallback);
    void setVersion(String version);
    String getVersion();

    interface ObtenerVersionCallback {
        void obtenida(String version);
        void onError(ErrorBundle errorBundle);

    }
}
