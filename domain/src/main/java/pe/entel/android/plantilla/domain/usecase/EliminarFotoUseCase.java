package pe.entel.android.plantilla.domain.usecase;

/**
 * Created by rtamayov on 18/04/2017.
 */
public interface EliminarFotoUseCase {

    void ejecutar(String identificador);
}
