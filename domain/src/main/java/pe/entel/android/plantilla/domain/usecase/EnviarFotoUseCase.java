package pe.entel.android.plantilla.domain.usecase;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 18/04/2017.
 */
public interface EnviarFotoUseCase {


    void ejecutar(String identificador, EnviarFotoCallback enviarFotoCallback);

    interface EnviarFotoCallback{
        void onEnviado(String mensaje);
        void onError(ErrorBundle errorBundle);
    }
}
