package pe.entel.android.plantilla.domain.usecase;

import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 14/04/2017.
 */

public interface EnviarPedidoUseCase {

    void ejecutar(Pedido pedido, Callback callback);

    interface Callback {
        void onEnviar(String mensaje);
        void onError(ErrorBundle errorBundle);
    }
}
