package pe.entel.android.plantilla.domain.usecase;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 17/04/2017.
 */
public interface EnviarPendientesUseCase {

    void ejecutar(EnviarPendientesCalback calback);

    interface EnviarPendientesCalback{
        void onEnviar(String mensaje);
        void onError(ErrorBundle errorBundle);
    }
}
