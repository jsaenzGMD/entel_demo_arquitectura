package pe.entel.android.plantilla.domain.usecase;

/**
 * Created by rtamayov on 17/04/2017.
 */
public interface GetTipoFotoUseCase {
    int ejecutar();
}
