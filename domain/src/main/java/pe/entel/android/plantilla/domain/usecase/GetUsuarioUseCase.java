package pe.entel.android.plantilla.domain.usecase;

import pe.entel.android.plantilla.domain.entity.Usuario;

/**
 * Created by rtamayov on 11/04/2017.
 */
public interface GetUsuarioUseCase {
    Usuario ejecutar();
}
