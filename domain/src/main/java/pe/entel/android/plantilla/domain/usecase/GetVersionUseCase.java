package pe.entel.android.plantilla.domain.usecase;

/**
 * Created by rtamayov on 11/04/2017.
 */
public interface GetVersionUseCase {
    String ejecutar();
}
