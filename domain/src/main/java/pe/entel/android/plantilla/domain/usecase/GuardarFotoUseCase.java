package pe.entel.android.plantilla.domain.usecase;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 17/04/2017.
 */
public interface GuardarFotoUseCase {
    void ejecutar(String identificador, byte[] data, GuardarFotoCallback guardarFotoCallback);

    interface GuardarFotoCallback{
        void onGuardado();
        void onError(ErrorBundle errorBundle);
    }
}
