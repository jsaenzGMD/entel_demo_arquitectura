package pe.entel.android.plantilla.domain.usecase;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Photo;

/**
 * Created by rtamayov on 19/04/2017.
 */
public interface ListaFotoPendientesUseCase {

    List<Photo> ejecutar();
}
