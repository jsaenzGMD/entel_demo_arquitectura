package pe.entel.android.plantilla.domain.usecase;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Pedido;

/**
 * Created by rtamayov on 17/04/2017.
 */
public interface ListaPedidoPendientesUseCase {
    List<Pedido> ejecutar();
}
