package pe.entel.android.plantilla.domain.usecase;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Producto;

/**
 * Created by rtamayov on 14/04/2017.
 */

public interface ListarProductoUseCase {
    List<Producto> ejecutar(boolean isCodigo, String busqueda);
    List<Producto> ejecutar();
}
