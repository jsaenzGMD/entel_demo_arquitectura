package pe.entel.android.plantilla.domain.usecase;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 11/04/2017.
 */
public interface ObtenerMaestrosUseCase {
    void ejecutar(String usuario, Callback callback);


    interface Callback {
        void onSincronizado(String strResult);

        void onError(ErrorBundle errorBundle);
    }
}
