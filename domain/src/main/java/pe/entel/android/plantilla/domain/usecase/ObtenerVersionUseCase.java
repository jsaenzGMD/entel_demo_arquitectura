package pe.entel.android.plantilla.domain.usecase;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 10/04/2017.
 */
public interface ObtenerVersionUseCase {
    void ejecutar(Callback callback);

    interface Callback {
        void obtenida();
        void onError(ErrorBundle errorBundle);
    }
}
