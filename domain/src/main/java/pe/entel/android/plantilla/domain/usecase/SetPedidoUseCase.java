package pe.entel.android.plantilla.domain.usecase;

import pe.entel.android.plantilla.domain.entity.Pedido;

/**
 * Created by rtamayov on 13/04/2017.
 */

public interface SetPedidoUseCase {
    void ejecutar(Pedido pedido);
}
