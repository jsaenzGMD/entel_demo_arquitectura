package pe.entel.android.plantilla.domain.usecase;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 03/04/2017.
 */
public interface UpdateAppUseCase {

    void updateApp(String url, Callback useCaseCallback);

    interface Callback {
        void onUpdatedApp();

        void onError(ErrorBundle errorBundle);
    }
}
