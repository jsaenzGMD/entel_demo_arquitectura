package pe.entel.android.plantilla.domain.usecase;

/**
 * Created by rtamayov on 07/04/2017.
 */
public interface ValidarOperadorUseCase {
    void ejecutar();
}
