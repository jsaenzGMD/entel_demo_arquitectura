package pe.entel.android.plantilla.domain.usecase;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 11/04/2017.
 */
public interface ValidarUsuarioUseCase {

    void ejecutar(String codigo, String clave, Callback callback);

    interface Callback {
        void onValidado(String mensaje);

        void onError(ErrorBundle errorBundle);
    }
}
