package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.repository.FotoRepository;
import pe.entel.android.plantilla.domain.usecase.EliminarFotoUseCase;

/**
 * Created by rtamayov on 18/04/2017.
 */
public class EliminarFotoUseCaseImpl implements EliminarFotoUseCase {

    final FotoRepository fotoRepository;

    public EliminarFotoUseCaseImpl(FotoRepository fotoRepository) {
        this.fotoRepository = fotoRepository;
    }


    @Override
    public void ejecutar(String identificador) {
        fotoRepository.eliminarFoto(identificador);
    }
}
