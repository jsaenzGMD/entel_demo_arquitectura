package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.FotoRepository;
import pe.entel.android.plantilla.domain.usecase.EnviarFotoUseCase;

/**
 * Created by rtamayov on 18/04/2017.
 */
public class EnviarFotoUseCaseImpl implements EnviarFotoUseCase {

    final FotoRepository fotoRepository;

    public EnviarFotoUseCaseImpl(FotoRepository fotoRepository) {
        this.fotoRepository = fotoRepository;
    }


    @Override
    public void ejecutar(String identificador, final EnviarFotoCallback enviarFotoCallback) {
        fotoRepository.enviarFoto(identificador, new FotoRepository.EnviarFotoCallback() {
            @Override
            public void onEnviado(String mensaje) {
                enviarFotoCallback.onEnviado(mensaje);
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                enviarFotoCallback.onError(errorBundle);
            }
        });
    }
}
