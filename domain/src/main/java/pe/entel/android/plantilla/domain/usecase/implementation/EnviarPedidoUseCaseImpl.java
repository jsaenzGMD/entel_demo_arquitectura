package pe.entel.android.plantilla.domain.usecase.implementation;

import java.text.SimpleDateFormat;
import java.util.Date;

import pe.entel.android.plantilla.domain.entity.Localizacion;
import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.entity.Usuario;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.LocationRepository;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;
import pe.entel.android.plantilla.domain.repository.UsuarioRepository;
import pe.entel.android.plantilla.domain.usecase.EnviarPedidoUseCase;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class EnviarPedidoUseCaseImpl implements EnviarPedidoUseCase {

    final PedidoRepository pedidoRepository;
    final LocationRepository locationRepository;
    final UsuarioRepository usuarioRepository;

    public EnviarPedidoUseCaseImpl(PedidoRepository pedidoRepository, LocationRepository locationRepository, UsuarioRepository usuarioRepository) {
        this.pedidoRepository = pedidoRepository;
        this.locationRepository = locationRepository;
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public void ejecutar(final Pedido pedido, final Callback callback) {

        locationRepository.obtenerLocalizacion(new LocationRepository.ObtenerLocalizacionCallback() {
            @Override
            public void obtener(Localizacion localizacion) {
                enviarPedido(pedido,localizacion,callback);
            }
        });

    }

    private void enviarPedido(final Pedido pedido, Localizacion localizacion, final Callback callback) {
        Usuario usuario = usuarioRepository.getUsuario();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String latitud = "0";
        String longitud = "0";

        if (localizacion!=null){
            latitud = localizacion.getLatitud();
            longitud = localizacion.getLongitud();
        }

        pedido.setFlgEnviado("F");
        pedido.setFecha(dateFormat.format(now));
        pedido.setLatitud(latitud);
        pedido.setLongitud(longitud);
        pedido.setIdUsuario(usuario.getId());

        pedidoRepository.enviarPedido(pedido, new PedidoRepository.EnviarPedidoCallback() {
            @Override
            public void onEnviado(String mensaje) {
                 pedidoRepository.setPedido(null);
                callback.onEnviar(mensaje);
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                pedidoRepository.setPedido(null);
                callback.onError(errorBundle);
            }
        });
    }
}
