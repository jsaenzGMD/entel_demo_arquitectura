package pe.entel.android.plantilla.domain.usecase.implementation;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.entity.Photo;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.FotoRepository;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;
import pe.entel.android.plantilla.domain.usecase.EnviarPendientesUseCase;

/**
 * Created by rtamayov on 17/04/2017.
 */
public class EnviarPendientesUseCaseImpl implements EnviarPendientesUseCase {

    final PedidoRepository pedidoRepository;
    final FotoRepository fotoRepository;
    EnviarPendientesCalback callback;


    public EnviarPendientesUseCaseImpl(PedidoRepository pedidoRepository, FotoRepository fotoRepository) {
        this.pedidoRepository = pedidoRepository;
        this.fotoRepository = fotoRepository;
    }

    @Override
    public void ejecutar(EnviarPendientesCalback callback) {
        this.callback = callback;

        List<Pedido> pedidosPendientes = pedidoRepository.listarPedidosPendientes();
        List<Photo> fotosPendientes = fotoRepository.listarFotosPendientes();

        if((pedidosPendientes.size() + fotosPendientes.size()) ==0){
            callback.onEnviar("No hay Pendientes");
        }else{
            enviarPendientes();
        }
    }


    private void enviarPendientes(){
        List<Pedido> pedidosPendientes = pedidoRepository.listarPedidosPendientes();
        List<Photo> fotosPendientes = fotoRepository.listarFotosPendientes();

        if(pedidosPendientes.size()!=0) {
            Pedido pendiente = pedidosPendientes.get(0);

            pedidoRepository.enviarPedido(pendiente, new PedidoRepository.EnviarPedidoCallback() {
                @Override
                public void onEnviado(String mensaje) {
                    enviarPendientes();
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    callback.onError(errorBundle);
                }
            });
        }else if(fotosPendientes.size()!=0){
            Photo pendiente = fotosPendientes.get(0);

            fotoRepository.enviarFoto(pendiente.getIdentificador(), new FotoRepository.EnviarFotoCallback() {
                @Override
                public void onEnviado(String mensaje) {
                    enviarPendientes();
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    callback.onError(errorBundle);
                }
            });

        }
        else{
            callback.onEnviar("Pendientes enviados correctamente");
        }
    }




}
