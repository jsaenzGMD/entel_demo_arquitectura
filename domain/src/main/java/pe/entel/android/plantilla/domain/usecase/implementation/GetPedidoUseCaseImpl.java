package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;
import pe.entel.android.plantilla.domain.usecase.GetPedidoUseCase;

/**
 * Created by rtamayov on 13/04/2017.
 */

public class GetPedidoUseCaseImpl implements GetPedidoUseCase {
    final PedidoRepository pedidoRepository;

    public GetPedidoUseCaseImpl(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @Override
    public Pedido ejecutar() {
        return pedidoRepository.getPedido();
    }
}
