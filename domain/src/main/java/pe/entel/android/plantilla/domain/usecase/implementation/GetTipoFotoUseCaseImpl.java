package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.repository.FotoRepository;
import pe.entel.android.plantilla.domain.usecase.GetTipoFotoUseCase;

/**
 * Created by rtamayov on 17/04/2017.
 */
public class GetTipoFotoUseCaseImpl implements GetTipoFotoUseCase {

    final FotoRepository fotoRepository;

    public GetTipoFotoUseCaseImpl(FotoRepository fotoRepository) {
        this.fotoRepository = fotoRepository;
    }


    @Override
    public int ejecutar() {
        return fotoRepository.getTipoFoto();
    }
}
