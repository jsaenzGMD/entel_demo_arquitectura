package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.entity.Usuario;
import pe.entel.android.plantilla.domain.repository.UsuarioRepository;
import pe.entel.android.plantilla.domain.usecase.GetUsuarioUseCase;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class GetUsuarioUseCaseImpl implements GetUsuarioUseCase {

    final UsuarioRepository usuarioRepository;

    public GetUsuarioUseCaseImpl(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public Usuario ejecutar() {
        return usuarioRepository.getUsuario();
    }
}
