package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.repository.VersionRepository;
import pe.entel.android.plantilla.domain.usecase.GetVersionUseCase;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class GetVersionUseCaseImpl implements GetVersionUseCase {

    final VersionRepository versionRepository;

    public GetVersionUseCaseImpl(VersionRepository versionRepository) {
        this.versionRepository = versionRepository;
    }

    @Override
    public String ejecutar() {
        return versionRepository.getVersion();
    }
}
