package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.FotoRepository;
import pe.entel.android.plantilla.domain.repository.UsuarioRepository;
import pe.entel.android.plantilla.domain.usecase.GuardarFotoUseCase;

/**
 * Created by rtamayov on 17/04/2017.
 */
public class GuardarFotoUseCaseImpl implements GuardarFotoUseCase {

    final FotoRepository fotoRepository;
    final UsuarioRepository usuarioRepository;

    public GuardarFotoUseCaseImpl(FotoRepository fotoRepository, UsuarioRepository usuarioRepository) {
        this.fotoRepository = fotoRepository;
        this.usuarioRepository = usuarioRepository;
    }


    @Override
    public void ejecutar(String identificador, byte[] data, final GuardarFotoCallback guardarFotoCallback) {

        String idUsuario = usuarioRepository.getUsuario().getId();

        fotoRepository.guardarFoto(idUsuario,identificador, data, new FotoRepository.GuardarFotoCallback() {
            @Override
            public void onGuardado() {
                guardarFotoCallback.onGuardado();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                guardarFotoCallback.onError(errorBundle);
            }
        });
    }
}
