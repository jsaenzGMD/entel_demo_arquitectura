package pe.entel.android.plantilla.domain.usecase.implementation;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Photo;
import pe.entel.android.plantilla.domain.repository.FotoRepository;
import pe.entel.android.plantilla.domain.usecase.ListaFotoPendientesUseCase;

/**
 * Created by rtamayov on 19/04/2017.
 */
public class ListaFotoPendientesUseCaseImpl implements ListaFotoPendientesUseCase {

    final FotoRepository fotoRepository;

    public ListaFotoPendientesUseCaseImpl(FotoRepository fotoRepository) {
        this.fotoRepository = fotoRepository;
    }

    @Override
    public List<Photo> ejecutar() {
        return fotoRepository.listarFotosPendientes();
    }
}
