package pe.entel.android.plantilla.domain.usecase.implementation;

import java.util.List;

import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;
import pe.entel.android.plantilla.domain.usecase.ListaPedidoPendientesUseCase;

/**
 * Created by rtamayov on 17/04/2017.
 */
public class ListaPedidoPendientesUseCaseImpl implements ListaPedidoPendientesUseCase {

    final PedidoRepository pedidoRepository;

    public ListaPedidoPendientesUseCaseImpl(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @Override
    public List<Pedido> ejecutar() {
        return pedidoRepository.listarPedidosPendientes();
    }
}
