package pe.entel.android.plantilla.domain.usecase.implementation;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.plantilla.domain.entity.Producto;
import pe.entel.android.plantilla.domain.repository.ProductoRepository;
import pe.entel.android.plantilla.domain.usecase.ListarProductoUseCase;

/**
 * Created by rtamayov on 14/04/2017.
 */

public class ListarProductoUseCaseImpl implements ListarProductoUseCase {

    final ProductoRepository productoRepository;

    public ListarProductoUseCaseImpl(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    @Override
    public List<Producto> ejecutar() {

        List<Producto> productos = new ArrayList<>();

        //if (isCodigo)
        //    productos = productoRepository.listarProductosXCodigo(busqueda);
        //else
        //    productos = productoRepository.listarProductosXNombre(busqueda);
        productos = productoRepository.listarProductos();
        return productos;
    }

    @Override
    public List<Producto> ejecutar(boolean isCodigo, String busqueda) {

        List<Producto> productos = new ArrayList<>();

        if (isCodigo)
            productos = productoRepository.listarProductosXCodigo(busqueda);
        else
            productos = productoRepository.listarProductosXNombre(busqueda);

        return productos;
    }
}
