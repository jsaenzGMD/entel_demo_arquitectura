package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.repository.FotoRepository;
import pe.entel.android.plantilla.domain.usecase.ObtenerFotoRutaUseCase;

/**
 * Created by rtamayov on 17/04/2017.
 */
public class ObtenerFotoRutaUseCaseImpl implements ObtenerFotoRutaUseCase {

    final FotoRepository fotoRepository;

    public ObtenerFotoRutaUseCaseImpl(FotoRepository fotoRepository) {
        this.fotoRepository = fotoRepository;
    }

    @Override
    public String ejecutar(String identificador) {
        return fotoRepository.obtenerRutaFoto(identificador);
    }
}
