package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.MaestroRepository;
import pe.entel.android.plantilla.domain.usecase.ObtenerMaestrosUseCase;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class ObtenerMaestrosUseCaseImpl implements ObtenerMaestrosUseCase {

    final MaestroRepository maestroRepository;

    public ObtenerMaestrosUseCaseImpl(MaestroRepository maestroRepository) {
        this.maestroRepository = maestroRepository;
    }

    @Override
    public void ejecutar(String usuario, final Callback callback) {
        maestroRepository.borrarTodo();
        maestroRepository.sincronizarMaestros(usuario, new MaestroRepository.SincronizarMaestrosCallback() {
            @Override
            public void onSincronizado(String strResult) {
                callback.onSincronizado(strResult);
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                callback.onError(errorBundle);
            }
        });

    }
}
