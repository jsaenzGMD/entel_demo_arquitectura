package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.VersionRepository;
import pe.entel.android.plantilla.domain.usecase.ObtenerVersionUseCase;

/**
 * Created by rtamayov on 10/04/2017.
 */
public class ObtenerVersionUseCaseImpl implements ObtenerVersionUseCase {

    final VersionRepository versionRepository;

    public ObtenerVersionUseCaseImpl(VersionRepository versionRepository) {
        this.versionRepository = versionRepository;
    }

    @Override
    public void ejecutar(final Callback callback) {
        versionRepository.obtenerVersion(new VersionRepository.ObtenerVersionCallback() {
            @Override
            public void obtenida(String version) {
                versionRepository.setVersion(version);
                callback.obtenida();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                callback.onError(errorBundle);
            }
        });
    }
}
