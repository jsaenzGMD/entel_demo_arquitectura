package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.entity.Pedido;
import pe.entel.android.plantilla.domain.repository.PedidoRepository;
import pe.entel.android.plantilla.domain.usecase.SetPedidoUseCase;

/**
 * Created by rtamayov on 13/04/2017.
 */

public class SetPedidoUseCaseImpl implements SetPedidoUseCase {
    final PedidoRepository  pedidoRepository;

    public SetPedidoUseCaseImpl(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @Override
    public void ejecutar(Pedido pedido) {
        pedidoRepository.setPedido(pedido);
    }
}
