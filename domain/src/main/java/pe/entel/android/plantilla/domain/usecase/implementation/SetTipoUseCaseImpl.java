package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.repository.FotoRepository;
import pe.entel.android.plantilla.domain.usecase.SetTipoFotoUseCase;

/**
 * Created by rtamayov on 18/04/2017.
 */
public class SetTipoUseCaseImpl implements SetTipoFotoUseCase {

    final FotoRepository fotoRepository;

    public SetTipoUseCaseImpl(FotoRepository fotoRepository) {
        this.fotoRepository = fotoRepository;
    }

    @Override
    public void ejecutar(int tipo) {
        fotoRepository.setTipoFoto(tipo);
    }
}
