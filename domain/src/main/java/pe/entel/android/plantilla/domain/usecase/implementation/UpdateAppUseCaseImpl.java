package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.UpdateAppRepository;
import pe.entel.android.plantilla.domain.usecase.UpdateAppUseCase;

/**
 * Created by rtamayov on 03/04/2017.
 */
public class UpdateAppUseCaseImpl implements UpdateAppUseCase {

    private Callback callback;
    private final UpdateAppRepository updateAppRepository;

    public UpdateAppUseCaseImpl(UpdateAppRepository updateAppRepository) {
        this.updateAppRepository = updateAppRepository;
    }

    @Override
    public void updateApp(String url, Callback callback) {
        if (callback == null) {
            throw new IllegalArgumentException("Interactor callback cannot be null!!!");
        }

        this.callback = callback;
        updateAppRepository.updateApp(url, updateAppCallback);
    }

    private final UpdateAppRepository.UpdateAppCallback updateAppCallback = new UpdateAppRepository.UpdateAppCallback() {
        @Override
        public void onUpdatedApp() {
            callback.onUpdatedApp();
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            callback.onError(errorBundle);
        }
    };
}
