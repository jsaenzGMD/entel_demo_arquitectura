package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.repository.UtilRepository;
import pe.entel.android.plantilla.domain.repository.OperadorRepository;
import pe.entel.android.plantilla.domain.usecase.ValidarOperadorUseCase;

/**
 * Created by rtamayov on 07/04/2017.
 */
public class ValidarOperadorUseCaseImpl implements ValidarOperadorUseCase {

    final OperadorRepository operadorRepository;
    final UtilRepository utilRepository;

    public ValidarOperadorUseCaseImpl(OperadorRepository operadorRepository, UtilRepository utilRepository) {
        this.operadorRepository = operadorRepository;
        this.utilRepository = utilRepository;
    }

    @Override
    public void ejecutar() {
        boolean haySenal = utilRepository.haySenal();

        if(haySenal){
            boolean validado = operadorRepository.validarOperador();
            if(!validado){
                throw new RuntimeException("Operador no valido");
            }
        }

    }
}
