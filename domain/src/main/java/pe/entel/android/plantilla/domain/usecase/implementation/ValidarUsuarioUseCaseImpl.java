package pe.entel.android.plantilla.domain.usecase.implementation;

import pe.entel.android.plantilla.domain.entity.Usuario;
import pe.entel.android.plantilla.domain.exception.EncapsulatedErrorBundle;
import pe.entel.android.plantilla.domain.exception.ErrorBundle;
import pe.entel.android.plantilla.domain.repository.UsuarioRepository;
import pe.entel.android.plantilla.domain.usecase.ValidarUsuarioUseCase;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class ValidarUsuarioUseCaseImpl implements ValidarUsuarioUseCase {

    final UsuarioRepository usuarioRepository;

    public ValidarUsuarioUseCaseImpl(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }


    @Override
    public void ejecutar(String codigo, final String clave, final Callback callback) {

        if(codigo.equals("")||clave.equals("")){
            Exception exception = new Exception("Ingresa usuario y clave");
            ErrorBundle errorBundle = new EncapsulatedErrorBundle(exception);
            callback.onError(errorBundle);
        }else{


            usuarioRepository.validarUsuario(codigo, clave, new UsuarioRepository.ValidarUsuarioCallback() {
                @Override
                public void onValidado(String mensaje, Usuario usuario) {
                    usuario.setClave(clave);
                    usuarioRepository.setUsuario(usuario);
                    callback.onValidado(mensaje);
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    callback.onError(errorBundle);
                }
            });


        }

    }
}
